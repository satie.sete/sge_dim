# Réduction de dimension des prix nodaux dans un réseau de distribution

Article présenté à [SGE 2023](https://sge2023.sciencesconf.org/) par Guénolé Chérot.

# Contenu du répertoire

- Version auteur de l'article
- Code utilisé pour tester la méthode proposée dans l'article