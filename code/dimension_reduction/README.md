# Dimension reduction
Use dimension reduction (PCA, Auto-encoder), to reduce the number of dual variable in the network.

# Organization of the code
## Creation of the data base
We first need to compute prices associated with the constrains of the distribution network.
They are drawn from the the OPF calcultation done by the PowerModels modules of PandaPower.
Using this library enables the calculation of fast and accurate price since it uses a second order cone relaxation which as been proven to be exact in radial networks.

Once prices are computed, they are stored in a data base named **Data_Base_Name** to be analysed later.

## PCA
Uses a the Principal Component Analysis to reduce the number of prices variable.

## Auto-encoder
Uses an Auto-Encoder to reduce to reduce the number of prices variable.
This techniques produces results that are more difficult to analise but has the advantage of being non-linear.
The dimension reduction is supposed to be better whith less loss of information.

## Analysis of the compression
Once learned, compressions need to be tested.
The compressed prices are fed back into the network in order to compute metrics such as energy missmatch, constrains violations, **..?**.