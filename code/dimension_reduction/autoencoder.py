# -*- coding: utf-8 -*-
import keras

class Autoencoder_model(keras.models.Model):
    def __init__(self, original_dim, latent_dim, typ=None, n_hidden=2, dim_hidden=None,
                 delete_one_dim=False, activation="relu", dropout=False):
        """
        Parameters
            - typ : None or conv
                Type of neural network
            - n_hidden : int
                Number of hidden layers
            - dim_hidden : array or None
                Number of layer in each hidden layer. If None, the number of layer is computed automatically
            - original_dim : int
                Number of neurons on the first and last layers
            - latent_dim : int
                Number of neurons in the bottleneck."""
                
        super(Autoencoder_model, self).__init__()
        self.init_const(original_dim, latent_dim)
        self._init_network(original_dim, latent_dim, typ, n_hidden, dim_hidden, delete_one_dim, activation, dropout)
      
    def _init_network(self, original_dim, latent_dim, typ=None, n_hidden=2, dim_hidden=None,
                      delete_one_dim=False, activation="relu", dropout=False):
        def _hidden_layers_complex(dim_hidden, input_vec):
            delta = original_dim - latent_dim
            if dim_hidden == None :
                delta_dim = int(delta/(n_hidden+1))
                dim_hidden = [original_dim - (k+1)*delta_dim for k in range(n_hidden)]
            
            if typ == None:
                encoded = keras.layers.Dense(dim_hidden[0], activation=activation)(input_vec)
                for k in range(1,len(dim_hidden)) :
                    if dropout:
                        keras.layers.Dropout(rate=0.25)(encoded)
                    encoded = keras.layers.Dense(dim_hidden[k], activation=activation)(encoded)
                encoded = keras.layers.Dense(latent_dim, activation=activation)(encoded)
                # We use a sigmoid activation function : the observation space is bounded between 0 and 1.
                input_vec_latent = keras.Input(shape=(latent_dim,))
                decoded = keras.layers.Dense(dim_hidden[-1], activation=activation)(input_vec_latent)
                for k in range(len(dim_hidden)-2,-1, -1) :
                    decoded = keras.layers.Dense(dim_hidden[k], activation=activation)(decoded)
                    if dropout:
                        keras.layers.Dropout(rate=0.25)(decoded)
                decoded = keras.layers.Dense(original_dim, activation='linear')(decoded)
            if typ == "conv":
                encoded = keras.layers.Conv1D(32, 3, activation=activation)(input_vec)
                encoded = keras.layers.MaxPooling1D(pool_size=2, strides=1, padding='same')(encoded)
                encoded = keras.layers.Conv1D(32, 3, activation=activation)(encoded)
                encoded = keras.layers.Flatten()(encoded)
                encoded = keras.layers.Dense(latent_dim, activation=activation)(encoded)
                
                input_vec_latent = keras.Input(shape=(latent_dim,))
                decoded = keras.layers.Dense(dim_hidden[1], activation=activation)(input_vec_latent)
                decoded = keras.layers.Dense(dim_hidden[0], activation='linear')(decoded)
            return encoded, decoded, input_vec_latent
        
        def _hidden_layers_simple(input_vec):
            encoded = keras.layers.Dense(latent_dim, activation='linear')(input_vec)
            input_vec_latent = keras.Input(shape=(latent_dim,))
            decoded = keras.layers.Dense(original_dim, activation='linear')(input_vec_latent)
            return encoded, decoded, input_vec_latent
        
        if delete_one_dim : input_vec = keras.Input(shape=(self.original_dim-1,))
        else : input_vec = keras.Input(shape=(self.original_dim,))
        if n_hidden != 0 : encoded, decoded, input_vec_latent = _hidden_layers_complex(dim_hidden, input_vec)
        else :  encoded, decoded, input_vec_latent = _hidden_layers_simple(input_vec)
        self.encoder = keras.models.Model(input_vec, encoded)
        self.decoder = keras.models.Model(input_vec_latent, decoded)
      
    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
    
    def init_const(self, original_dim, latent_dim):
        self.latent_dim = latent_dim
        self.original_dim = original_dim
        self.reconstructed_dim = original_dim
