# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 22:03:15 2022

@author: Guenole
"""
from os.path import join
import pandapower as pp
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import keras
# Custom import
from code_these.dimension_reduction.generate_data import get_n_dual,  init_result_array, _check_optimality, get_results, setup_network
from code_these.dimension_reduction.dimension_reduction import compress_decompress
import code_these.src._import as imp
import code_these.src._initialize as initialize
import code_these.src._modify_net as modify_net
import code_these.test_case_generation.plot.test_plot_from_scratch as plot
import code_these.dimension_reduction.import_func as import_func
import src._SOCWR_tools as SOCWR_tools
from code_these.dimension_reduction import autoencoder


def _choose_model(model_type, dim):
    if model_type == "pca" :
        path = join(save_path, "dimension_reduction/pca")
        model_name = "pca_"+str(dim)+"_dim"
        model = imp.load_model(path, model_name)
    elif model_type == "ae":
        path = join(save_path, "dimension_reduction/ae","ae_"+str(dim)+"_dim")
        model = keras.models.load_model(path, custom_objects={"CustomModel": autoencoder.Autoencoder_model})
    return model

def _compute_LMP(model_type):
    # Compute PF on network using compressed prices
    model = _choose_model(model_type, dim)
    scaler = pickle.load(open(join(save_path, "dimension_reduction/scaler", 'scaler.pkl'),'rb'))
    LMP = dual_array_opf[k, dic_dual["active power balance"]["id_dual"]] # Import LMP
    LMP_scaled = scaler.transform(LMP.reshape(1, -1))
    LMP_info_loss_scaled = compress_decompress(model, LMP_scaled, model_type) # compress^-1(compress(LMP))
    LMP_info_loss = scaler.inverse_transform(LMP_info_loss_scaled.reshape(1,-1))[0]
    return LMP, LMP_scaled[0], LMP_info_loss, LMP_info_loss_scaled

def _plot(LMP, LMP_tilde_ae, LMP_tilde_pca):
    plt.figure(figsize=(10,2), dpi=200)
    plt.bar(np.arange(len(LMP_tilde_ae))-0.2, LMP_tilde_ae-LMP, 0.2, label="ae")
    plt.bar(np.arange(len(LMP_tilde_ae))+0.2, LMP_tilde_pca-LMP,0.2, label="pca")
    plt.legend()
    plt.show()
    print("RMSE ae : ", np.sqrt(np.mean(np.square(LMP-LMP_tilde_ae))))
    print("RMSE pca : ", np.sqrt(np.mean(np.square(LMP-LMP_tilde_pca))))

def _plot_injection(net_1, net_2):
    def _plot(data, title):
        plt.plot(data, ".")
        plt.title(title)
        plt.show()
    data = net_1.res_load["p_mw"] - net_2.res_load["p_mw"]
    _plot(data, "load p")
    data = net_1.res_load["q_mvar"] - net_2.res_load["q_mvar"]
    _plot(data, "load q")
    data = net_1.res_gen["p_mw"] - net_2.res_gen["p_mw"]
    _plot(data, "gen p")
    data = net_1.res_gen["q_mvar"] - net_2.res_gen["q_mvar"]
    _plot(data, "gen q")
    data = net_1.res_ext_grid["p_mw"] - net_2.res_ext_grid["p_mw"]
    _plot(data, "ext grid p")
    data = net_1.res_ext_grid["q_mvar"] - net_2.res_ext_grid["q_mvar"]
    _plot(data, "ext grid q")

from julia.api import Julia
jl = Julia(compiled_modules=False)
path_origin , folder = imp.path_import("PC_Roman", 1)
save_path = join(path_origin,"processed/use", str(folder), "data")
dic = initialize._init_dic() 
dic["model_type"] = "ae"
net_opf, net_pf = setup_network(folder, path_origin)
# net_opf.line.loc[:, "max_i_ka"] = 0.01

data_path = join(path_origin,"processed/data")
days = [150] # np.sort(dic["test_days"]) ; [162] : [211]
t = 240

global_scale = 1

for day in days :
    power_agent = global_scale*import_func.import_power(net_opf, data_path, day) # Import power from database
    agent_cp1, agent_cp2 = import_func.import_prices(net_opf, data_path, day, str(folder)) # Import costs from database
    time_array = [180, 210, 240, 270] # [840, 870, 900, 900, 930, 960, 990, 1020] # [180, 210, 240, 270] # [t] ; np.arange(0,365*24*30,4*60) ; [840, 870, 900, 900, 930, 960,990]
    time_step = len(time_array) # Number of time steps
    n_dual = get_n_dual(net_opf) # Number of dual variables
    dic_dual = SOCWR_tools.get_all_dual(net_opf.dual) # Dictionnary used to understand duals
    n_LMP = 131 # Number of LMP prices (should be equal to the number of buses)
    primal_array_opf = init_result_array(net_opf, it=time_step) # Array containing primal values obtained with the OPF
    dual_array_opf = np.zeros((time_step, n_dual)) # Array containing all dual values obtained with the OPF
    primal_array_pf = [init_result_array(net_opf, it=time_step) for k in range(n_LMP)]
    dual_array_pf = np.zeros((n_LMP, time_step, n_dual))
    
    for k, t in enumerate(time_array) :
        # Compute OPF on network with impedance under-estimated
        modify_net.set_poly_cost(net_opf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])
        modify_net.set_bounds_agents(net_opf, power_agent.iloc[:,int(t)])
        pp.runpm(net_opf, pm_model='SOCWRPowerModel') # AC OPF (Would prefert SOC bug does not take voltage into account)
        modify_net.compute_vm_pu(net_opf)
        _check_optimality(net_opf.dual["dual_value"], t, day, dic_dual)
        get_results(primal_array_opf, k, net_opf)
        dual_array_opf[k] = net_opf.dual["dual_value"] # Stores dual values
        
        if True :
            modify_net.set_poly_cost(net_pf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])
            
            i = 0
            dim = 129
            
            model = _choose_model("pca", 129)
            scaler = pickle.load(open(join(save_path, "dimension_reduction/scaler", 'scaler.pkl'),'rb'))
            LMP = dual_array_opf[k, dic_dual["active power balance"]["id_dual"]] # Import LMP
            LMP_scaled = scaler.transform(LMP.reshape(1, -1))
            LMP_info_loss_scaled = compress_decompress(model, LMP_scaled, "pca") # compress^-1(compress(LMP))
            LMP_info_loss = scaler.inverse_transform(LMP_info_loss_scaled.reshape(1,-1))[0]
            res_load, res_gen = modify_net.get_pmw_from_LMP(LMP_info_loss, net_opf, dic_dual) # Compute Power = f^-1(LMP)
            net_pf = modify_net.set_power(net_pf, res_load, res_gen, net_opf.res_ext_grid, False)
            pp.runpm(net_pf, pm_model="SOCWRPowerModel") # Run powerflow
            _check_optimality(net_pf.dual["dual_value"], t, day, dic_dual)
            modify_net.compute_vm_pu(net_pf)
            modify_net.compute_line_loading(net_pf)
            net_pf.dual.loc[dic_dual["active power balance"]["id_dual"], "dual_value"] = LMP_info_loss
            if np.mean(np.abs(net_pf.res_bus["vm_pu"] - net_opf.res_bus["vm_pu"])) > 0.01 :
                print("day :", day)
                print("time :", t)
                print("error :", np.mean(np.abs(net_pf.res_bus["vm_pu"] - net_opf.res_bus["vm_pu"])))
            # LMP, LMP_scaled, LMP_tilde_pca, LMP_tilde_scaled_pca = _compute_LMP("pca")
            # _, _, LMP_tilde_ae, LMP_tilde_scaled_ae = _compute_LMP("ae")
            # # _plot(LMP, LMP_tilde_ae, LMP_tilde_pca)
            # _plot(LMP_scaled, LMP_tilde_scaled_ae, LMP_tilde_scaled_pca)
        

# for k, t in enumerate(time_array) :
#     # Compute OPF on network with impedance under-estimated
#     modify_net.set_poly_cost(net_opf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])
#     modify_net.set_bounds_agents(net_opf, power_agent.iloc[:,int(t)])
#     pp.runpm(net_opf, pm_model='SOCWRPowerModel') # AC OPF (Would prefert SOC bug does not take voltage into account)
#     modify_net.compute_vm_pu(net_opf)
#     _check_optimality(net_opf.dual["dual_value"], t, day, dic_dual)
#     get_results(primal_array_opf, k, net_opf)
#     dual_array_opf[k] = net_opf.dual["dual_value"] # Stores dual values
    
#     if True :
#         modify_net.set_poly_cost(net_pf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])

#         # Compute PF on network using compressed prices
#         LMP = dual_array_opf[k, dic_dual["active power balance"]["id_dual"]] # Import LMP
#         res_load, res_gen = modify_net.get_pmw_from_LMP(LMP, net_opf, dic_dual) # Compute Power = f^-1(LMP)
#         net_pf = modify_net.set_power(net_pf, res_load, res_gen, net_opf.res_ext_grid, False)
#         pp.runpm(net_pf, pm_model="SOCWRPowerModel") # Run powerflow
#         _check_optimality(net_pf.dual["dual_value"], t, day, dic_dual)
#         modify_net.compute_vm_pu(net_pf)
#         modify_net.compute_line_loading(net_pf)





