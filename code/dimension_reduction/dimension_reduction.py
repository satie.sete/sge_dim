#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Train a PCA and an auto-encoder to reduce the dimension of dual variable.
"""
# Standard libraries
import os
from os.path import join
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import numpy as np
import keras
import pickle
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from joblib import Parallel, delayed
from tqdm import tqdm
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize
from code_these.dimension_reduction import autoencoder
import code_these.dimension_reduction.variational_autoencoder as vae

#######################################################################################
####################################### Import ########################################
#######################################################################################

def _norm(data):
    normed_data = (data + 1)/2
    return normed_data

def _unnorm(normed_data):
    data = normed_data*2 - 1
    return data

def import_data(folder, path_origin, split_set="train"):
    variables, var_names, days = imp.read_all_variables(folder, path_origin, split_set)
    dic_dual = imp.load_dict(folder, path_origin, "dic_dual")
    dual_var = variables[0]
    time = imp.load_time(folder, path_origin)
    return dual_var, dic_dual, variables, var_names, time, days

def _import_state(dual_var, dual_index):
    index_voltage_mag = dual_index[2][0] # index where voltage magnitude are located
    index_voltage_angle = dual_index[9][0] # index where voltage angles are located
    index = np.concatenate((index_voltage_mag, index_voltage_angle))
    return dual_var[:,index]

#######################################################################################
######################################### FIT #########################################
#######################################################################################

def _split1(data, time):
    gkf = GroupKFold(n_splits=4)
    groups = np.arange(int(len(data)/len(time)))
    groups = np.repeat(groups, len(time))
    index_train, index_test = list(gkf.split(data, data, groups=groups))[0]
    return index_train, index_test

def _split2(data, index_train, index_test):
    data_train = data[index_train]
    data_test = data[index_test]
    return data_train, data_test

def fit_autoencoder_special(dic, data, time, components_to_throw, verbose=2, save_path=None):
    auto_encoder = autoencoder.Autoencoder_model(original_dim = np.shape(data)[1],
                                                 latent_dim = 3, n_hidden=dic["n_hidden"],
                                                 dim_hidden=None, delete_one_dim=True)
    auto_encoder.compile(optimizer=keras.optimizers.Adam(learning_rate=dic["learning_rate"]),
                         loss='mean_squared_error')
    data_in = np.delete(data, components_to_throw, axis=1)
    data_out = data
    if dic["test_ae_training"] == True :
        index_train, index_test = _split1(data, time)
        data_in_train, data_in_test  = _split2(data_in, index_train, index_test)
        data_out_train, data_out_test  = _split2(data_out, index_train, index_test)
        history = auto_encoder.fit(data_in_train, data_out_train,
                        epochs=dic["n_epochs"],
                        batch_size=dic["batch_size"],
                        shuffle=True, validation_data=(data_in_test, data_out_test),
                        verbose=verbose)
    else :
        history = auto_encoder.fit(data_in_train, data_out_train,
                        epochs=dic["n_epochs"],
                        batch_size=dic["batch_size"],
                        shuffle=True,
                        verbose=verbose)
    data_out_pred = auto_encoder.encoder.predict(data_in, verbose=0)
    if save_path != None :
        # save model 
        auto_encoder.save(join(save_path, "ae_3_dim_"+str(components_to_throw)+"thrown"))
        # save history
        path = join(save_path,"../ae_history")
        os.makedirs(path, exist_ok=True)
        f = open(join(path, "ae_3_dim_"+str(components_to_throw)+"thrown.pkl"),"wb")
        # write the python object (dict) to pickle file
        pickle.dump(history.history,f)
        # close file
        f.close()
        return "ok"
    return auto_encoder, data_out_pred, history.history

def fit_autoencoder(dic, data, time, n_components, verbose=2, save_path=None):
    auto_encoder = autoencoder.Autoencoder_model(original_dim = np.shape(data)[1],
                                                 latent_dim = n_components, n_hidden=dic["n_hidden"], dim_hidden=None)
    auto_encoder.compile(optimizer=keras.optimizers.Adam(learning_rate=dic["learning_rate"],
                                                         beta_1=dic["beta_1"],
                                                         beta_2=dic["beta_2"]),
                         loss='mean_squared_error')
    callback = keras.callbacks.EarlyStopping(monitor="val_loss", patience=dic["n_epochs"]/10, verbose=0,
                                mode="auto", baseline=None,
                                restore_best_weights=True,
                                start_from_epoch=dic["n_epochs"]/100)

    if dic["test_ae_training"] == True :
        index_train, index_test = _split1(data, time)
        data_train, data_test  = _split2(data, index_train, index_test)
        history = auto_encoder.fit(data_train, data_train,
                        epochs=dic["n_epochs"],
                        batch_size=dic["batch_size"],
                        shuffle=True, validation_data=(data_test,data_test),
                        verbose=verbose, callbacks=[callback])
    else :
        history = auto_encoder.fit(data, data,
                        epochs=dic["n_epochs"],
                        batch_size=dic["batch_size"],
                        shuffle=True,
                        verbose=verbose, callbacks=[callback])
    data_ae = auto_encoder.encoder.predict(data, verbose=0)
    if save_path != None :
        # save model 
        auto_encoder.save(join(save_path, "ae_"+str(n_components)+"_dim"))
        # save history
        path = join(save_path,"../ae_history")
        os.makedirs(path, exist_ok=True)
        f = open(join(path, "ae_"+str(n_components)+"_dim.pkl"),"wb")
        # write the python object (dict) to pickle file
        pickle.dump(history.history,f)
        # close file
        f.close()
        return "ok"
    return auto_encoder, data_ae, history.history

def fit_vae(data, dic):
    vae_cont = vae.Variational_autoencoder_contener(original_dim = np.shape(data)[1],
                                                    intermediate_dim = 40,
                                                    latent_dim = 3)
    callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)
    history = vae_cont.vae.fit(data, data, # validation_data=(x_test, x_test),
                      shuffle = True,
                      epochs=dic["n_epochs"],
                      batch_size=dic["batch_size"], 
                      callbacks=[callback])
    
    data_vae = vae_cont.encoder.predict(data)
    return vae_cont, data_vae
    
def fit_PCA(data, n_components=3):
    pca = PCA(n_components)
    pca.fit(data)
    data_pca = pca.fit_transform(data)
    return pca, data_pca

def compress_decompress(model, data, dic, dim=None):
    if dic["model_type"] == "pca":
        compress = model.transform(data.reshape(1,-1))
        decompress = model.inverse_transform(compress)
    elif dic["model_type"] == "ae":
        if dic["fit_ae_influence_LMP"] == False : decompress = model.predict(data, verbose=0)
        else : 
            data = np.delete(data, dim, axis=1)
            decompress = model.predict(data, verbose=0)
    return decompress[0]

####################################################################################
####################################### MAIN #######################################
####################################################################################
    
def main(dic, folder, path_origin, norm=None, verbose=0):
    dual_var, dic_dual, variables, var_names, time, days = import_data(folder, path_origin, "train")
    
    # ------ Only on LMP ------ #
    LMP_data = dual_var[:,dic_dual["active power balance"]["id_dual"]]
    if norm :
        scaler = preprocessing.StandardScaler().fit(LMP_data)
        LMP_scalled = scaler.transform(LMP_data)
    
        # Save scaler
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/scaler")
        os.makedirs(save_path, exist_ok=True)
        pickle.dump(scaler, open(join(save_path,'scaler.pkl'),'wb'))
        
    if dic["fit_pca"] :
        # Save PCA
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/pca")
        os.makedirs(save_path, exist_ok=True)
        print("FIT PCA")
        for dim in tqdm(range(LMP_scalled.shape[1]+1)):
            pca, _ = fit_PCA(LMP_scalled, n_components=dim)
            imp.save_model(save_path, model_name="pca_"+str(dim)+"_dim", model=pca)
    if dic["fit_ae"] :
        # Save auto-encoder
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae")
        os.makedirs(save_path, exist_ok=True)
        print("FIT Auto-Encoder")
        for dim in tqdm(dic["reduction_dim_list"][::-1]):
            fit_autoencoder(dic, LMP_scalled, time, dim, verbose, save_path)
        # Parallel(n_jobs=dic["n_jobs"])(delayed(fit_autoencoder)(dic, LMP_scalled, time, dim, verbose, save_path) for dim in tqdm(dic["reduction_dim_list"]))
    if dic["fit_ae_influence_LMP"]:
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae_influence")
        os.makedirs(save_path, exist_ok=True)
        print("FIT Auto-Encoder influence LMP")
        Parallel(n_jobs=dic["n_jobs"])(delayed(fit_autoencoder_special)(dic, LMP_scalled, time, comp, verbose, save_path) for comp in tqdm(dic["reduction_dim_list"]))

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman",3)
    norm=True
    dic = initialize._init_dic()
    main(dic, folder = folder, path_origin = path_origin, norm = norm, verbose=1)
    