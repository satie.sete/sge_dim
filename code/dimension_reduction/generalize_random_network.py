# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 10:01:15 2023

@author: Guenole

Purpose of this file :
    Create a set of random network and apply the same routine as in "generate_data.py".
    The aim is to generalize results obtained on the Eu network.
    In particular :
        - Plot the evolution of sub-obtimality
        - Investigate whether or not, clusters found in the latent space of the Eu network are general or not
"""
# Standard library
import os
from os.path import join
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import os
import numpy as np
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize
import code_these.dimension_reduction.dimension_reduction as dimension_reduction
from code_these.dimension_reduction.generate_data import generate_networks, generate_data, launch_julia

def get_dic(n_dict, test_code=False):
    dic_array = [initialize._init_dic() for k in range(n_dict)]
    for k in range(n_dict):
        dic_array[k]["rs"] =  RandomState(MT19937(SeedSequence(k)))
        dic_array[k]["fit_ae"] = False
        dic_array[k]["model_type"] = "pca"
        dic_array[k]["network"] = "random_BT"
        dic_array[k]["apt_p_kw"] = 700
        initialize._refresh(dic_array[k])
    if test_code :
        for k in range(n_dict) :
            dic_array[k]["reduction_dim_list"] = [1,40,129]
            dic_array[k]["all_days"] = np.arange(0,365,1)
            dic_array[k]["time_array"] = np.arange(0, 24*60, 30)
            initialize._refresh(dic_array[k])
    return dic_array

def routine(dic, path_origin, folder, linux):
    generate_networks(path_origin, folder, dic)
    print("------------------ Generate data set ------------------")
    generate_data(folder, path_origin, dic, linux, split_set="train")
    print("------------------ Fit models ------------------")
    dimension_reduction.main(dic, folder, path_origin, norm=True)
    print("------------------ Test models ------------------")
    generate_data(folder, path_origin, dic, linux, split_set="test")

def main(path_origin, folder, linux, n_dict, test_code=False):
    """Create a set of random network and apply the same routine as in "generate_data.py".
    The aim is to generalize results obtained on the Eu network."""
    # Create a dictonary to remember of parameter used during the simulation.
    dic_array = get_dic(n_dict, test_code=test_code)
    for k in range(n_dict):
        routine(dic_array[k], path_origin, join(str(folder),str(k)), linux)
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman", 0)
    n_dict = 100
    test_code = False
    # Other parameters
    linux = True
    launch_julia(linux)
    # Compute
    main(path_origin, folder, linux, n_dict, test_code)