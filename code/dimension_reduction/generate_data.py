#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: gcherot

Compute OPF on a test case and store dual and primal value.
The simulation is on the hole year.
"""
import sys
from os.path import dirname
sys.path.append("/home/gcherot/")


# Standard library
import os
from os.path import join
import pandapower as pp
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from tqdm import tqdm
from scipy.io import savemat
import os
import pickle
import keras
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize
import code_these.src._modify_net as modify_net
import code_these.test_case_generation.create.main as create_network
import code_these.src._SOCWR_tools as SOCWR_tools
import code_these.dimension_reduction.import_func as import_func
import code_these.dimension_reduction.dimension_reduction as dimension_reduction
from code_these.dimension_reduction.dimension_reduction import compress_decompress
from code_these.dimension_reduction import autoencoder

def setup_network(folder, path_origin):
    """Import and initialize networks.
    return
    ---------
    net_gather_data : network on which the we will gather OPF data to train the neural network and/or the dimension reduction
    net_pf : network used to test the control using nodal prices
    """
    net_opf = import_func.import_net(folder=join(str(folder), ""), path_origin=path_origin) # Import saved network
    net_opf = modify_net.init_net(net_opf, True) # Set initial values from tables
    
    net_pf = import_func.import_net(folder=join(str(folder), ""), path_origin=path_origin) # Import saved network
    net_pf = modify_net.init_net(net_pf, True) # Set initial values from tables
    net_pf = modify_net.lossen_bounds(net_pf) # Lossen bound so that OPF can be computed (OPF used as PF)
    return net_opf, net_pf

def setup_folders(folder, path_origin):
    """Create all folders needed to save data."""
    # Path where data is located
    data_path = join(path_origin,"processed/data")
    # Generate the folder in which to save the simulation's results
    save_path = join(path_origin,"processed/use", str(folder), "data")
    path = join(save_path, "train/")
    names = setup_folders_data(path)
    path = join(save_path, "test/opf")
    names = setup_folders_data(path)
    path = join(save_path, "test/pf")
    os.makedirs(path, exist_ok=True)
    path = join(save_path, "../info/")
    os.makedirs(path, exist_ok=True)
    return data_path, save_path, names

def setup_folders_data(save_path):
    """Used by setup_folders."""
    os.makedirs(save_path, exist_ok=True)
    names = ["dual",
             "res_bus",
             "res_line",
             "res_trafo",
             "res_ext_grid",
             "res_load",
             "res_gen",
             "res_welfare"]
    for name in names :
        path = join(save_path,name)
        os.makedirs(path, exist_ok=True)
    return names

def get_eval_days(split_set, train_days, test_days, val_days):
    if split_set=="train" : eval_days = train_days
    elif split_set=="test" : eval_days = test_days
    elif split_set=="val" : eval_days = val_days
    return eval_days

def generate_data(folder, path_origin, dic, linux=False, split_set="train"):
    """Calls the parallel function that performs all the simulation."""   
    data_path, save_path, names = setup_folders(folder, path_origin)
    net_opf, net_pf = setup_network(folder, path_origin)
    day_error = []
    eval_days = get_eval_days(split_set, dic["train_days"], dic["test_days"], dic["val_days"])
    # Compute OPF in parallel fonctions
    # for d in eval_days :
    #     parallel_func(net_opf, net_pf, d, save_path, data_path, folder, names, dic, linux, split_set)
        # parallel_func_try(net_opf, net_pf, d, save_path, data_path, folder, names, dic, linux, split_set)
    
    Parallel(n_jobs=dic["n_jobs"])(delayed(parallel_func_try)(net_opf, net_pf, d, 
                                                save_path, data_path, 
                                                folder, names, dic, linux, 
                                                split_set) for d in eval_days)
    return folder, day_error

def get_n_dual(net):
    """Get the number of dual value of the simulation by computing an OPF.
    Parameters
    -----------
     - net : pandapower network
         Network that will be used for simulation.
    """
    pp.runpm(net, pm_model='SOCWRPowerModel')
    return len(net.dual)

def _choose_model(save_path , model_type, dim, dic):
    if model_type == "pca" :
        path = join(save_path, "dimension_reduction/pca")
        model_name = "pca_"+str(dim)+"_dim"
        model = imp.load_model(path, model_name)
    elif model_type == "ae":
        if dic["fit_ae_influence_LMP"] == False :
            path = join(save_path, "dimension_reduction/ae","ae_"+str(dim)+"_dim")
            model = keras.models.load_model(path, custom_objects={"CustomModel": autoencoder.Autoencoder_model})
        else :
            path = join(save_path, "dimension_reduction/ae_influence","ae_3_dim_"+str(dim)+"thrown")
            model = keras.models.load_model(path, custom_objects={"CustomModel": autoencoder.Autoencoder_model})
    return model

def parallel_func_try(net_opf, net_pf, day, save_path, data_path, folder, names, dic,
                      linux=False, split_set="train"):
    try :
        parallel_func(net_opf, net_pf, day, save_path, data_path, folder, names, dic, linux, split_set)
    except :
        print("Day could not be computed : "+str(day))

def parallel_func(net_opf, net_pf, day, save_path, data_path, folder, names, dic,
                  linux=False, split_set="train"):
    """Compute the OPF for a specific day (all time step of that day)""" 
        
    launch_julia(linux)
    power_agent = dic["global_scale"]*import_func.import_power(net_opf, data_path, day) # Import power from database
    agent_cp1, agent_cp2 = import_func.import_prices(net_opf, data_path, day, str(folder)) # Import costs from database
    time_array = dic["time_array"] # One step every 4h, simulation on one day
    time_step = len(time_array) # Number of time steps
    n_dual = get_n_dual(net_opf) # Number of dual variables
    dic_dual = SOCWR_tools.get_all_dual(net_opf.dual) # Dictionnary used to understand duals
    primal_array_opf = init_result_array(net_opf, it=time_step) # Array containing primal values obtained with the OPF
    dual_array_opf = np.zeros((time_step, n_dual)) # Array containing all dual values obtained with the OPF
    primal_array_pf = [init_result_array(net_opf, it=time_step) for k in range(len(dic["reduction_dim_list"]))]
    dual_array_pf = np.zeros((len(dic["reduction_dim_list"]), time_step, n_dual))
    for k, t in enumerate(time_array) :
        # Compute OPF on network with impedance under-estimated
        modify_net.set_poly_cost(net_opf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])
        modify_net.set_bounds_agents(net_opf, power_agent.iloc[:,int(t)])
        pp.runpm(net_opf, pm_model='SOCWRPowerModel') # AC OPF (Would prefert SOC bug does not take voltage into account)
        modify_net.compute_vm_pu(net_opf)
        _check_optimality(net_opf.dual["dual_value"], t, day, dic_dual)
        get_results(primal_array_opf, k, net_opf)
        dual_array_opf[k] = net_opf.dual["dual_value"] # Stores dual values
        
        if split_set == "test":
            modify_net.set_poly_cost(net_pf, agent_cp1.iloc[:,int(t)], agent_cp2.iloc[:,int(t)])
            for i, dim in enumerate(dic["reduction_dim_list"]) :
                # Compute PF on network using compressed prices
                model = _choose_model(save_path, dic["model_type"], dim, dic)
                scaler = pickle.load(open(join(save_path, "dimension_reduction/scaler", 'scaler.pkl'),'rb'))
                LMP = dual_array_opf[k, dic_dual["active power balance"]["id_dual"]] # Import LMP
                LMP_scaled = scaler.transform(LMP.reshape(1, -1))
                LMP_info_loss_scaled = compress_decompress(model, LMP_scaled, dic, dim=dim) # compress^-1(compress(LMP))
                LMP_info_loss = scaler.inverse_transform(LMP_info_loss_scaled.reshape(1,-1))[0]
                res_load, res_gen = modify_net.get_pmw_from_LMP(LMP_info_loss, net_opf, dic_dual) # Compute Power = f^-1(LMP)
                net_pf = modify_net.set_power(net_pf, res_load, res_gen, net_opf.res_ext_grid, False)
                pp.runpm(net_pf, pm_model="SOCWRPowerModel") # Run powerflow
                _check_optimality(net_pf.dual["dual_value"], t, day, dic_dual)
                modify_net.compute_vm_pu(net_pf)
                modify_net.compute_line_loading(net_pf) 
                get_results(primal_array_pf[i], k, net_pf)
                net_pf.dual.loc[dic_dual["active power balance"]["id_dual"], "dual_value"] = LMP_info_loss
                dual_array_pf[i,k] = net_pf.dual["dual_value"] # Stores dual values
    
    if split_set == "train":
        save(primal_array_opf, dual_array_opf, day, save_path, split_set, net_opf, names, time_array, dic_dual)
    elif split_set == "test":
        save(primal_array_opf, dual_array_opf, day, save_path, join(split_set, "opf"), net_opf, names, time_array, dic_dual)
        for i, dim in enumerate(dic["reduction_dim_list"]) :
            save_folder = join("test/pf", dic["model_type"], str(dim))
            setup_folders_data(join(save_path, save_folder))
            save(primal_array_pf[i], dual_array_pf[i], day, save_path, save_folder, net_pf, names, time_array, dic_dual)

def _check_optimality(dual, t, day, dic_dual):
    """Used to check on each simulation if the OPF has converged or not.
    To do this, we check if the dual values of SOC ralaxed cosntraints are != 0."""
    if (np.abs(dual[dic_dual["SOC relaxation"]["id_dual"]]) < 1).any() :
        print("Not optimal : time = ", t, " day = ", day)
        return False
    else : return True

def init_result_array(net, it = 24*60):
    """Initialise the array that will be saved.
    Especially, takes the shape of each dataframe to create an array with the good shape."""
    list_array = []
    sh = net.res_bus[["vm_pu","va_degree","p_mw","q_mvar"]].shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    sh = net.res_line.shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    sh = net.res_trafo.shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    sh = net.res_ext_grid.shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    sh = net.res_load.shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    sh = net.res_gen.shape
    list_array.append(np.zeros((it, sh[0] , sh[1])))
    list_array.append(np.zeros((it)))
    return list_array

def copy_shape(A):
    if type(A) is list:
        return [copy_shape(elem) for elem in A]
    else:
        return None

def get_results(list_array, time, net):
    """Store primal values in array"""
    list_array[0][time] = net.res_bus[["vm_pu","va_degree","p_mw","q_mvar"]].values
    list_array[1][time] = net.res_line.values
    list_array[2][time] = net.res_trafo.values
    list_array[3][time] = net.res_ext_grid.values
    list_array[4][time] = net.res_load.values
    list_array[5][time] = net.res_gen.values
    list_array[6][time] = net.res_cost
    return list_array

def save(result_array, dual_array, day, save_path, folder, net, names, time, dic_dual):
    """Save the results of one day of simulation."""
    save_results(result_array, day, join(save_path,folder), names[1:])
    save_dual_values(dual_array, day, join(save_path,folder))
    save_time(save_path, time)
    save_data_structure(save_path, net)
    save_dual_structure(save_path, dic_dual)

def save_dual_values(dual_array, day, save_path):
    file = join(save_path, "dual", str(day)+".npy")
    np.save(file, dual_array)

def save_time(save_path, time):
    """Used only to remember wich time stamps were used for the simulation"""
    np.savetxt(join(save_path,"time.csv"), time, delimiter=',')

def save_dual_structure(save_path, dic_dual):
    with open(join(save_path,'dic_dual.pkl'), 'wb') as f:
        pickle.dump(dic_dual, f)

def save_data_structure(save_path, net):
    """Saves the name of each componant of the saved array."""
    l1 = pd.Series(["vm_pu","va_degree","p_mw","q_mvar"], name='res_bus')
    l2 = pd.Series(net.res_line.columns, name='res_line')
    l3 = pd.Series(net.res_trafo.columns, name='res_trafo')
    l4 = pd.Series(net.res_ext_grid.columns, name='res_ext_grid')
    l5 = pd.Series(net.res_load.columns, name='res_load')
    l6 = pd.Series(net.res_gen.columns, name='res_gen')
    df = pd.concat([l1,l2,l3,l4,l5,l6], axis=1)
    df.to_csv(join(save_path, 'data_structure.csv'))

def get_days(path_origin, folder):
    """Get days used for train, test and validation."""
    save_path = join(path_origin, "processed/use", folder)
    days_names = ["all_days", "train_days", "test_days", "val_days"]
    days_array = len(days_names)*[None]
    for k in range(len(days_names)):
        path = os.path.join(save_path, "info", days_names[k]+".csv")
        days_array[k] = np.load(path, delimiter=',', fmt='%s')
    return days_array

def save_results(prim_array, day, save_path, names, file=".npy"):
    """Save result in .npy or .mat files."""
    for k, name in enumerate(names):
        path = join(save_path, name, str(day)+file)
        if file == ".npy":
            np.save(path, prim_array[k])
        elif file == ".mat":
            savemat(path, {"data":prim_array[k]})
    
def save_network_attribute(path_origin, folder, line_lenght, line_lenght_variance, repeat, ref):
    path = join(path_origin, "processed/use", str(folder))
    data = {'line_lenght':[line_lenght],
            'line_lenght_variance':[line_lenght_variance],
            'reference_network':[ref]}
    # Create DataFrame
    df = pd.DataFrame(data)
    df.to_csv(join(path,'info.csv'), index=False)

def save_dict_parameters(path_origin, ref, dic):
    # create a binary pickle file 
    path = join(path_origin, "processed/use", str(ref))
    f = open(join(path,"dict_parameters.pkl"),"wb")
    # write the python object (dict) to pickle file
    pickle.dump(dic,f)
    # close file
    f.close()

def test_results(path_origin):
    for k in np.arange(0,100):
        save_path = join(path_origin,"processed/use", str(k), "data")
        names = ["res_bus",
                 "res_line",
                 "res_trafo",
                 "res_ext_grid",
                 "res_load",
                 "res_gen",
                 "res_welfare"]
        for n in names :
            tilde = np.load(join(save_path,"data_ref_pf",n,"0.npy"))
            ref = np.load(join(save_path,"data_ref_opf",n,"0.npy"))
            s = np.sum((ref-tilde)**2)
            print(s)
        print()
    
def launch_julia(linux):
    if linux : # BUG only on Linux
        from julia.api import Julia
        jl = Julia(compiled_modules=False)
        
def generate_networks(path_origin, folder, dic):
    """Generate a network with unknown impedance and saves it in a folder."""
    create_network.main(dic, path_origin, folder, import_data=False)
    save_dict_parameters(path_origin, folder, dic)

def main(dic, path_origin, folder, linux=False):
    """Folder : wich network to test.
    path_origin : Where the network and data are located."""
    # generate_networks(path_origin, folder, dic)
    # errors_train = generate_data(folder, path_origin, dic, linux, split_set="train")
    # dimension_reduction.main(dic, folder, path_origin, norm=True)
    dic["model_type"] = "pca"
    errors_pca = generate_data(folder, path_origin, dic, linux, split_set="test")
    # dic["model_type"] = "ae"
    # errors_ae = generate_data(folder, path_origin, dic, linux, split_set="test")
    # return errors_train, errors_pca , errors_ae
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman", 4)

    # Create a dictonary to remember of parameter used during the simulation.
    dic = initialize._init_dic()
    
    # Other parameters
    linux = True
    launch_julia(linux)
    
    # Compute
    errors = main(dic, path_origin, folder, linux)
    