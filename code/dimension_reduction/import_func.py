#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Implement the function to fast import an environnement.
Imported through this mean respect the gym interface.
A RL can be set to learn how to maximize the reward.
"""
# Import
import os
from os.path import join
import pandapower as pp
import numpy as np
import pandas as pd

# Functions
def import_net(folder, path_origin="H:/data/"):
    """ Return the panda network.
    path_origin : Path to the data folder.
    folder : name of the specific folder (we generate a lot of test_case, each of them with a unique ID)
    """
    path_folder = join(path_origin, "processed/use", str(folder))
    panda_network = pp.from_pickle(join(path_folder,"net.p"))
    return panda_network

def split_train_test(rs, all_days, size=0.8):
    train_days = rs.choice(all_days, int(size*len(all_days)), replace = False)
    test_days = all_days[~np.isin(all_days, train_days)]
    return train_days, test_days

def split_train_test_val(rs, all_days):
    train_days, val_days = split_train_test(rs, all_days)
    train_days, test_days = split_train_test(rs, train_days)
    return train_days, test_days, val_days

def import_power(net, data_path, day):
    """Import agent power"""
    load_id = net.load.index[net.load.infinite_price == False]
    index = np.concatenate((load_id, net.gen.index, net.ext_grid.index))
    index.sort()
    data = pd.DataFrame(0, columns=np.arange(24*60), index=index)
    for k in net.agent_table.index:
        et = net.agent_table.loc[k, "et"]
        if et=="load": scale = net.scale_load
        elif et=="gen" : scale = net.scale_gen
        id_network = int(net.agent_table["id_network"][k])
        id_data = int(net.agent_table["id_data"][k])
        typ = net.agent_table["type"][k]
        path = join(data_path, typ, str(id_data), str(day)+".csv")
        data.loc[id_network] += scale*1e-3*np.loadtxt(path, delimiter=",") # Agregate all agents according to their id_network
    data = np.maximum(data, 0) # Power cannot be produced by load nor consumed by gen.
    return data

def import_prices(net, data_path, day, folder):
    index = np.concatenate((net.load.index, net.gen.index, net.ext_grid.index))
    index.sort()
    agent_cp1 = pd.DataFrame(columns=np.arange(24*60), # TODO set better time stamp
                               index=index)
    agent_cp2 = pd.DataFrame(columns=np.arange(24*60), # TODO set better time stamp
                               index=index)
    data_EPEX = import_EPEX(net, data_path, day)
    agent_price = import_agent_prices(data_path, folder)
    _affect_agent_price(agent_cp1, data_EPEX, net, agent_price["cp1"])
    _affect_agent_price(agent_cp2, 0, net, agent_price["cp2"])
    return agent_cp1, agent_cp2

def _affect_agent_price(df, data_EPEX, net, agent_price):
    df.loc[0] = data_EPEX  # Sell price defined by EPEX spot prices
    df.loc[1] = data_EPEX - 0.1*np.abs(data_EPEX) # Buy price is 10% lower the sell price. Ensures convexity.
    load_id = net.load.index[net.load.infinite_price == False]
    load_id_infinite = net.load.index[net.load.infinite_price == True]
    gen_id = net.gen.index
    df.loc[load_id] = np.asarray(24*60*[agent_price[load_id]]).T
    df.loc[load_id_infinite] = np.asarray(24*60*[agent_price[load_id]*3]).T
    df.loc[gen_id] = np.asarray(24*60*[agent_price[gen_id]]).T

def import_agent_prices(data_path, folder):
    path = join(data_path, "../use", str(folder), "agent_prices.csv")
    agent_price = pd.read_csv(path, delimiter=",")
    agent_price = agent_price.set_index("id_network")
    return agent_price

def import_EPEX(net, data_path, day):
    """Import agent power"""
    path = join(data_path, "EPEX", "2021", str(day)+".csv")
    data_EPEX = np.loadtxt(path, delimiter=",")
    return data_EPEX

def get_directories_path(dir_path):
    path_list = [x for x in os.walk(dir_path)] # Get all file name (around 45 solar panels)
    return path_list[0][1] # Directories are in the second column

def get_files_path(dir_path):
    path_list = [x for x in os.walk(dir_path)] # Get all file name (around 45 solar panels)
    lis = []
    for x in path_list[0][2] :
        if x.endswith("csv") :
            lis.append(x)
    return lis