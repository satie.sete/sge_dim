#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Does all the procedure to :
        1. Generate data using OPF
        2. Fit PCA
        3. Use the fitted PCA to generate new data and compute sub-optimality between normal OPF and PF with dimension reduction.
"""
import numpy as np
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
# Custom import
import src._import as imp
import test_case_generation.create.main as test_case_gen
import dimension_reduction.dimension_reduction as dim_red
import dimension_reduction.generate_data as gen_data
import dimension_reduction.plots.main_fig as fig
    
def main(folder, path_origin, rs, norm=None, linux=False, n_bus=50, power=200):
    test_case_gen.main(rs, import_data=False, folder = folder, path_origin = path_origin, network="LMP",
                        n_bus=n_bus, n_subnetwork=1, line_lenght = 0.03, line_lenght_variance = 0,
                        apt_p_kw=power, ev_percent=0.2, pv_percent=0.5)
    gen_data.main(folder = folder, path_origin = path_origin, validation=False, linux=linux)
    dim_red.main(folder = folder, path_origin = path_origin, norm = norm)
    gen_data.main(folder = folder, path_origin = path_origin, validation=True, linux=linux)
    fig.main(folder = folder, path_origin = path_origin, norm=None)

def loop_main():
    norm=None
    rs = RandomState(MT19937(SeedSequence(1)))
    n_test = 10
    n_bus = np.linspace(30,80,n_test, dtype=int)
    n_bus = n_bus[5:]
    power_apt = [200] #, 400]
    for i, power in enumerate(power_apt):
        for k, bus in enumerate(n_bus):
            path_origin , folder = imp.path_import("PC_Roman",k+10+i*n_test)
            main(folder = folder, path_origin = path_origin, rs=rs, norm = norm, linux=True, n_bus=bus, power=power)

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman",1)
    rs = RandomState(MT19937(SeedSequence(1)))
    main(folder = folder, path_origin = path_origin, rs=rs, norm = None, linux=True)