# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 16:33:38 2023

@author: Guenole
"""

import warnings
import pandas as pd
import numpy as np
from pandapower import topology
from os.path import join
import os
from sklearn import cluster, mixture
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import kneighbors_graph
import code_these.src._import as imp

def to_percent(data1, data2):
    return 100*(data1-data2)/(np.abs(data1)+np.abs(data2))

def _find_biggest_gap(data1, data2):
    gap = data1 - data2
    if len(gap.shape) == 2 : gap = np.abs(gap).mean(axis=1)
    df = pd.DataFrame(gap, columns=['gap'])
    df.sort_values(by=['gap'], inplace=True, ascending=False)
    return df

def find_biggest_voltage_gap(variables_exact, variables_approx, dim):
    voltage_exact = variables_exact[1][:,:,0]
    voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
    return _find_biggest_gap(voltage_exact, voltage_approx[dim])

def find_biggest_line_gap(variables_exact, variables_approx, dim):
    line_exact = variables_exact[2][:,:,-1]
    line_approx = np.asarray([x[2][:,:,-1] for x in variables_approx])
    return _find_biggest_gap(line_exact, line_approx[dim])

def find_biggest_p_load_gap(variables_exact, variables_approx, dim):
    line_exact = variables_exact[5][:,:,0]
    line_approx = np.asarray([x[5][:,:,0] for x in variables_approx])
    return _find_biggest_gap(line_exact, line_approx[dim])

def find_biggest_p_gen_gap(variables_exact, variables_approx, dim):
    line_exact = variables_exact[6][:,:,0]
    line_approx = np.asarray([x[6][:,:,0] for x in variables_approx])
    return _find_biggest_gap(line_exact, line_approx[dim])

def _create_df_distance(data_list, label_list, net):
    distances = topology.calc_distance_to_bus(net, 0)
    df = distances.to_frame()
    df = df.rename(columns = {0:'distances'})
    df.sort_index(inplace=True)
    for k, name in enumerate(label_list):
        df[name] = data_list[k]
    return df

def _constrains_cost(variables_exact, variables_approx):
    def _voltage(variables_exact, variables_approx):
        voltage_exact = variables_exact[1][:,:,0]
        voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
        cost_exact = np.sum(np.maximum(np.abs(voltage_exact - 1) -0.05, 0), axis=1)
        cost_approx = np.sum(np.maximum(np.abs(voltage_approx - 1) -0.05, 0), axis= 2)
        return cost_approx - cost_exact
    
    def _congestion(variables_exact, variables_approx):
        loading_exact = variables_exact[2][:,:,-1]
        loading_approx = np.asarray([x[2][:,:,-1] for x in variables_approx])
        cost_exact = np.sum(np.maximum(np.abs(loading_exact - 100), 0), axis=1)/100
        cost_approx = np.sum(np.maximum(np.abs(loading_approx - 100), 0), axis=2)/100
        return cost_approx - cost_exact
    
    cost_voltage = _voltage(variables_exact, variables_approx)
    cost_congestion = _congestion(variables_exact, variables_approx)
    tot = cost_voltage + cost_congestion
    return tot, cost_voltage, cost_congestion

def _get_data_x(variables_exact, net, typ):
    voltage = variables_exact[1][:,:,0]
    p_mw = variables_exact[1][:,:,2]
    if typ == "max_voltage" : data_x = voltage.max(axis=0)
    elif typ == "min_voltage" : data_x = voltage.min(axis=0)
    elif typ == "mean_voltage" : data_x = voltage.mean(axis=0)
    elif typ == "max_power" : data_x = p_mw.max(axis=0)
    elif typ == "min_power" : data_x = p_mw.min(axis=0)
    elif typ == "mean_power" : data_x = p_mw.mean(axis=0)
    elif typ == "distance" : data_x = topology.calc_distance_to_bus(net, 0)
    else : data_x = np.arange(voltage.shape[1])
    return data_x

def get_LMP(dual_var_exact, dic_dual):
    LMP = dual_var_exact[:,dic_dual["active power balance"]["id_dual"]]
    return LMP

def get_LMP_latent(path_origin, folder, dual_var_exact, dic_dual, scaler, latent_dim=3):
    LMP = get_LMP(dual_var_exact, dic_dual)
    LMP_scaled = scaler.transform(LMP)
    save_path = join(path_origin,"processed/use", str(folder), "data")
    pca = imp.load_model(join(save_path, "dimension_reduction/pca"), "pca_"+str(latent_dim)+"_dim")
    LMP_latent = pca.transform(LMP_scaled)
    return LMP_latent

def get_label(path_origin, folder, dual_var_exact, clustering, dic_dual, scaler):
    LMP_latent = get_LMP_latent(path_origin, folder, dual_var_exact, dic_dual, scaler)
    clustering.fit(LMP_latent)
    labels = clustering.labels_
    return labels, LMP_latent

def stats_per_cluster(delta, n_clusters, labels, method="mean"):
    n_dim = len(delta)
    data = np.zeros((n_clusters, n_dim))
    n_data_per_clust = np.zeros(n_clusters)
    for k in range(n_clusters):
        ind = np.argwhere(labels==k)
        n_data_per_clust[k] = len(ind) 
        if method == "mean" : data[k] = np.mean(delta[:, ind], axis=1).flatten()
        if method == "max" : data[k] = np.max(delta[:, ind], axis=1).flatten()
    return data, n_data_per_clust
    
def compute_error(path_origin, folder, dual_var_exact, n_clusters, clustering, dic_dual, scaler, delta, norm=None):
    def _change_label(label, argsort):
        label_copy = np.zeros(label.shape)
        for new_l, old_l in enumerate(argsort):
            ind = np.where(label==old_l)
            label_copy[ind] = new_l
        return label_copy
    
    labels, LMP_latent = get_label(path_origin, folder, dual_var_exact, clustering, dic_dual, scaler)
    data, n_data_per_clust = stats_per_cluster(delta, n_clusters, labels)
    # Normalize
    n_data_per_clust = 100*n_data_per_clust/np.sum(n_data_per_clust)
    if norm == None :
        norm = np.max(data)
        data = 100*data/norm
    else : data = 100*data/norm
    argsort = np.argsort(n_data_per_clust)[::-1]
    labels = _change_label(labels, argsort)
    return data[argsort], n_data_per_clust[argsort], norm, LMP_latent, labels

def delta(typ, variables_exact, variables_approx, method="mean"):
    if typ == "voltage":
        voltage_exact = variables_exact[1][:,:,0]
        voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
        delta = voltage_approx - voltage_exact
        if method=="mean" : delta = np.mean(np.abs(delta), axis=2)
        if method=="max" : delta = np.max(np.abs(delta), axis=2)
    elif typ == "welfare":
        welfare_exact = variables_exact[-1]
        welfare_approx = np.asarray([x[-1] for x in variables_approx])
        delta = np.abs(welfare_approx - welfare_exact)
    return delta

def import_clustering(path_origin, folder, dual_var_exact, dic_dual, scaler, n_clusters):
    def _clustering_annex():
        pca = imp.load_model(join(save_path, "dimension_reduction/pca"), "pca_"+str(3)+"_dim")
        LMP = dual_var_exact[:,dic_dual["active power balance"]["id_dual"]]
        LMP_scaled = scaler.transform(LMP)
        LMP_latent = pca.transform(LMP_scaled)
        bandwidth = cluster.estimate_bandwidth(LMP_latent, quantile=0.3)
        connectivity = kneighbors_graph(LMP_latent, n_neighbors=3, include_self=False)
        return connectivity, bandwidth
    
    def _instance(clustering_method, n_clust):
        connectivity, bandwidth = _clustering_annex()
        if clustering_method == "K-moyennes":
            return cluster.KMeans(init="k-means++", n_clusters=n_clust, n_init=4)
        if clustering_method == "Birch":
            return cluster.Birch(n_clusters=n_clust)
        if clustering_method == "Regroupement hiérarchique":
            return cluster.AgglomerativeClustering(n_clusters=n_clust, linkage="ward", connectivity=connectivity)
        if clustering_method == "Partitionnement spectral":
            return cluster.SpectralClustering(n_clusters=n_clust, assign_labels='discretize')
        if clustering_method == "MeanShift":
            return cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
        if clustering_method == "AffinityPropagation":
            return cluster.AffinityPropagation(damping=0.75, preference=-220, random_state=0)
        if clustering_method == "GaussianMixture":
            return mixture.GaussianMixture(n_components=n_clust, covariance_type="full")
        
    save_path = join(path_origin,"processed/use", str(folder), "data")
    clustering = []
    for k, clustering_method in enumerate(n_clusters):
        for n_clust in n_clusters[clustering_method]:
            clustering.append({"clustering_method" : clustering_method,
                               "n_clust" : n_clust,
                               "instance" : _instance(clustering_method, n_clust)})
    return clustering

def import_n_clusters(n_clust, fig=1, all=True):
    n_clusters = {}
    if fig == 1 :
        n_clusters["Birch"] = [n_clust]
        n_clusters["K-moyennes"] = [n_clust]
        if all :
            n_clusters["Regroupement hiérarchique"] = [n_clust]
            n_clusters["Partitionnement spectral"] = [n_clust]
    else :
        n_clusters["Birch"] = [10, 30]
        n_clusters["K-moyennes"] = [30]
        if all :
            n_clusters["Regroupement hiérarchique"] = [30]
            n_clusters["Partitionnement spectral"] = [30]
    return n_clusters