#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Train a PCA and an auto-encoder to reduce the dimension of dual variable.
"""
# Standard libraries
import os
from os.path import join
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import numpy as np
from tqdm import tqdm
import pickle
# Custom import
import code_these.src._import as imp
import code_these.dimension_reduction.plots.plot_reduction as plot_reduction
import code_these.dimension_reduction.dimension_reduction as dim_red
import code_these.dimension_reduction.import_func as import_func
from code_these.dimension_reduction.generate_data import _check_optimality

def _import(folder, path_origin, dim=0, model_type="pca", get_pca=True):
    def _delete_not_opti(dual_var_exact, variables_exact, dual_var_array, variables_approx_array, days):
        voltage_exact = variables_exact[1][:,:,0]
        voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx_array])
        diff = np.mean(np.abs(voltage_exact-voltage_approx[-1]), axis=1)
        l_day = int(dual_var_exact.shape[0]/len(days))
        not_opti = []
        for k in range(dual_var_array[0].shape[0]): # For each time step
            if diff[k] > 0.05 :
                print("Not converged : time ", k%l_day, " day : ", days[k//l_day][:-4])
            keep = _check_optimality(dual_var_exact[k], k%l_day, days[k//l_day][:-4], dic_dual)
            if keep == False : not_opti.append(k)
            for dim in range(len(dual_var_array)): # For each dimension
                if keep == True :
                    keep = _check_optimality(dual_var_array[dim][k], k%l_day, days[k//l_day][:-4], dic_dual)
                    if keep == False : not_opti.append(k)
        for dim in range(len(dual_var_array)): # For each dimension
            dual_var_array[dim] = np.delete(dual_var_array[dim], not_opti, axis=0)
            variables_approx_array[dim] = [np.delete(x, not_opti, axis=0) for x in variables_approx_array[dim]]
        dual_var_exact = np.delete(dual_var_exact, not_opti, axis=0)
        variables_exact = [np.delete(x, not_opti, axis=0) for x in variables_exact]
        return dual_var_exact, variables_exact, dual_var_array, variables_approx_array
    
    dual_var_exact, dic_dual, variables_exact, var_names, time, days = dim_red.import_data(folder, path_origin, "test/opf")
    dual_var_array = []
    variables_approx_array = []
    dic = imp.load_dict(folder, path_origin, "../dict_parameters")
    for dim in tqdm(dic["reduction_dim_list"]):
        dual_var, dic_dual, variables, var_names, time, _ = dim_red.import_data(folder, path_origin, "test/pf/"+model_type+"/"+str(dim))
        dual_var_array.append(dual_var)
        variables_approx_array.append(variables)
    # Delete non_optimal solutions
    dual_var_exact, variables_exact, dual_var_array, variables_approx_array = _delete_not_opti(dual_var_exact, variables_exact,
                                                                                               dual_var_array, variables_approx_array, days)
    # Get scaller
    save_path = join(path_origin,"processed/use", str(folder), "data")
    scaler = pickle.load(open(join(save_path, "dimension_reduction/scaler", 'scaler.pkl'),'rb'))
    # Get PCA
    if get_pca :
        n_dual = len(dic_dual["active power balance"]["id_dual"])-1
        pca_full = imp.load_model(join(save_path, "dimension_reduction/pca"), "pca_"+str(n_dual)+"_dim")
    else : pca_full = None
    return dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, pca_full, scaler

####################################################################################
####################################### MAIN #######################################
####################################################################################

def _plot_pca(folder, path_origin):
    dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, pca_full, scaler = _import(folder, path_origin, dim=0, model_type="pca")
    save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/pca")
    os.makedirs(save_path, exist_ok=True)
    dim_array = 100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"])
    dic["model_type"] = "pca"
    plot_reduction.plot_fig1(path_origin, folder, variables_exact, variables_approx_array, dim_array, pca=pca_full)
    plot_reduction.plot_LMP_time(scaler, dic_dual, dual_var_exact, dual_var_array, -1, dim_array,  ax=None, plot=False, bus=None)
    plot_reduction.plot_fig2(path_origin, folder, variables_exact, variables_approx_array, dim_array)
    plot_reduction.plot_fig3(path_origin, folder, variables_exact, variables_approx_array, dual_var_exact,
                             dic, dic_dual, dim_array, dim=0)
    plot_reduction.plot_fig3(path_origin, folder, variables_exact, variables_approx_array,
                             dual_var_exact, dic, dic_dual, dim_array, dim=6)
    net = import_func.import_net(folder=join(str(folder), ""), path_origin=path_origin) # Import saved network
    # Voltage distance PCC
    plot_reduction.plot_fig4(path_origin, folder, net, variables_exact, variables_approx_array, dic, dim=[0, 1, -1])
    # LMP components
    plot_type = ["distance", "max_voltage", "min_voltage", "mean_voltage", "max_power", "min_power", "mean_power"]
    for typ in plot_type :
        # Chaque composante
        plot_reduction.plot_PCA_componant_analysis(folder, path_origin, net, variables_exact, pca_full, typ=typ)
        # Heatmap
        plot_reduction.plot_PCA_componant_colormap(folder, path_origin, pca_full, variables_exact, net, typ ,norm=False)
    # for k in range(30):
    #     plot_reduction.plot_PCA_network_map(folder, path_origin, pca_full, net, dim=k, norm=True)
    plot_reduction.plot_PCA_multiple_network_map(folder, path_origin, pca_full, net, variables_exact, dim_array=[0,1,2,3], norm=True)
    plot_reduction.plot_importance_totale_PN_network(folder, path_origin, pca_full, variables_exact, net)
    plot_reduction.plot_PCA_circle(folder, path_origin, variables_exact, dual_var_exact, net, dic_dual, scaler)
    plot_reduction.plot_TKT(folder, path_origin, pca_full)
    plot_reduction.plot_cluster_performance(folder, path_origin, variables_exact, variables_approx_array, dual_var_exact, dim_array, [0,2,8,11,13], dic_dual, scaler, latent_dim=3)
    # LMP cluster in dim 3
    plot_reduction.plot_cluster_latent_LMP(folder, path_origin, dual_var_exact, dic_dual, scaler, 9)
    # Network state depending on LMP cluster
    plot_reduction.plot_state_cluster_LMP(folder, path_origin, dual_var_exact
                                          , variables_exact, variables_approx_array,
                                          dic_dual, scaler, [1,3,5,8])
    # Correlation between welfare gap and voltage violations
    plot_reduction.plot_correlation_welfare_volage_violation(folder, path_origin, variables_exact, variables_approx_array, dim_array=[0])
    # Plot mapping latent space
    plot_reduction.plot_cluster_latent_performance_dim(folder, path_origin, variables_exact, variables_approx_array, dual_var_exact,
                                                       dic["reduction_dim_list"], dic_dual, scaler, n_clust=8, latent_dim=3)
    # plot_reduction.plot_performance_dim_red_heterogene(folder, path_origin, variables_exact, variables_approx_array, dual_var_exact,
    #                                                    dic["reduction_dim_list"], dic_dual, scaler, n_clust=100, latent_dim=3)
    plot_reduction.plot_performance_dim_red_heterogene_bis(folder, path_origin, variables_exact, variables_approx_array, dual_var_exact,
                                                       dic["reduction_dim_list"], dic_dual, scaler, n_clust=100, latent_dim=3, method="mean")
    plot_reduction.plot_performance_dim_red_heterogene_bis(folder, path_origin, variables_exact, variables_approx_array, dual_var_exact,
                                                       dic["reduction_dim_list"], dic_dual, scaler, n_clust=100, latent_dim=3, method="max")
    plot_reduction.LMP_3D_perf(path_origin, folder, dual_var_exact, dic_dual, scaler, variables_exact, variables_approx_array, dic["reduction_dim_list"])
    
    return dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, pca_full, scaler
    
    
def _plot_ae(folder, path_origin, dual_var_approx_pca):
    dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, pca_full, scaler = _import(folder, path_origin, dim=0, model_type="ae")
    dic["model_type"] = "ae"
    save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae")
    os.makedirs(save_path, exist_ok=True)
    
    dim_array = 100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"])
    plot_reduction.plot_history_fit(path_origin, folder, dim=dic["reduction_dim_list"], dic=dic)
    plot_reduction.plot_fig1(path_origin, folder, variables_exact, variables_approx_array,
                             100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"]), pca=pca_full, title="ae",
                             scaler=scaler, dic_dual=dic_dual, dual_exact=dual_var_exact, dual_approx_list=[dual_var_approx_pca, dual_var_array],
                             label_list=["ACP", "Auto-encodeur"])
    plot_reduction.plot_fig2(path_origin, folder, variables_exact, variables_approx_array, 
                             100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"]), title="ae")
    plot_reduction.plot_fig3(path_origin, folder, variables_exact, variables_approx_array, dual_var_exact,
                             dic, dic_dual, dim_array, dim=dic["reduction_dim_list"][0], title="ae")
    
def _plot_ae_influence_LMP(folder, path_origin):
    dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, _, scaler = _import(folder, path_origin, dim=0, model_type="ae", get_pca=False)
    save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae")
    dic["model_type"] = "ae"
    os.makedirs(save_path, exist_ok=True)
    dim_array = 100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"])
    plot_reduction.plot_fig1(path_origin, folder, variables_exact, variables_approx_array,
                             100*np.asarray(dic["reduction_dim_list"])/max(dic["reduction_dim_list"]), pca=None, title="ae_influence",
                             scaler=scaler, dic_dual=dic_dual, dual_exact=dual_var_exact, dual_approx_list=[dual_var_array, dual_var_array],
                             label_list=["ACP", "Auto-encodeur"])

def main(folder, path_origin):
    dual_var_exact, variables_exact, dual_var_approx_pca, variables_approx_array_pca, dic, dic_dual, pca_full, scaler = _plot_pca(folder, path_origin)
    _plot_ae(folder, path_origin, dual_var_approx_pca)
    # _plot_ae_influence_LMP(folder, path_origin)
    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Gueno_win",4)
    # dual_var_exact, variables_exact, dual_var_array, variables_approx_array, dic, dic_dual, pca_full, scaler = _import(folder, path_origin, dim=0)
    # save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/pca")
    # os.makedirs(save_path, exist_ok=True)
    main(folder = folder, path_origin = path_origin)
    