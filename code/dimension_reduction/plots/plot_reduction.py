# -*- coding: utf-8 -*-
"""
Created on Mon Sep 19 19:49:52 2022

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Plot relative to dimension reduction.
"""
import plotly.express as px
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import warnings
import pandas as pd
import numpy as np
from pandapower import topology
from os.path import join
import os
import plotly.graph_objects as go
from sklearn import cluster, mixture
from sklearn.neighbors import kneighbors_graph
import matplotlib.gridspec as grd
import matplotlib as mpl
from matplotlib import cm
from matplotlib.patches import Circle
import pickle
# Custom libraries
import code_these.src._import as imp
from code_these.dimension_reduction.dimension_reduction import compress_decompress
from code_these.dimension_reduction.generate_data import _choose_model
import code_these.test_case_generation.plot.test_plot_from_scratch as custom_net_plot
import code_these.dimension_reduction.plots.compute_for_plot as compute

vmax_g = 1.1
vmin_g = 0.9

def _plot_bug(variables_exact, variables_approx):
    voltage_exact = variables_exact[1][:,:,0]
    voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
    diff = np.mean(np.abs(voltage_exact-voltage_approx[-1]), axis=1)
    plt.plot(diff)
    print(np.where(diff>0.1))

def _history_fit(path_origin, folder, dim, dic, ax, log, legend, ylabel):
    import matplotlib.ticker as mtick
    if dic["fit_ae_influence_LMP"] == False :
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae_history", "ae_"+str(dim)+"_dim.pkl")
    else :
        save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/ae_history", "ae_3_dim_"+str(dim)+"thrown.pkl")
    with open(save_path, "rb") as file_pi:
        history = pickle.load(file_pi)
    ax.plot(history["loss"], label="Objectif entrainement")
    try : ax.plot(history["val_loss"], label="Objectif validation")
    except : pass
    ax.set_xlabel("Epoch")
    ax.set_yscale('log')
    ax.xaxis.set_major_formatter(mtick.LogFormatterSciNotation())
    if log : ax.set_xscale('log')
    if legend : ax.legend()
    if ylabel : ax.set_ylabel("RMSE")
    ax.set(xlim=(0, len(history["loss"])))
    
def plot_history_fit(path_origin, folder, dim, dic):
    fig, axs = plt.subplots(len(dim),2, figsize=(6,3*len(dim)), dpi=300, sharey=True)
    for k, d in enumerate(dim) :
        _history_fit(path_origin, folder, d, dic, axs[k,0], False, True, True)
        _history_fit(path_origin, folder, d, dic, axs[k,1], True, False, False)
    save(folder, path_origin, 'history_fit')

def save(folder, path_origin, title, ext=".pdf"):
    # SAVE
    dir_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/plot")
    os.makedirs(dir_path, exist_ok=True)
    plt.savefig(join(dir_path, title+ext), bbox_inches="tight")
    print(join(dir_path, title+ext))
    plt.close()

def _plot_compare_obs(data1, data2, xticks, ax, label1=None, label2 =None,
                     ylabel=None, xlabel=None):
    N = len(data1)
    ind = np.arange(N)  # the x locations for the groups
    width = 0.35       # the width of the bars

    ax.bar(ind, data1, width, label=label1)
    ax.bar(ind + width, data2, width, label=label2)
    
    # add some text for labels, title and axes ticks
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(xticks)

def plot_voltage_f_distance_PCC(net, variables_exact, variables_approx, dic, time,
                                voltage_limits=False, dim=0, ax=None, legend=True, xlabel=True, markersize=1):
    voltage_exact = variables_exact[1][time,:,0]
    voltage_approx = np.asarray([x[1][time,:,0] for x in variables_approx])
    data_list = []
    label_list = []
    for d in dim[::-1]:
        data_list.append(voltage_approx[d])
        dim_percent = int(np.round(100*dic["reduction_dim_list"][d]/dic["reduction_dim_list"][-1], 0))
        label_list.append(" $\\widetilde{PN}$ : " + str(dim_percent) + " % des variables latentes")
    if ax == None : fig, ax = plt.subplots(1, figsize=(6,2.25), dpi=300)
    ax, df = _plot_X_f_distance_PCC(ax, net, data_list, label_list, limits=True, legend=legend, xlabel=xlabel, markersize=markersize)
    ax.plot([0, max(df["distances"])], [vmin_g, vmin_g], "r--", label="Valeur min et max")
    ax.plot([0, max(df["distances"])], [vmax_g, vmax_g], "r--")
    ax.set_ylabel("Tension (en p.u.)")
    def extremum(df, func):
        ex = 1
        for n in df.keys()[1:]:
            ex = func(ex, func(df[n]))
        return ex
    ymin = extremum(df, min)
    ymax = extremum(df, max)
    ax.set(xlim=(0, max(df["distances"])), ylim=(ymin-0.01, ymax+0.01))

def plot_pca_components_f_distance_PCC(folder, path_origin, net, pca, dim=0, link=False, ax=None, markersize=1):
    data_list = np.abs([pca.components_[dim]])
    data_list = (data_list.T/data_list.max(axis=1)).T # Norm 
    label_list = ["Composante "+str(dim)]
    if ax == None : fig, ax = plt.subplots(1, figsize=(6,2.25), dpi=300)
    ax, df = _plot_X_f_distance_PCC(ax, net, data_list, label_list, limits=True, link=link, legend=False, xlabel=False, norm=True, markersize=markersize)
    ax.set_ylabel("")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set(xlim = (0, 1), ylim=(0, 1.1))
    ax.text(0.1, 0.85, str(dim), color='blue')

def _plot_X_f_distance_PCC(ax, net, data_list, label_list, limits=False, link=True, legend=True, xlabel=True, norm=False, markersize=1):
    def _plot(name, k, link=True):
        color = plt.rcParams['axes.prop_cycle'].by_key()['color']
        ax.plot(df["distances"], df[name], ".", color=color[k], label=name, markersize=markersize)
        if link :
            for l_i in net.line.index:
                l = net.line.loc[l_i]
                ax.plot([df.loc[l.from_bus, "distances"], df.loc[l.to_bus, "distances"]],
                        [df.loc[l.from_bus, name], df.loc[l.to_bus, name]],
                        color=color[k])
            
    df = compute._create_df_distance(data_list, label_list, net)
    if norm : df.loc[:,"distances"] = df.loc[:,"distances"]/np.max(df.loc[:,"distances"])
    for k, name in enumerate(label_list[::-1]):
        _plot(name, k, link)
    
    if legend : ax.legend(loc="upper left")
    if xlabel : ax.set_xlabel("Distance au PCC (en km)")
    return ax, df

def plot_voltage(variables_exact, variables_approx, time):
    voltage_exact = variables_exact[1][:,:,0]
    voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
    fig, ax = plt.subplots(1, dpi=300)
    xticks = np.arange(len(voltage_exact[time]),1)
    _plot_compare_obs(voltage_exact[time], voltage_approx[0][time], xticks, ax, label1="Exact", label2 ="1 dimension",
                         ylabel="Tension du bus", xlabel="Numéro du bus")
    ax.set(ylim=(0.90, 1.1))

def plot_scatter_2D_descrete(ax, data_latent, label, dim0=0, dim1=1, ylabel=None, vmax=None):
    new_cmap = cm.get_cmap('turbo', vmax+1)
    sc = ax.scatter(data_latent[:,dim0], data_latent[:,dim1], c=label,
                    s=1, vmin=-0.5, vmax=vmax+0.5, cmap=new_cmap)
    if ylabel!=None : ax.set_ylabel(ylabel)
    ax.set_xticks([])
    ax.set_yticks([])
    return sc

def plot_scatter_2D_continous(ax, data_latent, label, dim0=0, dim1=1, ylabel=None, xlabel=None, vmin=None, vmax=None, text=None):
    from matplotlib import colors
    new_cmap = cm.get_cmap('viridis')
    sc = ax.scatter(data_latent[:,dim0], data_latent[:,dim1], c=label,
                    s=1, cmap=new_cmap, norm=colors.AsinhNorm(linear_width= 0.002, vmin=vmin, vmax=vmax))
    if ylabel!=None : ax.set_ylabel(ylabel)
    if xlabel!=None : ax.set_xlabel(xlabel)
    ax.set_xticks([])
    ax.set_yticks([])
    if text != None :
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        # place a text box in upper left in axes coords
        ax.text(0.05, 0.95, text, transform=ax.transAxes, fontsize=8,
                verticalalignment='top', horizontalalignment="left", bbox=props)
    return sc

def plot_scatter_3D(data, color, title, folder, path_origin):
    dir_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/plot")
    os.makedirs(dir_path, exist_ok=True)
    df = pd.DataFrame(data)
    df.reset_index(inplace=True)
    df["color"] = color
    fig = px.scatter_3d(df, x=0, y=1, z=2, color="color",
                        color_continuous_scale="viridis",
                        title="Dual prices compressed in 3D space using "+str(title))
    fig.update_traces(marker_size = 2)
    fig.write_html(join(dir_path,'scatter_3D_'+str(title)+'.html'), auto_open=False)
    
def plot_feature_importance(pca):
    plt.figure(dpi=300, tight_layout=True)
    plt.bar(np.arange(0, len(abs( pca.components_ [0] )), 1), abs( pca.components_ [0] ))
    plt.title("Importance of each dual")
    plt.xlabel("Dual number")
    plt.ylabel("Importance in the decomposition")
    
def plot_explained_variance(pca, title, ax=None, plot=False):
    """Plot the explained variance of a PCA a a function of the number of components."""
    cum_expl = 100*pca.explained_variance_ratio_.cumsum()
    if ax == None :
        fig, ax = plt.subplots(1, dpi=300, tight_layout=True)
        plot = True
    ax.semilogx(np.arange(1, len(cum_expl)+1,1), cum_expl, ".")
    ax.set_ylabel("Variance \n expliquée \n (en %)", rotation=0, ha='right', va="center")
    if plot == True:
        ax.set_title(title)
        ax.set_xlabel("Nombre de composantes")
        plt.plot()

def plot_RMSE(scaler, dic_dual, dual_exact, dual_approx_list, label_list, dim_array, ax):
    """Plot the explained variance of a PCA a a function of the number of components."""
    LMP_exact = dual_exact[:,dic_dual["active power balance"]["id_dual"]]
    LMP_scaled_exact = scaler.transform(LMP_exact)
    LMP_approx = []
    LMP_scaled_approx = []
    for i in range(len(dual_approx_list)):
        LMP_approx.append(np.asarray([dual_approx_list[i][k][:,dic_dual["active power balance"]["id_dual"]] for k in range(len(dual_approx_list[i]))]))
        LMP_scaled_approx.append(np.asarray([scaler.transform(LMP_approx[i][k]) for k in range(len(LMP_approx[i]))]))
    
    RMSE = [np.sqrt(np.mean(np.square(LMP_scaled_approx[k]-LMP_scaled_exact), axis=(1,2))) for k in range(len(LMP_scaled_approx))]

    for k in range(len(label_list)):
        ax.loglog(dim_array, RMSE[k], '.', label=label_list[k])
    ax.set_ylabel("Racine de l'ereur \n quadratique moyenne \n (normé)", rotation=0, ha='right', va="center")
    ax.set(xlim=(min(dim_array), max(dim_array)))
    ax.legend()
        
def plot_LMP_time(scaler, dic_dual, dual_var_exact, dual_var_approx, dim, dim_array, ax=None, plot=False, bus=None):
    LMP_exact = dual_var_exact[:,dic_dual["active power balance"]["id_dual"]]
    LMP_scaled_exact = scaler.transform(LMP_exact)
    LMP_approx = np.asarray([dual_var_approx[k][:,dic_dual["active power balance"]["id_dual"]] for k in range(len(dual_var_approx))])
    LMP_scaled_approx = np.asarray([scaler.transform(LMP_approx[k]) for k in range(len(LMP_approx))])
    
    if ax == None :
        fig, ax = plt.subplots(1, dpi=300, tight_layout=True)
    
    if bus == None :
        ax.plot(LMP_exact.flatten(), label="Exacte")
        ax.plot(LMP_approx[dim].flatten(), label="Compressé, "+str(dim_array[-1]) + "% dimension")
    else :
        ax.plot(LMP_exact[:,bus], label="Exacte")
        ax.plot(LMP_approx[dim][:, bus], label="Compressé, "+str(dim_array[-1]) + "% dimension")
    ax.legend()
    ax.set_xlabel("temps")
    ax.set_ylabel("Prix nodal normé")
    

def plotly_plot_LMP_evolution_surface(data, day, folder, path_origin):
    """Plot the evolution of LMP as a function of time and bus id.
    The result is stored in a html file."""
    fig = go.Figure(data=[go.Surface(z=data)])
    fig.update_layout(title='LMP evolution during day '+str(day), autosize=False,
                      scene = dict(xaxis_title='Bus',
                                   yaxis_title='Time (h)',
                                   zaxis_title='LMP (€/MWh)',
                                   yaxis = dict(tickmode='array',
                                                ticktext=np.arange(0, 24, 4, dtype=int),
                                                tickvals=np.arange(0, data.shape[0], int(data.shape[0]/6), dtype=int))),
                      width=1500, height=700,
                      margin=dict(l=0,r=0,t=100,b=0),
                      scene_camera=dict(eye=dict(x=2.0, y=2.0, z=0.75)))
    # SAVE
    dir_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/plot/LMP_evolution")
    os.makedirs(dir_path, exist_ok=True) # Create folder if does not exist
    fig.write_html(join(dir_path,'LMP_evolution_day_'+str(day)+'.html'), auto_open=False)
    
def plt_plot_LMP_evolution_surface(data, day, folder, path_origin):
    """Plot the evolution of LMP as a function of time and bus id.
    The result is stored in a png file."""
    fig = plt.figure(dpi=300)
    ax = plt.axes(projection='3d')
    X = np.linspace(0, 24, data.shape[0])
    Y = np.arange(0, data.shape[1], 1)
    X, Y = np.meshgrid(X, Y)
    ax.plot_surface(X, Y, data.T ,cmap='viridis', edgecolor='none', antialiased=True)
    ax.set_xlim(0, 24)
    ax.set_ylim(0, data.shape[1]-1)
    ax.set_yticks(np.linspace(0, data.shape[1]-1, 5, dtype=int))
    ax.set_xlabel("Time (h)")
    ax.set_ylabel("Bus")
    ax.set_zlabel("LMP (€/MWh)")
    ax.set_title('LMP evolution, day '+str(day))
    # SAVE
    save(folder, path_origin, 'LMP_evolution_day_'+str(day))
    plt.show()
    
def plot_welfare_difference(variables_exact, variables_approx, dim):
    """Plot the welfare difference between the OPF and the result of a PF using the reconstructed prices."""
    welfare_exact = variables_exact[-1]
    welfare_approx = variables_approx[-1]
    delta_welfare = welfare_exact-welfare_approx
    plt.plot(delta_welfare)
    plt.xlabel("time")
    plt.ylabel("welfare exact - welfare approx")
    plt.title("Welfare comparison using PCA dim "+str(dim))
    plt.show()
    
    plt.plot(welfare_exact)
    plt.xlabel("time")
    plt.ylabel("welfare exact")
    plt.title("Welfare comparison using PCA dim "+str(dim))
    plt.show()
    
def _decile(data, ax, var, display_decile,
            display_median, display_mean, display_max, display_legend,
           xlabel, ylabel, title, symlog=0, y_legend=0, xlog=True, ylim_min=0, x_legend=1,
           color="b", fill=True):
    # Plot
    n_percentile = 10
    percent = np.linspace(0,100*(1-1/n_percentile), n_percentile)[1:]
    n_percentile = len(percent)
    n_var = len(var)
    data_plot = np.zeros((n_var, n_percentile))
    data_max = np.zeros((n_var))
    data_min = np.zeros((n_var))
    for k in range(n_var):
        data_plot[k] = np.percentile(data[k], percent)
        data_max[k] = np.max(data[k])
        data_min[k] = np.min(data[k])
    if display_decile :
        for k in range(n_percentile):
            if fill :
                trace = ax.fill_between(var, data_plot[:,k], data_plot[:, n_percentile-1-k],
                                        color=color, alpha = 1/n_percentile, linewidth=0)
            else :
                trace = ax.plot(var, data_plot[:,k], color=color, linewidth=2*k/10)[0]
        trace.set_label("Décile")
    if display_median : ax.plot(var, data_plot[:,int(n_percentile/2)], "b", alpha= 0.5, label="Médiane")
    if display_mean : ax.plot(var, np.mean(data_plot, axis=1), "r", alpha= 0.5, label="Moyenne")
    if xlog : ax.set_xscale('log')
    if display_max :
        ax.plot(var, data_min, "--", color=color, alpha=0.2, label="Valeurs min max")
        ax.plot(var, data_max, "--", color=color, alpha=0.2)
    if symlog != 0 :
        ax.set_yscale('symlog', linthresh=symlog)
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
    
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel, rotation=0, ha='right', va="bottom") # va="center" / "bottom"
    ax.set_title(title)
    if display_legend : ax.legend(bbox_to_anchor=(x_legend, y_legend), loc='upper right')
    ax.set(xlim=(min(var), max(var)), ylim=(ylim_min, None))
    return ax

def _count_voltage_violation(variables_approx):
    voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
    v_max = np.where(voltage_approx - (vmax_g + 0.001) > 0)
    v_min = np.where(voltage_approx - (vmin_g - 0.001) < 0)
    data = np.zeros(voltage_approx.shape)
    data[v_max] = 1
    data[v_min] = 1
    data = 100*data.sum(axis=2)/data.shape[2]
    return data
    
def find_biggest_voltage_violation_gap(variables_exact, variables_approx, dim):
    violation = _count_voltage_violation(variables_approx)[dim]
    zeros = np.zeros(violation.shape)
    return compute._find_biggest_gap(violation, zeros)

def plot_fig1(path_origin, folder, variables_exact, variables_approx, dim_array, pca, title="pca",
              scaler=None, dic_dual=None, dual_exact=None, dual_approx_list=None, label_list=None):
    """Plot the loss in wellfare depending on the dimension of the latent space."""
    def Fig_welfare(variables_exact, variables_approx, ax, y_legend):
        welfare_exact = variables_exact[-1]
        welfare_approx = np.asarray([x[-1] for x in variables_approx])
        cost_constrains, _, _ = compute._constrains_cost(variables_exact, variables_approx)
        welfare_approx += 0 # cost_constrains
        data = 200*(welfare_exact-welfare_approx)/(np.abs(welfare_exact)+np.abs(welfare_approx))
        data = np.abs(data)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = True,
                    xlabel="", ylabel="Différence \n coût total (en %)",
                    title="", symlog=1, x_legend=1.5, y_legend=y_legend, xlog=False)
    
    def Fig_voltage(variables_exact, variables_approx, ax):
        voltage_exact = variables_exact[1][:,:,0]
        voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
        data = voltage_exact-voltage_approx
        data = np.abs(data)#.mean(axis=2)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = False,
                    xlabel="", ylabel="Différence de\ntension \n (en p.u.)",
                    title="", symlog=0.01, xlog=False)
        
    def Fig_violation(variables_approx, ax, xlabel):
        data = _count_voltage_violation(variables_approx)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = False,
                    xlabel=xlabel,
                    ylabel="Fréquence des \n violations de \n tension (en %)",
                    title="", symlog=1, xlog=False)
        
    # Plot
    if title=="ae_influence" :
        fig, axs = plt.subplots(3, figsize=(5,5), dpi=300, sharex=True)
        n=-1
        y_legend = 1
        xlabel="Indice dimension normalisée \n (en %)"
    else :
        fig, axs = plt.subplots(4, figsize=(5,5), dpi=300, sharex=True)
        n=0
        y_legend = 2.2
        xlabel="Nombre de dimensions normalisé \n (en %)"
        if title=="pca": plot_explained_variance(pca, "", ax=axs[0])
        elif title=="ae": plot_RMSE(scaler, dic_dual, dual_exact, dual_approx_list, label_list, dim_array, ax=axs[0])
    Fig_welfare(variables_exact, variables_approx, axs[1+n], y_legend)
    Fig_voltage(variables_exact, variables_approx, axs[2+n])
    Fig_violation(variables_approx, axs[3+n], xlabel)
    # fig.suptitle("Sous-optimalité : Réduction de dimension des prix nodaux", y =0.92)
    fig.subplots_adjust(wspace=0, hspace=0.1)
    # Save
    save(folder, path_origin, "fig1_"+title)
    
def plot_fig2(path_origin, folder, variables_exact, variables_approx, dim_array, title="pca"):
    def Fig_loading(variables_exact, variables_approx, ax):
        loading_exact = variables_exact[2][:,:,-1]
        loading_approx = np.asarray([x[2][:,:,-1] for x in variables_approx])
        data = loading_exact-loading_approx
        data = np.abs(data)#.mean(axis=2)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = False,
                    xlabel="", ylabel="Différence \n utilisation \n lignes (en %)",
                    title="", symlog=0)
        
    def Fig_power_gen(variables_exact, variables_approx, ax):
        p_mw_gen_exact = variables_exact[6][:,:,0]
        p_mw_gen_approx = np.asarray([x[6][:,:,0] for x in variables_approx])
        data = compute.to_percent(p_mw_gen_exact, p_mw_gen_approx)
        data = np.abs(data)#.mean(axis=2)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = True,
                    xlabel="", ylabel="Différence \n puissance \n consommateurs \n (en %)",
                    title="", symlog=0.1, x_legend=1.5, y_legend=2.2)
        
    def Fig_power_load(variables_exact, variables_approx, ax):
        p_mw_load_exact = variables_exact[5][:,:,0]
        p_mw_load_approx = np.asarray([x[5][:,:,0] for x in variables_approx])
        data = compute.to_percent(p_mw_load_exact, p_mw_load_approx)
        data = np.abs(data)#.mean(axis=2)
        _decile(data, ax, dim_array, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = False,
                    xlabel="Nombre de dimensions normalisé (en %)",
                    ylabel="Différence \n puissance \n producteurs \n (en %)",
                    title="", symlog=0)
    # Plot
    fig, axs = plt.subplots(3, figsize=(5,3.75), dpi=300, sharex=True)
    Fig_loading(variables_exact, variables_approx, axs[0])
    Fig_power_gen(variables_exact, variables_approx, axs[1])
    Fig_power_load(variables_exact, variables_approx, axs[2])
    # fig.suptitle("Sous-optimalité : Réduction de dimension des prix nodaux", y =0.92)
    fig.subplots_adjust(wspace=0, hspace=0.1)
    # Save
    save(folder, path_origin, "fig2"+title)
    
def plot_fig3(path_origin, folder, variables_exact, variables_approx, dual_exact,
              dic, dic_dual, dim_array, dim=0, title="pca"):
    def influence_on_LMP(func, ax, x_label, ylabel, lengend, xlog=True):
        df = func(variables_exact, variables_approx, dim)
        N_time_step = len(df)
        n_sep = 10
        decile = int(N_time_step/n_sep)
        data = []
        xticks = np.zeros(n_sep)
        for dec in np.arange(n_sep):
            index = df.index[dec*decile:(dec+1)*decile]
            LMP_error = np.zeros(len(index))
            for i, k in enumerate(index) :
                LMP = dual_exact[k, dic_dual["active power balance"]["id_dual"]] # Import LMP
                LMP_info_loss = compress_decompress(model, LMP, dic) # PCA^-1(PCA(LMP))
                LMP_error[i] = np.abs(200*(LMP-LMP_info_loss)/(np.abs(LMP)+np.abs(LMP_info_loss))).mean()
            data.append(LMP_error)
            xticks[dec] = df.iloc[(dec+1)*decile]
        _decile(data, ax, xticks, display_decile=True,
                    display_median=True, display_mean=True, display_max=True,
                    display_legend = lengend, x_legend=-0.12, y_legend=0.4,
                    xlabel=x_label, ylabel=ylabel,
                    title="", symlog=0, xlog=xlog, ylim_min=None)
    
    save_path = join(path_origin,"processed/use", str(folder), "data")
    model = _choose_model(save_path , title, dim, dic)
    fig, axs = plt.subplots(1,3, figsize=(8,1.8), dpi=300, sharey=True)
    influence_on_LMP(compute.find_biggest_voltage_gap, axs[0], "Différence tension \n (en p.u.)", "Amplitude erreur \n prix nodaux \n (en %)", True)
    influence_on_LMP(compute.find_biggest_line_gap, axs[1], "Différence utilisation lignes \n (en % du max)", "", False)
    influence_on_LMP(find_biggest_voltage_violation_gap, axs[2], "Fréquence violation tension \n (en %)", "", False, xlog=False)
    ############################### TODO ##############################
    text = str(_round(dim_array[dim],1))+"%"
    fig.subplots_adjust(wspace=0.05, hspace=0.5)
    if text != None :
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        # place a text box in upper left in axes coords
        axs[0].text(0.05, 0.95, text, transform=axs[0].transAxes, fontsize=8,
                verticalalignment='top', horizontalalignment="left", bbox=props)
    #######################################################################
    
    # Save
    save(folder, path_origin, "fig3_"+title+"_"+str(dic["reduction_dim_list"][dim])+"dim")
    
def plot_fig4(path_origin, folder, net, variables_exact, variables_approx, dic, dim=[0], title="pca"):
    df_gap = compute.find_biggest_voltage_gap(variables_exact, variables_approx, dim[0])
    observation = [int(0*len(df_gap)/100)+1, int(25*len(df_gap)/100)+1, int(50*len(df_gap)/100)+0]
    fig, axs = plt.subplots(len(observation), figsize=(6,2.25*len(observation)), dpi=300, sharex=True)
    legend = [False, False, True]
    for k in range(len(observation)):
        plot_voltage_f_distance_PCC(net, variables_exact, variables_approx, dic, df_gap.index[observation[k]],
                                    voltage_limits=True, dim=dim, ax=axs[k], legend=legend[k], xlabel=legend[k], markersize=5)
    fig.subplots_adjust(wspace=0, hspace=0.1)
    # Save
    save(folder, path_origin, "fig_tension_distance_PCC"+title)

def plot_cluster_latent_LMP(folder, path_origin, dual_var_exact, dic_dual, scaler, n_clusters=9):
    save_path = join(path_origin,"processed/use", str(folder), "data")
    pca = imp.load_model(join(save_path, "dimension_reduction/pca"), "pca_"+str(3)+"_dim")
    LMP = dual_var_exact[:,dic_dual["active power balance"]["id_dual"]]
    LMP_scaled = scaler.transform(LMP)
    LMP_latent = pca.transform(LMP_scaled)
    X = LMP_latent
    bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)
    connectivity = kneighbors_graph(
        X, n_neighbors=3, include_self=False)
    ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
    two_means = cluster.MiniBatchKMeans(n_clusters=n_clusters, n_init=3)
    ward = cluster.AgglomerativeClustering(
        n_clusters=n_clusters, linkage="ward", connectivity=connectivity
    )
    spectral = cluster.SpectralClustering(
        n_clusters=n_clusters,
        assign_labels='discretize')
    dbscan = cluster.DBSCAN(eps=0.3)
    optics = cluster.OPTICS(
        min_samples=7,
        xi=0.05,
        min_cluster_size=0.1,
    )
    affinity_propagation = cluster.AffinityPropagation(
        damping=0.75, preference=-220, random_state=0
    )
    gmm = mixture.GaussianMixture(
        n_components=n_clusters, covariance_type="full"
    )
    # average_linkage = cluster.AgglomerativeClustering(
    #     linkage="average",
    #     metric="cityblock",
    #     n_clusters=n_clusters,
    #     connectivity=connectivity,
    # )
    birch = cluster.Birch(n_clusters=n_clusters)
    
 
    clustering_algorithms = (
        # ("MiniBatch_KMeans", two_means),
        # ("Affinity_Propagation", affinity_propagation),
        # ("MeanShift", ms),
        ("Spectral_Clustering", spectral),
        # ("Ward", ward),
        # ("Agglomerative\nClustering", average_linkage),
        # ("DBSCAN", dbscan),
        # ("OPTICS", optics),
        ("BIRCH", birch),
        # ("Gaussian_Mixture", gmm),
    )
    for name, algorithm in clustering_algorithms:
        with warnings.catch_warnings():
            warnings.filterwarnings(
                "ignore",
                message="the number of connected components of the "
                + "connectivity matrix is [0-9]{1,2}"
                + " > 1. Completing it to avoid stopping the tree early.",
                category=UserWarning,
            )
            warnings.filterwarnings(
                "ignore",
                message="Graph is not fully connected, spectral embedding"
                + " may not work as expected.",
                category=UserWarning,
            )
            algorithm.fit(X)
        plot_scatter_3D(LMP_latent, algorithm.labels_, "latent_cluster"+name, folder, path_origin)
        
def plot_PCA_componant_analysis(folder, path_origin, net, variables_exact, pca, typ = ["distance"]):
    fig = plt.figure(constrained_layout=False, figsize=(7,2.4*len(typ)), dpi=300)
    subfig = fig.subfigures(len(typ), 1) # See : https://stackoverflow.com/questions/68552473/sharing-of-xlabel-across-gridspec-subplots-axes-partial-row
    for k, sub in enumerate(subfig):
        gs = sub.add_gridspec(nrows=3, ncols=10, wspace=0, hspace=0)
        for i in range(30):
            ax = subfig.add_subplot(gs[i])

def plot_PCA_componant_analysis(folder, path_origin, net, variables_exact, pca, typ = "distance"):
    def _plot(data_x, pca, dim):
        data_list = np.abs(pca.components_[dim])
        data_list = (data_list/data_list.max()) # Norm
        ax.plot(data_x, data_list, ".", markersize=1)
        ax.set_ylabel("")
        ax.set_xticks([])
        ax.set_yticks([])
        mi = min(data_x)
        ma = max(data_x)
        d = (ma-mi)/20
        ax.set(xlim = (mi-d, ma+d), ylim=(0, 1.1))
        ax.text(min(data_x)+d, 0.85, str(dim), color='blue')
    
    xlabel = {"distance" : "Distance PCC (normé)",
              "max_voltage" : "Tension maximum",
              "min_voltage" : "Tension minimum",
              "mean_voltage" : "Tension moyenne",
              "max_power" : "Puissance maximum",
              "min_power" : "Puissance minimum",
              "mean_power" : "Puissance moyenne",
              }
    
    fig, axs = plt.subplots(3, 10, figsize=(7,2.4), dpi=300, sharex=True, sharey=True)
    for k, ax in enumerate(axs.flat):
        if typ == "distance" : plot_pca_components_f_distance_PCC(folder, path_origin, net, pca, dim=k, link=False, ax=ax)
        else :
            data_x = compute._get_data_x(variables_exact, net, typ)
            _plot(data_x, pca, dim=k)
    fig.subplots_adjust(wspace=0, hspace=0)
    fig.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    plt.xlabel(xlabel[typ])
    plt.ylabel("Importance prix \n nodaux (normé)")
    # Save
    save(folder, path_origin, "ACP_composantes_PCC_"+typ)

def plot_PCA_componant_colormap(folder, path_origin, pca, variables_exact, net, typ, norm=False):
    def _plot1(pca, ax, variables_exact, net, typ, norm=False):
        components = np.abs(pca.components_)
        if norm : components = (components.T/components.max(axis=1)).T
        data_x = compute._get_data_x(variables_exact, net, typ)
        ind = np.argsort(data_x)
        htmp = components[:,ind]
        im = ax.imshow(htmp)
        # ax.invert_yaxis()
        ax.set_xlabel("Prix nodaux, espace initial")
        ax.set_ylabel("Composantes espace latent")
        return im, htmp
    
    def _plot2(components, ax):
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        ax.set_yticks([0,1])
        y = components.sum(axis=0)/max(components.sum(axis=0))
        x=np.arange(0, len(y),1)
        ax.plot(x,y,'k',lw=0.5)
        # ax.set_ylabel("Somme")
        plt.xlim(0,len(y))
        plt.ylim(0,1)
        
    fig = plt.figure(figsize=(5,5), dpi=300)
    gs = grd.GridSpec(2, 2, height_ratios=[1,10], width_ratios=[6,1], wspace=0.1)
    ax2 = plt.subplot(gs[2])
    im, components = _plot1(pca, ax2, variables_exact, net, typ, norm=norm)
    colorAx = plt.subplot(gs[3])
    cbar = plt.colorbar(im, cax = colorAx)
    if norm : cbar.set_label("Valeur des coefficients de la matrice \n (normé par ligne)")
    else : cbar.set_label("Valeur des coefficients de la matrice")#, rotation=90, va="top")
    ax = plt.subplot(gs[0])
    _plot2(components, ax)
    # Save
    save(folder, path_origin, "ACP_composantes_heatmap_"+typ)
    
def plot_PCA_network_map(folder, path_origin, pca, net, dim, norm=False):
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    components = np.abs(pca.components_)
    if norm : components = (components.T/components.max(axis=1)).T
    color = pd.DataFrame(components[dim], columns=["importance"])
    custom_net_plot._plot_bus(ax, net, color, "importance", "Importance relatives ACP", vmin=0, vmax=color.max())
    custom_net_plot._plot_line(ax, net)
    # custom_net_plot._plot_agent(fig, ax, net, size=0.4)
    ax.axis('off')
    ax.set_title("Composante "+str(dim))
    # Save
    save(folder, path_origin, "ACP_composantes_network")
    
def plot_PCA_multiple_network_map(folder, path_origin, pca, net, variables_exact, dim_array, norm=False):
    fig = plt.figure(figsize=(10,5), dpi=300)
    gs = grd.GridSpec(2, 5, height_ratios=[1,1], width_ratios=[1,1,0.05,0.3,1.4], wspace=0.1)
    components = np.abs(pca.components_)
    if norm : components = (components.T/components.max(axis=1)).T
    for k, i in enumerate([0,1,5,6]) :
        ax = plt.subplot(gs[i])
        dim = dim_array[k]
        color = pd.DataFrame(components[dim], columns=["importance"])
        _, im = custom_net_plot._plot_bus(ax, net, color, "importance", "Importance relatives ACP",
                                            vmin=0, vmax=color.max(), display_cbar=False)
        custom_net_plot._plot_line(ax, net)
        custom_net_plot._plot_agent(ax, net, 0.3, "PCC", y_translate=10)
        ax.axis('off')
        ax.set_title("Vecteur latent "+str(dim))
    colorAx = plt.subplot(gs[:,-3])
    cbar = plt.colorbar(im, cax=colorAx)
    cbar.set_label("Importance relative de chaque prix nodal")
    plot_PCA_network_stats(folder, path_origin, variables_exact, net, ax=plt.subplot(gs[0,-1]), typ="voltage")
    plot_PCA_network_stats(folder, path_origin, variables_exact, net, ax=plt.subplot(gs[1,-1]), typ="power")
    # Save
    save(folder, path_origin, "ACP_composantes_mutiple_network")
    
def plot_PCA_network_stats(folder, path_origin, variables_exact, net, ax=None, typ="voltage"):
    def _plot(label, vmin, vmax, ax=None):
        if ax == None : fig, ax = plt.subplots(figsize=(8,3), dpi=300)
        custom_net_plot._plot_bus(ax, net, color, "color", label, vmin, vmax)
        custom_net_plot._plot_line(ax, net)
        custom_net_plot._plot_agent(ax, net, 0.3, "PCC", y_translate=10)
        # custom_net_plot._plot_agent(ax, net, size=0.4)
        ax.axis('off')
    
    if typ=="voltage":
        voltage = compute._get_data_x(variables_exact, net, typ="mean_voltage")
        color = pd.DataFrame(voltage, columns=["color"])
        _plot("Tension moyenne (en p.u.)", vmin=color.min(), vmax=color.max(), ax=ax)
        ax.set_title("Tension moyenne")
    elif typ=="power":
        power = 1e3*compute._get_data_x(variables_exact, net, typ="mean_power")
        color = pd.DataFrame(power, columns=["color"])
        _plot("Puissance moyenne (en kW)", vmin=-6, vmax=6, ax=ax)
        ax.set_title("Puissance moyenne")

def LMP_3D_perf(path_origin, folder, dual_var_exact, dic_dual, scaler, variables_exact, variables_approx, dim_array):
    def _to_log(data):
        return np.log(data) + np.abs(np.min(np.log(data)))
    def _color(typ="perf", dim=0):
        delta_v = compute.delta("voltage", variables_exact, variables_approx)
        if typ=="perf":
            color = _to_log(delta_v[dim,:])
        if typ == "gradient" :
            color = np.gradient(delta_v, dim_array, axis=0).mean(axis=0)
        if typ == "gradient_2" :
            color = np.gradient(np.gradient(delta_v, dim_array, axis=0),dim_array, axis=0).mean(axis=0)
        return color
    def create_df(LMP_latent):
        columns = ["x", "y", "z", "color", "dim"]
        n_row, n_col = LMP_latent.shape
        df_list = []
        for dim, dim_c in enumerate(dim_array):
            data = np.zeros((n_row,n_col+2))
            data[:,:3]=LMP_latent
            data[:,3] = _color("perf", dim)
            data[:,4] = dim_c
            df_list.append(pd.DataFrame(data = data,
                              columns = columns))
        df = pd.concat(df_list)
        return df
    
    save_path = join(path_origin,"processed/use", str(folder), "data")
    pca = imp.load_model(join(save_path, "dimension_reduction/pca"), "pca_"+str(3)+"_dim")
    LMP = dual_var_exact[:,dic_dual["active power balance"]["id_dual"]]
    LMP_scaled = scaler.transform(LMP)
    LMP_latent = pca.transform(LMP_scaled)
    
    df = create_df(LMP_latent)
    # px.scatter(df, x="gdpPercap", y="lifeExp", animation_frame="year", animation_group="country",
    #        size="pop", color="continent", hover_name="country",
    #        log_x=True, size_max=55, range_x=[100,100000], range_y=[25,90])
    fig = px.scatter_3d(df, x="x", y="y", z="z", color="color",
                        color_continuous_scale="viridis",
                        animation_frame="dim")
    fig.update_traces(marker_size = 2)
    fig["layout"].pop("updatemenus") # optional, drop animation buttons
    dir_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/plot")
    fig.write_html(join(dir_path,'scatter_3D_perf.html'), auto_open=False)

def plot_state_cluster_LMP(folder, path_origin, dual_var_exact, variables_exact,
                           variables_approx, dic_dual, scaler, n_clusters=[1,3,6,9]):
    def _change_label(label, argsort):
        label_copy = np.zeros(label.shape)
        for new_l, old_l in enumerate(argsort):
            ind = np.where(label==old_l)
            label_copy[ind] = new_l
        return label_copy
    
    def _plot(ax, n_clust, delta_v, norm_v, delta_w, norm_w):
        clustering = cluster.Birch(n_clusters=n_clust)
        errors_v, n_data_per_clust, norm_v, LMP_latent, labels = compute.compute_error(path_origin, folder, dual_var_exact, n_clust, clustering, dic_dual, scaler, delta_v, norm_v)
        errors_w, _, norm_w, _, _ = compute.compute_error(path_origin, folder, dual_var_exact, n_clust, clustering, dic_dual, scaler, delta_w, norm_w)
        ax.bar(np.arange(n_clust)-width, errors_v[:,0], width, label="Tension")
        ax.bar(np.arange(n_clust), errors_w[:,0], width, label="Coût")
        ax.bar(np.arange(n_clust)+width, n_data_per_clust, width, label="Taille cluster")
        ax.plot([-1,max(n_clusters)],[100,100], "--", color="grey", linewidth=0.6)
        ax.tick_params(axis='y')
        ax.set_yscale('symlog', linthresh=10)
        ax.set(xlim=(-0.5, max(n_clusters)-0.5))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
        return norm_v, norm_w, LMP_latent, labels
    
    delta_v = compute.delta("voltage", variables_exact, variables_approx)
    delta_w = compute.delta("welfare", variables_exact, variables_approx)/100
    norm_v = None
    norm_w = None
    ax_ref = None
    width = 0.25
    
    fig = plt.figure(constrained_layout=False, figsize=(8,1.2*len(n_clusters)), dpi=300)
    (subfig_bl, subfig_br) = fig.subfigures(1, 2, wspace=0, width_ratios=[2, 1]) # See : https://stackoverflow.com/questions/68552473/sharing-of-xlabel-across-gridspec-subplots-axes-partial-row
    gs_l = subfig_bl.add_gridspec(nrows=len(n_clusters), ncols=1, wspace=0, hspace=0.1)
    gs_r = subfig_br.add_gridspec(nrows=len(n_clusters), ncols=3, wspace=0, hspace=0, width_ratios=[1.05,1,0.1])
    for k, n_clust in enumerate(n_clusters):
        ax = subfig_bl.add_subplot(gs_l[k], sharex=ax_ref, sharey=ax_ref)
        if ax_ref == None : ax_ref = ax
        norm_v, norm_w, LMP_latent, labels = _plot(ax, n_clust, delta_v, norm_v, delta_w, norm_w)
        ax.set_xticklabels([])
        ax_r0 = subfig_br.add_subplot(gs_r[k,0])
        plot_scatter_2D_descrete(ax_r0, LMP_latent, labels, dim0=0, dim1=1, ylabel="Axe 0", vmax=max(n_clusters)-1)
        ax_r1 = subfig_br.add_subplot(gs_r[k,1])
        cb = plot_scatter_2D_descrete(ax_r1, LMP_latent, labels, dim0=0, dim1=2, vmax=max(n_clusters)-1)
    
    ax_ref.legend()
    ax.set_xticklabels(np.arange(-1,n_clusters[-1], dtype=int))
    ax_r0.set_xlabel("Axe 1")
    ax_r1.set_xlabel("Axe 2")
    colorAx = subfig_br.add_subplot(gs_r[:,2])
    cbar = plt.colorbar(cb, cax = colorAx)
    cbar.set_label("N° du groupe")
    subfig_bl.supxlabel("N° du groupe", fontsize=10)
    subfig_bl.supylabel("Différence performances (en % par rapport à \n la valeur observée sur l'ensemble des données)",
                        fontsize=10, ha="right")
    subfig_br.supxlabel("Visualisation des groupes", fontsize=10)
    # Save
    save(folder, path_origin, "impact_cluster_performance")

def plot_correlation_welfare_volage_violation(folder, path_origin, variables_exact, variables_approx, dim_array):
    def _plot_difference(ax, data_v, xlabel, ylabel, color, xline=None):
        _plot(ax, data_v, color)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        if xline != None :
            ax.plot([xline, xline], [-100,100], '--', linewidth=1, color="grey")
    def _plot_violation(ax, data_v, color):
        _plot(ax, data_v, color)
        ax.set_xlabel("Fréquence violations (en %)")
    def _plot(ax, data_v, color):
        for dim in dim_array :
            ax.scatter(data_v[dim], delta_w[dim], s=0.5, c=color[dim], cmap="jet")
        ax.plot([min(data_v[dim])-0.01, max(data_v[dim])+0.01], [0,0], '--', linewidth=1, color="grey")
        ax.set_xlim(min(data_v[dim])-0.005, max(data_v[dim])+0.005)
        ax.set_ylim(min(delta_w[dim])-0.005, max(delta_w[dim])+0.005)
        
    welfare_exact = variables_exact[-1]
    welfare_approx = np.asarray([x[-1] for x in variables_approx])
    delta_w = welfare_approx - welfare_exact
    
    voltage_exact = variables_exact[1][:,:,0]
    voltage_approx = np.asarray([x[1][:,:,0] for x in variables_approx])
    delta_v = np.mean(voltage_approx - voltage_exact, axis=2)
    
    v_max = np.where(voltage_approx - (vmax_g + 0.001) > 0)
    v_min = np.where(voltage_approx - (vmin_g - 0.001) < 0)
    data = np.zeros(voltage_approx.shape)
    data[v_max] = 1
    data[v_min] = 1
    data_v = 100*data.mean(axis=2)
    color = data_v != 0

    fig, axs = plt.subplots(1, 3, figsize=(9,2), dpi=300, sharey=True)
    _plot_difference(axs[0], voltage_approx.min(axis=2), "Tension min (en p.u.)", "Différence coût", color, vmin_g)
    _plot_difference(axs[1], voltage_approx.max(axis=2), "Tension max (en p.u.)", "", color, vmax_g)
    _plot_violation(axs[2], data_v, color)
    fig.subplots_adjust(wspace=0.1, hspace=0)
    # Save
    save(folder, path_origin, "correlation_welfare_tension")

def plot_cluster_latent_performance_dim(folder, path_origin, variables_exact, variables_approx, dual_var_exact,
                                        dim_array, dic_dual, scaler, n_clust=8, latent_dim=3):
    def _plot(ax, clustering, norm_v, norm_w, cluster_method):
        errors_v, n_data_per_clust, norm_v, LMP_latent, labels = compute.compute_error(path_origin, folder, dual_var_exact,
                                                                               n_clust, clustering, dic_dual, scaler, 
                                                                               delta_v, norm_v)
        errors_w, _, norm_w, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                                  n_clust, clustering, dic_dual, scaler, delta_w, norm_w)
        
        c_bar = cm.get_cmap('viridis', len(dim_array))
        colors = c_bar.colors
        width = 0.8/len(dim_array)
        for k, dim in enumerate(dim_array):
            ax.bar(np.arange(n_clust)+k*width-0.4, errors_v[:,k], width, label=str(dim), color=colors[k])
        ax.plot([-1,n_clust],[100,100], "--", color="grey", linewidth=0.6)
        ax.set_yscale('symlog', linthresh=4)
        ax.set(xlim=(-0.5, n_clust-0.5))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
        ax.set_xticklabels([])
        ax.text(0, 150, cluster_method, color='black')
        ax.set_yticks([1, 10, 100, 400])
        ax.set_ylim([0, 500])
        return LMP_latent, labels
    
    delta_v = compute.delta("voltage", variables_exact, variables_approx)
    delta_w = compute.delta("welfare", variables_exact, variables_approx)/100
    _, _, norm_v, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                       1, cluster.Birch(n_clusters=1), dic_dual, scaler, delta_v, None)
    _, _, norm_w, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                       1, cluster.Birch(n_clusters=1), dic_dual, scaler, delta_w, None)
    
    n_clusters = compute.import_n_clusters(n_clust, fig=1)
    clustering = compute.import_clustering(path_origin, folder, dual_var_exact, dic_dual, scaler, n_clusters)
    
    fig = plt.figure(constrained_layout=False, figsize=(8,1.2*len(clustering)), dpi=300)
    (subfig_bl, subfig_br) = fig.subfigures(1, 2, width_ratios=[2, 1]) # See : https://stackoverflow.com/questions/68552473/sharing-of-xlabel-across-gridspec-subplots-axes-partial-row
    gs_l = subfig_bl.add_gridspec(nrows=len(clustering), ncols=3, wspace=0.02, hspace=0.1, width_ratios=[100,2,10])
    gs_r = subfig_br.add_gridspec(nrows=len(clustering), ncols=3, wspace=0, hspace=0, width_ratios=[1.05,1,0.1])
    
    for k in range(len(clustering)):
        ax = subfig_bl.add_subplot(gs_l[k,0])
        LMP_latent, labels = _plot(ax, clustering[k]["instance"], norm_v, norm_w, clustering[k]["clustering_method"])
        ax_r0 = subfig_br.add_subplot(gs_r[k,0])
        plot_scatter_2D_descrete(ax_r0, LMP_latent, labels, dim0=0, dim1=1, ylabel="Axe 0", vmax=n_clust-1)
        ax_r1 = subfig_br.add_subplot(gs_r[k,1])
        cb = plot_scatter_2D_descrete(ax_r1, LMP_latent, labels, dim0=0, dim1=2, vmax=n_clust-1)
    ax.set_xticklabels(np.arange(-1, n_clust,dtype=int))
    
    cmap = cm.viridis
    dim_array = np.asarray(dim_array)
    bounds = np.arange(len(dim_array)+1)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N, extend='neither')
    colorAx = subfig_bl.add_subplot(gs_l[:,1])
    cbar = fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap),
             cax=colorAx, orientation='vertical',
             ticks=bounds[:-1]+0.5,
             label="Nombre de variables latentes normalisées (en %)")
    cbar.ax.set_yticklabels(np.asarray(np.round(100*dim_array/max(dim_array),0), dtype=int))

    ax_r0.set_xlabel("Vecteur \n latent 1")
    ax_r1.set_xlabel("Vecteur \n latent 2")
    colorAx = subfig_br.add_subplot(gs_r[:,2])
    cbar = plt.colorbar(cb, cax = colorAx)
    cbar.set_label("N° du groupe")
    
    subfig_bl.supxlabel("N° du groupe", fontsize=10, y=-0.025)#, va="top")
    subfig_bl.supylabel("Différence tension (en % par rapport à \n la valeur observée sur l'ensemble des données)",
                        fontsize=10, ha="right", va="center")
    subfig_br.supxlabel("Visualisation groupes", fontsize=10, y=-0.025)#, va="top")
    
    # Save
    save(folder, path_origin, "Cartographie_espace_latent")

def plot_performance_dim_red_heterogene_bis(folder, path_origin, variables_exact, variables_approx, dual_var_exact, # TODO : A supprimé quand l'autre figure sera tracé
                                        dim_array, dic_dual, scaler, n_clust=8, latent_dim=3, method="mean"):
    def _data_heterogen_cluster(n_dim_per_cluster, delta, n_data_per_clust, dim_array, labels):
        data = np.zeros((len(n_dim_per_cluster), delta.shape[1]))
        dim_mean = np.sum(dim_array[n_dim_per_cluster]*(n_data_per_clust/np.sum(n_data_per_clust)), axis=1)
        for k in range(len(n_dim_per_cluster)):
            for l in labels :
                ind = np.argwhere(labels==l)
                data[k, ind] = delta[n_dim_per_cluster[k,l], ind]
        return data, dim_mean
    
    def _compute_n_dim_per_cluster(data, n_cluster):
        def _recursive(d):
            try : out[k] = np.asarray([min(np.argwhere(data[k,:] <= perf+d))[0] for k in range(n_cluster)])
            except : out[k] = _recursive(d*1.05)
            return out[k]
                
        perf_array = np.concatenate((np.logspace(-0.5,-2,30), np.linspace(0.01,0.0001,20)))
        out = np.zeros((len(perf_array), n_cluster))
        for k, perf in enumerate(perf_array):
            d = 0.0001
            out[k] = _recursive(d)
        return np.asarray(out, dtype=int)
    
    def _plot(ax, delta, dim_array, data_array, dim_mean):
        _decile(data_array, ax, dim_mean, display_decile=True,
                    display_median=False, display_mean=False, display_max=True, display_legend=False,
                   xlabel="Nombre variables latentes moyenne (en %)", ylabel="Différence tension \n (en p.u.)",
                   title="", symlog=0.005, y_legend=1, xlog=True, ylim_min=0, x_legend=1, color="green", fill=False)
        _decile(delta, ax, dim_array, display_decile=True,
                    display_median=False, display_mean=False, display_max=True, display_legend=False,
                   xlabel="Nombre variables latentes moyenne (en %)", ylabel="Différence tension \n (en p.u.)",
                   title="", symlog=0.005, y_legend=1, xlog=True, ylim_min=0, x_legend=1, fill=False)
        
        h = [plt.plot([], color="blue")[0], plt.plot([], color="green")[0]]
        legend2 = plt.legend(h, ["Homogène", "Hétérogène"], loc="upper right", bbox_to_anchor=(.999, 0.99))
        ax.add_artist(legend2)
        h = [plt.plot([], color="grey")[0], plt.plot([], "--", color="grey")[0]]
        legend2 = plt.legend(h, ["Décile", "Max"], loc="upper right", bbox_to_anchor=(.999, 0.7))
        ax.add_artist(legend2)
    
    delta_v = compute.delta("voltage", variables_exact, variables_approx)
    delta_w = compute.delta("welfare", variables_exact, variables_approx)/100

    # Compute performance
    dim_array = 100*np.asarray(dim_array)/max(dim_array)
    n_clusters = 30
    clustering = compute.import_clustering(path_origin, folder, dual_var_exact, dic_dual, scaler, {"Birch":[n_clusters]})[0]
    fig, ax = plt.subplots(1, figsize=(6,2.5), dpi=300)
    labels, _ = compute.get_label(path_origin, folder, dual_var_exact, clustering["instance"], dic_dual, scaler)
    data, n_data_per_clust = compute.stats_per_cluster(delta_v, n_clusters, labels, method)
    n_dim_per_cluster = _compute_n_dim_per_cluster(data, n_clusters)
    data_array, dim_mean = _data_heterogen_cluster(n_dim_per_cluster, delta_v, n_data_per_clust, dim_array, labels)
    
    _plot(ax, delta_v, dim_array, data_array, dim_mean)
    
    
    # Save
    save(folder, path_origin, "Performance_n_dim_heterogene_method_"+method)
    
def plot_performance_dim_red_heterogene(folder, path_origin, variables_exact, variables_approx, dual_var_exact, # TODO : A supprimer quand l'autre figure sera tracé
                                        dim_array, dic_dual, scaler, n_clust=8, latent_dim=3):
    def _compute_perf(clustering, norm_v, norm_w, n_cluster):
        errors_v, n_data_per_clust, norm_v, LMP_latent, labels = compute.compute_error(path_origin, folder, dual_var_exact,
                                                                               n_cluster, clustering, dic_dual, scaler, 
                                                                               delta_v, norm_v)
        errors_w, _, norm_w, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                                  n_cluster, clustering, dic_dual, scaler, delta_w, norm_w)
        return errors_v, errors_w, n_data_per_clust
    
    def _compute_n_dim_mean(errors_v, n_data_per_clust, perf_array, n_cluster):
        perf_array = np.linspace(500,2,100)
        n = len(perf_array)
        n_dim_mean = np.zeros(n)
        perf_mean = np.zeros(n)
        perf_max = np.zeros(n)
        for k, perf in enumerate(perf_array):
            d = 0.5
            dim = np.asarray([dim_array[min(np.argwhere(errors_v[k,:] <= perf+d))[0]] for k in range(n_cluster)])
            p =  np.asarray([errors_v[k, min(np.argwhere(errors_v[k,:] <= perf+d))[0]] for k in range(n_cluster)])
            perf_max[k] = max(p)
            n_dim_mean[k] = np.sum(n_data_per_clust*dim/100)
            perf_mean[k] = np.sum(n_data_per_clust*p/100)
        return n_dim_mean, perf_mean, perf_max
    
    
    def _plot(ax, data, n_dim_homo, perf_homo):
        def _color(clustering_method, shift=0):
            color = plt.rcParams['axes.prop_cycle'].by_key()['color']
            if clustering_method == "K-moyennes":
                return color[1+shift]
            if clustering_method == "Birch":
                return color[0+shift]
            if clustering_method == "Regroupement hiérarchique":
                return color[2+shift]
            if clustering_method == "Partitionnement spectral":
                return color[3+shift]
            
        ax.plot(n_dim_homo, perf_homo, linewidth = 2, label="Homogène", color="black")
        leg = [plt.plot([], linewidth = 2, color="black")]
        l = ["Homogène"]
        for k in range(len(data)):
            clustering_method = data[k]["clustering_method"]
            ax.plot(data[k]["n_dim_mean"], data[k]["perf_mean"],
                    "--", color=_color(clustering_method),
                    label=clustering_method)# + " " + str(data[k]["n_clusters"]) + " groupes")
            ax.plot(data[k]["n_dim_mean"], data[k]["perf_max"],
                    linestyle="dotted", color=_color(clustering_method))
            ax.set(xlim=(0.9,100), ylim=(0, None))
            leg.append(plt.plot([], color=_color(clustering_method)))
            l.append(clustering_method)
        ax.set_xlabel("Nombre de variables latentes (en %)") 
        ax.legend()
        ax.set_ylabel("Différence de tension \n (en % par rapport à une \n situation sans groupement)")
        ax.set_xscale('log')
        ax.set_yscale('symlog', linthresh=100)
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: '{:g}'.format(y)))
        ticks = np.concatenate((np.arange(0,100,10),np.arange(100,500,100)))
        labels = len(ticks)*[""]
        labels[5] = 50
        labels[10] = 100
        labels[13] = 400
        ax.set_yticks(ticks)
        ax.set_yticklabels(labels)
        h = [k[0] for k in leg]
        legend1 = plt.legend(handles=h, labels = l, loc="upper right")
        ax.add_artist(legend1)
        h = [plt.plot([], '--', color="grey")[0], plt.plot([], linestyle="dotted", color="grey")[0]]
        legend2 = plt.legend(h, ["Moyenne", "Maximum"], loc="upper right", bbox_to_anchor=(.999, 0.37))
        ax.add_artist(legend2)
    
    delta_v = compute.delta("voltage", variables_exact, variables_approx)
    delta_w = compute.delta("welfare", variables_exact, variables_approx)/100
    # Get norm
    errors_v_0, _, norm_v, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                       1, cluster.Birch(n_clusters=1), dic_dual, scaler, delta_v, None)
    errors_w_0, _, norm_w, _, _ = compute.compute_error(path_origin, folder, dual_var_exact,
                                       1, cluster.Birch(n_clusters=1), dic_dual, scaler, delta_w, None)
    # Compute performance
    dim_array = 100*np.asarray(dim_array)/max(dim_array)
    n_clusters = compute.import_n_clusters(30, fig=1, all=True)
    clustering = compute.import_clustering(path_origin, folder, dual_var_exact, dic_dual, scaler, n_clusters)
    data = []
    fig, ax = plt.subplots(1, figsize=(6,2.5), dpi=300)
    for k in range(len(clustering)):
        errors_v, errors_w, n_data_per_clust = _compute_perf(clustering[k]["instance"], norm_v, norm_w, clustering[k]["n_clust"])
        n_dim_mean, perf_mean, perf_max = _compute_n_dim_mean(errors_v, n_data_per_clust, errors_v_0[0], clustering[k]["n_clust"])
        data.append({"clustering_method" : clustering[k]["clustering_method"],
                   "n_dim_mean" : n_dim_mean,
                   "perf_mean" : perf_mean,
                   "perf_max" : perf_max,
                   "n_clusters" : clustering[k]["n_clust"]})
    
    _plot(ax, data, dim_array, errors_v_0[0])
    # Save
    save(folder, path_origin, "Performance_n_dim_heterogene")

def plot_network_time_t(folder, path_origin, variables_exact, net, t):
    voltage = variables_exact[1][:,:,0][t]
    color = pd.DataFrame(voltage, columns=["color"])
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    custom_net_plot._plot_bus(ax, net, color, "color", "Tension (en p.u.)",vmin=vmin_g, vmax=vmax_g)
    custom_net_plot._plot_line(ax, net)
    ax.axis('off')
    save(folder, path_origin, "network_tension_t"+str(t), ".png")
    
    loading_exact = variables_exact[2][:,:,-1][t]
    color = pd.DataFrame(loading_exact/max(loading_exact), columns=["color"])
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    custom_net_plot._plot_bus(ax, net)
    custom_net_plot._plot_line(ax, net, color, "color", "Utilisation lignes (en % du max)")
    ax.axis('off')
    save(folder, path_origin, "network_flux_t_"+str(t), ".png")
    
def plot_importance_totale_PN_network(folder, path_origin, pca, variables_exact, net):
    components = np.abs(pca.components_)
    exp_var = pca.explained_variance_
    weighted = np.matmul(exp_var, components)
    # y = 100*components.sum(axis=0)/max(components.sum(axis=0))
    y = 100*weighted/max(weighted)
    color = pd.DataFrame(y, columns=["color"])
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    custom_net_plot._plot_bus(ax, net, color, "color", "Importance somme pondérée \n prix nodaux normalisé (en %)",vmin=min(y), vmax=max(y))
    custom_net_plot._plot_line(ax, net)
    custom_net_plot._plot_agent(ax, net, 0.3, "PCC", y_translate=8)
    ax.axis('off')
    save(folder, path_origin, "importance_totale_PN_network", ".pdf")

def plot_history(hist):
    fig, ax = plt.subplots(figsize=(8,3), dpi=300)
    ax.plot(hist.history["loss"], label="Loss")
    ax.plot(hist.history["val_loss"], label="Validation loss")
    ax.legend()
    ax.set(xlim=(0, len(hist.history["loss"])))
    ax.set_yscale('log')
    ax.set_ylabel("Erreur")
    ax.set_xlabel("Nombre d'epoch")

def _round(number, r):
    if number < 10 :
        return np.round(number,r)
    else : return int(number)
    
def plot_cluster_performance(folder, path_origin, variables_exact, variables_approx, dual_var_exact,
                             dim_array, dim_ploted, dic_dual, scaler, latent_dim=3):
    delta_v = compute.delta("voltage", variables_exact, variables_approx, "mean")
    delta_w = compute.delta("welfare", variables_exact, variables_approx)/100
    LMP_latent = compute.get_LMP_latent(path_origin, folder, dual_var_exact, dic_dual, scaler, latent_dim)

    fig = plt.figure(constrained_layout=False, figsize=(1.2*len(dim_ploted),2.4), dpi=300)
    gs_r = fig.add_gridspec(nrows=3, ncols=len(dim_ploted), wspace=0, hspace=0, height_ratios=[1.05,1,0.1])

    for k, d in enumerate(dim_ploted):
        text = str( _round(dim_array[d],1))+"%"
        ax_r0 = fig.add_subplot(gs_r[0,k])
        plot_scatter_2D_continous(ax_r0, LMP_latent, delta_v[d], dim0=0, dim1=1, xlabel="Vecteur \n latent 0", vmin=0, vmax=np.max(delta_v))
        ax_r0.xaxis.set_label_position('top')
        ax_r1 = fig.add_subplot(gs_r[1,k])
        cb = plot_scatter_2D_continous(ax_r1, LMP_latent, delta_v[d], dim0=0, dim1=2, vmin=0, vmax=np.max(delta_v), text=text)
        if k == 0:
            ax_r0.set_ylabel("Vecteur \n latent 1")
            ax_r1.set_ylabel("Vecteur \n latent 2")
    import matplotlib as mpl

    colorAx = fig.add_subplot(gs_r[2,:])
    cbar = plt.colorbar(cb, cax = colorAx, ticks=[0,1e-3,0.01, 0.02, 0.05, 0.1,0.15], orientation='horizontal')
    cbar.set_label("Différence tension moyenne (en p.u.)")
    
    cbar.ax.set_xticklabels(np.asarray([0,1e-3,0.01, 0.02,0.05, 0.1,0.15], dtype=str))
    
    # Save
    save(folder, path_origin, "Performance_espace_latent")


def _PCA_circle(folder, path_origin, variables_exact, dual_var_exact, net, dic_dual, scaler, subfig, cmap_label, cmap_color):
    """Plot from https://nirpyresearch.com/pca-correlation-circle/"""
    LMP = -compute.get_LMP(dual_var_exact, dic_dual)
    n_latent = LMP.shape[1]
    LMP_latent = compute.get_LMP_latent(path_origin, folder, dual_var_exact, dic_dual, scaler, latent_dim=n_latent)
    
    ccircle = []
    eucl_dist = []
    for i,j in enumerate(LMP .T):
        corr1 = np.corrcoef(j,LMP_latent[:,0])[0,1]
        corr2 = np.corrcoef(j,LMP_latent[:,1])[0,1]
        ccircle.append((corr1, corr2))
        eucl_dist.append(np.sqrt(corr1**2 + corr2**2))
    
    with plt.style.context(('seaborn-whitegrid')):
        gs = subfig.add_gridspec(nrows=1, ncols=2, width_ratios=[6,0.3], wspace=0.01)
        axs = subfig.add_subplot(gs[0])
        for i,j in enumerate(eucl_dist):
            # arrow_col = plt.cm.viridis((eucl_dist[i] - np.array(eucl_dist).min())/\
            #                         (np.array(eucl_dist).max() - np.array(eucl_dist).min()) )
            arrow_col = plt.cm.viridis((cmap_color[i]-min(cmap_color))/(max(cmap_color)-min(cmap_color)))
            axs.arrow(0,0, # Arrows start at the origin
                     ccircle[i][0],  #0 for PC1
                     ccircle[i][1],  #1 for PC2
                     lw = 2, # line width
                     length_includes_head=True, 
                     color = arrow_col,
                     fc = arrow_col,
                     head_width=0.05,
                     head_length=0.05)
        # Draw the unit circle, for clarity
        circle = Circle((0, 0), 1, facecolor='none', edgecolor='k', linewidth=1, alpha=0.5)
        axs.add_patch(circle)
        axs.set_xlabel("Variable latente 0")
        axs.set_ylabel("Variable latente 1")
    # plt.tight_layout()
    ax = subfig.add_subplot(gs[1])
    cb = mpl.colorbar.ColorbarBase(ax, orientation='vertical', 
                                   cmap='viridis',
                                   norm=mpl.colors.Normalize(min(cmap_color), max(cmap_color)),  # vmax and vmin
                                   label=cmap_label)
    
def plot_PCA_circle(folder, path_origin, variables_exact, dual_var_exact, net, dic_dual, scaler):
    n_fig = 3
    fig = plt.figure(constrained_layout=True, figsize=(3.2*n_fig, 2.25), dpi=300)
    subfig = fig.subfigures(1, n_fig, wspace=0.03, width_ratios=[1.1,1.12,1.03]) # See : https://stackoverflow.com/questions/68552473/sharing-of-xlabel-across-gridspec-subplots-axes-partial-row
    
    cmap_label = ["Distance au PCC (normé)", "Tension moyenne (en p.u.)", "Puissance moyenne (en kW)"]
    cmap_color = [compute._get_data_x(variables_exact, net, label) for label in ["distance", "mean_voltage", "mean_power"]]
    cmap_color[2] *= 1e3 # From MW to kW
    cmap_color[2] = cmap_color[2].clip(-6,6)
    for k, sub in enumerate(subfig):
        _PCA_circle(folder, path_origin, variables_exact, dual_var_exact, net, dic_dual, scaler, sub, cmap_label[k], cmap_color[k])
    
    # Save
    save(folder, path_origin, "PCA_cercle")
    
def plot_TKT(folder, path_origin, pca):
    T = pca.components_[:,1:]
    n = T.shape[1]
    dir_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/plot/TKT")
    os.makedirs(dir_path, exist_ok=True)
    for k in range(n+1):
        fig, ax = plt.subplots()
        K = np.diag(np.concatenate((np.ones(k), np.zeros(n-k))))
        im = ax.imshow((T.T)@K@T, cmap="viridis", vmin=-0.2, vmax=1)
        plt.axis("off")
        cbar = ax.figure.colorbar(im)
        save(folder, path_origin, "TKT/"+str(k))
    
    from pdf2image import convert_from_path
    
    def make_gif(frame_folder):
        frames = [convert_from_path(join(dir_path, str(k)+".pdf"))[0] for k in range(n)]
        frame_one = frames[0]
        frame_one.save(dir_path+"/../TKT.gif", format="GIF", append_images=frames,
                   save_all=True, duration=50)#, loop=0)
    
    make_gif(dir_path)
    
    
    
    
    
    
    
