# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes
"""
# Custom import
import src._import as imp
import dimension_reduction.import_func as import_func
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence

def test_import_net(folder, path_origin):
    """Folder : which network to test.
    path_origin : Where the network and data are located."""
    net = import_func.import_net(folder=folder, path_origin=path_origin)
    return net
    
def test_import_days(folder, path_origin):
    """Folder : which network to test.
    path_origin : Where the network and data are located."""
    days = import_func.get_all_days(folder=folder, path_origin=path_origin)
    return days
    
def test_separate_test_train_days(rs, all_days):
    """rs : random seed for replicability
    days : array of days to be splited"""
    train_days, test_days = import_func.split_train_test(rs=rs, all_days=all_days)
    return train_days, test_days
    
def main(folder, path_origin):
    """Folder : which network to test.
    path_origin : Where the network and data are located.
    rs : random seed for replicability"""
    # Test import func
    net = test_import_net(folder, path_origin)
    days = test_import_days(folder, path_origin)
    train_days, test_days = test_separate_test_train_days(rs = RandomState(MT19937(SeedSequence(folder))), all_days=days)
    
    # Test 
    return net, days, train_days, test_days

    
if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC",0)
    net, days, train_days, test_days = main(folder = folder,
                                            path_origin = path_origin)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    