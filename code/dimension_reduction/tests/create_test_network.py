# -*- coding: utf-8 -*-
"""
Created on Fri Aug 26 11:51:46 2022

@author: Guenole

Creates a very simple network that is going to be used for testing purposes.
Power and cost are midified in the file named "modif_test_network".
"""
# Standard libraries
import pandapower as pp
import numpy as np
# Custom import
import code_these.test_case_generation.create.create_test_case as create_test_case
import code_these.test_case_generation.create.assign_agent_to_bus as assign_agent_to_bus
import code_these.src._modify_net as _modif_net

###################################################################################
############################# Create network topology #############################
###################################################################################

def create_network(structure="meshed"):
    """Create the structure of a simple network.
    Choose between meshed and radial."""
    if structure == "meshed":
        net = create_meshed_network()
    if structure == "radial":
        net = create_radial_network()
    return net

def _init_net():
    net = pp.create_empty_network()
    
    min_vm_pu = 0.9
    max_vm_pu = 1.1
    
    #create buses
    pp.create_bus(net, name="bus 1", vn_kv=20, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)
    for k in range(1,6):
        pp.create_bus(net, name="bus "+str(k+1), vn_kv=0.4, min_vm_pu=min_vm_pu, max_vm_pu=max_vm_pu)
    
    # Create transformer
    pp.create_transformer(net, 
                          hv_bus=0, 
                          lv_bus=1, 
                          std_type="0.4 MVA 20/0.4 kV",
                          name="trafo",
                          in_service=True,
                          index=None,
                          max_loading_percent=100,
                          parallel=1,
                          df=1.0,
                          )
    net.trafo["shift_degree"]=0 #OPF fails if it has to consider angle.
    return net

def create_meshed_network():
    """Create a simple meshed network structure."""
    net = create_radial_network()
    pp.create_line(net, 3, 4, length_km=0.035, std_type='94-AL1/15-ST1A 0.4', max_loading_percent=100)
    
    return net

def create_radial_network():
    """Create a simple radial network structure."""
    net = _init_net()
    #create 0.4 kV lines
    pp.create_line(net, 1, 2, length_km=0.035, std_type='94-AL1/15-ST1A 0.4', max_loading_percent=100)
    pp.create_line(net, 2, 3, length_km=0.035, std_type='94-AL1/15-ST1A 0.4', max_loading_percent=100)
    pp.create_line(net, 2, 4, length_km=0.035, std_type='94-AL1/15-ST1A 0.4', max_loading_percent=1)
    pp.create_line(net, 4, 5, length_km=0.035, std_type='94-AL1/15-ST1A 0.4', max_loading_percent=100)
    return net

###################################################################################
########################## Create power exchange profile ##########################
###################################################################################

def create_power_exchange1(net):
    """"""
    # Price of each load
    cp1 = -1*np.array([99,101,80])
    cp2 = 50*cp1
    # Price of the grid
    c_grid_sell = 100
    c_grid_buy = 80
    
    ext1 = pp.create_ext_grid(net, 0, vm_pu=1., slack=True, index = 0, min_p_mw=0)
    ext2 = pp.create_ext_grid(net, 0, vm_pu=1., slack=False, index = 1, max_p_mw=0)
    pp.create_poly_cost(net, ext1, 'ext_grid', cp0_eur= 0 , cp1_eur_per_mw=c_grid_sell, cp2_eur_per_mw2=c_grid_sell,)
    pp.create_poly_cost(net, ext2, 'ext_grid', cp0_eur= 0 , cp1_eur_per_mw=c_grid_buy, cp2_eur_per_mw2=c_grid_buy,)
    
    #create loads
    lo0 = pp.create_load(net, 4, p_mw=0, q_mvar=0, min_p_mw=0, max_p_mw=10,
                         min_q_mvar=0, max_q_mvar= 0, index = 2, controllable=True)
    pp.create_poly_cost(net, lo0, 'load', cp0_eur= 0 , cp1_eur_per_mw=cp1[0], cp2_eur_per_mw2=cp2[0])
    
    
    lo01 = pp.create_load(net, 4, p_mw=0, q_mvar=0, min_p_mw=0, max_p_mw=10,
                         min_q_mvar=0, max_q_mvar= 0, index = 3, controllable=True)
    pp.create_poly_cost(net, lo01, 'load', cp0_eur= 0 , cp1_eur_per_mw=cp1[0], cp2_eur_per_mw2=cp2[0])
    lo02 = pp.create_load(net, 4, p_mw=0, q_mvar=0, min_p_mw=0, max_p_mw=10,
                         min_q_mvar=0, max_q_mvar= 0, index = 4, controllable=True)
    pp.create_poly_cost(net, lo02, 'load', cp0_eur= 0 , cp1_eur_per_mw=10*cp1[0], cp2_eur_per_mw2=10*cp2[0])
    
    
    lo4 = pp.create_load(net, 5, p_mw=0, q_mvar=0, min_p_mw=0, max_p_mw=100,
                         min_q_mvar=0, max_q_mvar= 0, index = 6, controllable=True)
    pp.create_poly_cost(net, lo4, 'load', cp0_eur= 0 , cp1_eur_per_mw=cp1[1], cp2_eur_per_mw2=0.1*cp2[1])
    lo5 = pp.create_load(net, 3, p_mw=0, q_mvar=0, min_p_mw=0, max_p_mw=100,
                         min_q_mvar=0, max_q_mvar= 0, index = 7, controllable=True)
    pp.create_poly_cost(net, lo5, 'load', cp0_eur= 0 , cp1_eur_per_mw=cp1[2], cp2_eur_per_mw2=0.1*cp2[2])
    return net
    
def create_power_exchange2(net):
    """"""
    # Price of each gen
    cp1 = np.array([65, 20])
    cp2 = cp1
    # create generators
    g2 = pp.create_gen(net, 3, p_mw=40, q_mvar=0, min_p_mw=0, max_p_mw=100,
                       min_q_mvar=-100, max_q_mvar= 100, index = 8, vm_pu=1.0)
    pp.create_poly_cost(net, g2, 'gen', cp1_eur_per_mw=cp1[0], cp2_eur_per_mw2=cp2[0])
    g3 = pp.create_gen(net, 4, p_mw=40, q_mvar=0, min_p_mw=0, max_p_mw=200,
                       min_q_mvar=-100, max_q_mvar= 100, index = 9, vm_pu=1.0)
    pp.create_poly_cost(net, g3, 'gen', cp1_eur_per_mw=cp1[1], cp2_eur_per_mw2=cp2[1])
    return net

###################################################################################
###################### Main function : execution of the code ######################
###################################################################################

def main(net_type="simple"):
    if net_type == "simple":
        net = create_network("radial")
        net = create_power_exchange1(net)
        net = create_power_exchange2(net)
    elif net_type == "LMP" : 
        net = create_test_case.create_LMP_network()
        net, agent_table, agent_prices = assign_agent_to_bus.custom_LMP(None, None, net)
        net = _modif_net.init_net(net, all_feasible=True)
    return net
    
if __name__=="__main__":
    net = main()
    