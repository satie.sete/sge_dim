# -*- coding: utf-8 -*-
"""
Created on Fri Aug 26 11:51:46 2022

@author: Guenole
"""
# Standard libraries
import pandapower as pp
import numpy as np
import time
from pandapower.plotting.plotly import pf_res_plotly, simple_plotly
# Custom import
import code_these.src._test_function as test_func
import code_these.src._AC_tools as AC_tools
import code_these.src._SOCWR_tools as SOCWR_tools
from code_these.dimension_reduction.plots.plot_template import _figure
from code_these.dimension_reduction.plots.plot_handle import _fig_dual, _fig_dual_multiple

import code_these.test_case_generation.plot.plot_test_case as plot
import code_these.test_case_generation.plot.test_plot_from_scratch as plot_from_scratch
import code_these.dimension_reduction.tests.modif_test_network as test_network

###################################################################################
############################# Create network topology #############################
###################################################################################

def scale(net, scale=1e-3):
    """scale de power asked by agents on the network"""
    if len(net.gen) > 0:
        net.gen[["p_mw","q_mvar", "min_p_mw",
                 "max_p_mw", "min_q_mvar", "max_q_mvar"]] *= scale
    if len(net.load) > 0:
        net.load[["p_mw","q_mvar", "min_p_mw",
                  "max_p_mw", "min_q_mvar", "max_q_mvar"]] *= scale
    return net

def test_get_power_from_LMP(net, id_dual, scl):
    import code_these.src._modify_net as modify_net
    LMP = net.dual["dual_value"].values[id_dual["active power balance"]["id_dual"]]
    res_load, res_gen = modify_net.get_pmw_from_LMP(LMP, net, id_dual)
    
    time.sleep(1)
    print("\n\n res load :\n",res_load/scl, "\n")
    print(net.res_load/scl)
    print("\n\n Res gen : \n")
    print(res_gen/scl, "\n")
    print(net.res_gen/scl)
    print("\n\n res ext_grid :\n", net.res_ext_grid/scl)

###################################################################################
###################### Main function : execution of the code ######################
###################################################################################

def main(model="SOC", scl=1e-3):
    net = test_network.net_a0()
    plot.plot_test_case(net)
    simple_plotly(net, auto_open=False)
    net = scale(net, scl)
    if model == "SOC":
        pp.runpm(net, pm_model="SOCWRPowerModel") # ACPPowerModel or SOCWRPowerModel
        id_dual = SOCWR_tools.get_all_nodal(net.dual)
    if model == "AC":
        pp.runpm(net, pm_model="ACPPowerModel")
        id_dual = AC_tools.get_all_nodal(net.dual) # BUG need to change this function (previously array, now dict)
    return net, id_dual
    
if __name__=="__main__":
    from julia.api import Julia
    jl = Julia(compiled_modules=False)
    model="SOC"
    scl = 1
    net, id_dual = main(model, scl)
    test_get_power_from_LMP(net, id_dual, scl)
    
    ### PLOT ###
    pf_res_plotly(net, auto_open=False) # Show network in plotly (open browser)
    _figure(_fig_dual,
            {"LMP" : net.dual["dual_value"][id_dual["active power balance"]["id_dual"]]},
            {"LMP" : id_dual["active power balance"]["id_bus"]})
    plot_from_scratch.plot_constraints(net, size=1)
    # # Show dual values separated by class
    # data = SOCWR_tools.organize_dual(net.dual["dual_value"], id_dual)
    # _figure(len(id_dual)*[_fig_dual_multiple], data, 6, 2, 6.4, 10)
    
    res, _ = test_func._test_SOC_relaxation(net, id_dual)
    res, info_LMP, max_dist, _ = test_func._test_LMP_congestion(net, id_dual)
    print(res)
    