#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 30 16:45:30 2023

@author: gcherot
"""

# Standard libraries
import os
import sys
from os.path import join
os.environ['KMP_DUPLICATE_LIB_OK']='True' # Prevent crash when using plt
import numpy as np
import keras
import tensorflow as tf
from sklearn import preprocessing

import optuna
import logging

# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize
from code_these.dimension_reduction import autoencoder
from code_these.dimension_reduction.dimension_reduction import import_data, _split1, _split2



def objective(original_dim, latent_dim, trial, data_train, data_test):
    # activation = trial.suggest_categorical("activation", ["relu", "tanh"])
    # n_hidden = trial.suggest_int("num_layers", 0, 4)
    # dropout = trial.suggest_categorical("dropout", [True, False])
    # optimizer = trial.suggest_categorical("optimizer", ["adam", "SGD", "RMSprop"])
    # decay_rate = trial.suggest_float("decay_rate", 0.5, 1, log=False)
    lr = trial.suggest_float("learning_rate", 1e-6, 1e-2, log=True)
    beta_1 = trial.suggest_float("beta_1", 0.7, 1-1e-5, log=False)
    beta_2 = trial.suggest_float("beta_2", 0.7, 1-1e-5, log=False)
    
    activation = "relu"
    n_hidden = 0
    dropout = False
    optimizer = "adam"
    decay_rate = 1
    
    model_to_train = model(original_dim, latent_dim, n_hidden, activation, dropout, lr, optimizer, decay_rate,
                           beta_1, beta_2)
    for step in range(20):
        history = model_to_train.fit(data_train, data_train,
                        epochs=dic["n_epochs"],
                        batch_size=dic["batch_size"],
                        shuffle=True, validation_data=(data_test,data_test),
                        verbose=0)
        trial.report(history.history["val_loss"][-1], step)
        # Handle pruning based on the intermediate value.
        if trial.should_prune():
            raise optuna.TrialPruned()
    return history.history["val_loss"][-1]

def model(original_dim, latent_dim, n_hidden, activation, dropout, lr, optimizer, decay_rate, beta_1, beta_2):
    auto_encoder = autoencoder.Autoencoder_model(original_dim, latent_dim, n_hidden=n_hidden, activation=activation, dropout=dropout)
    if optimizer == "adam":
        opt = keras.optimizers.Adam(learning_rate=lr,
                                    beta_1=beta_1,
                                    beta_2=beta_2)
    else :
        lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
                                    initial_learning_rate=lr,
                                    decay_steps=1000,
                                    decay_rate=decay_rate)
        if optimizer == "SGD" :
            opt = keras.optimizers.SGD(learning_rate=lr_schedule)
        elif optimizer == "RMSprop" :
            opt = keras.optimizers.RMSprop(learning_rate=lr_schedule)
    
    auto_encoder.compile(optimizer=opt, loss='mean_squared_error')
    return auto_encoder

def get_data(folder, path_origin):
    dual_var, dic_dual, variables, var_names, time, days = import_data(folder, path_origin, "train")
    LMP_data = dual_var[:,dic_dual["active power balance"]["id_dual"]]
    scaler = preprocessing.StandardScaler().fit(LMP_data)
    data = scaler.transform(LMP_data)
    # Split data
    index_train, index_test = _split1(data, time)
    data_train, data_test  = _split2(data, index_train, index_test)
    return data_train, data_test

def main(path_origin , folder):
    save_path = join(path_origin, "processed/use", str(folder), "data/dimension_reduction/hyperparameter_tunning")
    data_train, data_test = get_data(folder, path_origin)
    original_dim = np.shape(data_train)[1]
    latent_dim = 100
    obj = lambda trial : objective(original_dim, latent_dim, trial, data_train, data_test)
    
    optuna.logging.get_logger("optuna").addHandler(logging.StreamHandler(sys.stdout))
    study_name = "study_3"  # Unique identifier of the study.
    storage_name = "sqlite:///{}.db".format(study_name)
    optuna.logging.get_logger("optuna").addHandler(logging.StreamHandler(sys.stdout))
    study = optuna.create_study(study_name=study_name, storage=storage_name,
                                sampler=optuna.samplers.TPESampler(),
                                pruner=optuna.pruners.MedianPruner())
    study.optimize(obj, n_trials=500)
    return study

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman",2)
    norm=True
    dic = initialize._init_dic()
    study = main(path_origin, folder)