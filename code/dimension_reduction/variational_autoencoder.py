# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:54:50 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes
"""
import keras
from keras import layers
from keras import backend as K


class Variational_autoencoder_contener():
    def __init__(self, original_dim, intermediate_dim, latent_dim):
        self.init_const(original_dim, intermediate_dim, latent_dim)

        inputs = keras.Input(shape=(original_dim,))
        h = layers.Dense(intermediate_dim, activation='relu')(inputs)
        z_mean = layers.Dense(latent_dim)(h)
        z_log_sigma = layers.Dense(latent_dim)(h)
        
        z = layers.Lambda(self.sampling)([z_mean, z_log_sigma])
        
        # Create encoder
        self.encoder = keras.Model(inputs, [z_mean, z_log_sigma, z], name='encoder')
        
        # Create decoder
        latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
        x = layers.Dense(intermediate_dim, activation='relu')(latent_inputs)
        outputs = layers.Dense(original_dim, activation='sigmoid')(x)
        self.decoder = keras.Model(latent_inputs, outputs, name='decoder')
        
        # instantiate VAE model
        outputs = self.decoder(self.encoder(inputs)[2])
        self.vae = keras.Model(inputs, outputs, name='vae_mlp')
        
        reconstruction_loss = keras.losses.binary_crossentropy(inputs, outputs)
        reconstruction_loss *= original_dim
        kl_loss = 1 + z_log_sigma - K.square(z_mean) - K.exp(z_log_sigma)
        kl_loss = K.sum(kl_loss, axis=-1)
        kl_loss *= -0.5
        vae_loss = K.mean(reconstruction_loss + kl_loss)
        self.vae.add_loss(vae_loss)
        self.vae.compile(optimizer='adam')

        
    def sampling(self, args):
        z_mean, z_log_sigma = args
        epsilon = K.random_normal(shape=(K.shape(z_mean)[0], self.latent_dim),
                                  mean=0., stddev=0.1)
        return z_mean + K.exp(z_log_sigma) * epsilon
    
    def init_const(self, original_dim, intermediate_dim, latent_dim):
        self.latent_dim = latent_dim
        self.intermediate_dim = intermediate_dim
        self.original_dim = original_dim
        self.reconstructed_dim = original_dim
        
if __name__ == "__main__":
    # Test if the vae work as expected (Not realy)
    original_dim = 28 * 28
    intermediate_dim = 64
    latent_dim = 2
    
    vae_cont = Variational_autoencoder_contener(original_dim, intermediate_dim, latent_dim)
    
    from keras.datasets import mnist
    import matplotlib.pyplot as plt
    import numpy as np

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
    
    callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)
    
    history = vae_cont.vae.fit(x_train, x_train,
                      shuffle = True,
                      epochs=10,
                      batch_size=64,
                      validation_data=(x_test, x_test),
                      callbacks=[callback])
    
    x_test_encoded = vae_cont.encoder.predict(x_test, batch_size=256)
    x_test_encoded = np.asarray(x_test_encoded)
    plt.figure(figsize=(6, 6))
    plt.scatter(x_test_encoded[0,:,0], x_test_encoded[0,:,1], c=y_test)
    plt.colorbar()
    plt.show()


    # Display a 2D manifold of the digits
    n = 15  # figure with 15x15 digits
    digit_size = 28
    figure = np.zeros((digit_size * n, digit_size * n))
    # We will sample n points within [-15, 15] standard deviations
    grid_x = np.linspace(-15, 15, n)
    grid_y = np.linspace(-15, 15, n)
    
    for i, yi in enumerate(grid_x):
        for j, xi in enumerate(grid_y):
            z_sample = np.array([[xi, yi]])
            x_decoded = vae_cont.decoder.predict(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[i * digit_size: (i + 1) * digit_size,
                   j * digit_size: (j + 1) * digit_size] = digit
    
    plt.figure(figsize=(10, 10))
    plt.imshow(figure)
    plt.show()
    
    
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()
