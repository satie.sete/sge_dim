# -*- coding: utf-8 -*-
"""
Created on Thu Sep  8 14:25:52 2022

@author: Guenole
"""
import numpy as np

def get_all_nodal(data):
    """Ranked in the order of this video : https://youtu.be/ONJYq8uPOU4 (TODO)"""
    bus_nodal_p = get_nodal_price_p(data)
    bus_nodal_q = get_nodal_price_q(data)
    bus_line_lim_p = get_line_lim_p(data)
    bus_line_lim_q = get_line_lim_q(data)
    bus_power_line_lim = get_power_line_lim(data)
    bus_injection_p = get_injection_p(data)
    bus_injection_q = get_injection_q(data)
    bus_bus_voltage_angle = get_bus_voltage_angle(data)
    bus_line_voltage_angle = get_line_voltage_angle(data)
    bus_voltage_magnitude = get_voltage_magnitude(data)
    
    ret = [bus_nodal_p, bus_nodal_q, 
           bus_line_lim_p, bus_line_lim_q,
           bus_power_line_lim, bus_injection_p, bus_injection_q,
           bus_bus_voltage_angle, bus_line_voltage_angle,
           bus_voltage_magnitude]
    title = ["active power balance", "reactive power balance",
             "line active limits", "line reactive limits",
             "thermal limits",
             "agent active power injection", "agent reactive power injection",
             "Bus Voltage angle", "Line voltage angle difference",
             "Bus voltage magnitude"]
    
    return ret


def get_nodal_price_p(data):
    """Constrain on the active power injected at each bus"""
    bus = _get_nodal(data, "p")
    return bus

def get_nodal_price_q(data):
    """Constrain on the reactive power injected at each bus"""
    bus = _get_nodal(data, "q")
    return bus

def get_line_lim_p(data):
    """Constrain on the active power going trough each line"""
    bus = _get_line_lim(data, "p")
    return bus

def get_line_lim_q(data):
    """Constrain on the reactive power going trough each line"""
    bus = _get_line_lim(data, "q")
    return bus

def get_power_line_lim(data):
    """contrains on |W|² <= WiiWjj (Second order cone relaxation)
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (strats at 1))
        """
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("²") != -1 :
            id_line.append(k)
            id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("_p[(")+7]))
    return np.asarray([id_line, id_bus])

def get_injection_p(data):
    """Constrain on the active power injected by each agent"""
    bus = _get_injection(data, "p")
    return bus

def get_injection_q(data):
    """Constrain on the active power injected by each agent"""
    bus = _get_injection(data, "q")
    return bus

def get_bus_voltage_angle(data):
    """Constrain on the voltage angle at each bus"""
    bus = _get_voltage_angle(data, "bus")
    return bus

def get_line_voltage_angle(data):
    """Constrain on the voltage angle along each line"""
    bus = _get_voltage_angle(data, "line")
    return bus

def get_voltage_magnitude(data):
    """Contrain on the voltage magnitude on each bus"""
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("vm") != -1 :
            id_line.append(k)
            id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("vm")+3]))
    return np.asarray([id_line, id_bus])

###############################################################
#################### To use internaly only ####################
###############################################################

def _get_voltage_angle(data, bus_or_line):
    """Constrain on the voltage angle along each line"""
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("va") != -1 :
            if data.Name.loc[k].find("<=") == -1 and data.Name.loc[k].find(">=") == -1:
                if bus_or_line == "bus":
                    id_line.append(k)
                    id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("va")+3]))
            else :
                if bus_or_line == "line":
                    id_line.append(k)
                    id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("va")+3]))

    return np.asarray([id_line, id_bus])

def _get_injection(data, p_or_q):
    """Constrain on the active/reactive power injected by each agent"""
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("_"+p_or_q+"[(") == -1 :
            if data.Name.loc[k].find(p_or_q+"g") != -1 :
                id_line.append(k)
                id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find(p_or_q+"g")+3]))
    return np.asarray([id_line, id_bus])

def _get_nodal(data, p_or_q):
    """Constrain on the active/reactive power injected at each bus.
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (strats at 1))
        """
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("==") != -1 :
            if data.Name.loc[k].find("_"+p_or_q+"[(") != -1 :
                id_line.append(k)
                id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("_"+p_or_q+"[(")+7]))
    return np.asarray([id_line, id_bus])

def _get_line_lim(data,p_or_q):
    """Constrain on the active/reactive power going trough each line
    
    Take dual DataFrame from PowerModels.
    Return 2D array :
        - Id of the raw containing a bus egality constrain
        - Id of the bus (PowerModels convention (strats at 1))
        """
    id_line = []
    id_bus = []
    for k in range(len(data)):
        if data.Name.loc[k].find("²") == -1 :
            if data.Name.loc[k].find("==") == -1 :
                if data.Name.loc[k].find("_"+p_or_q+"[(") != -1 :
                    id_line.append(k)
                    id_bus.append(int(data.Name.loc[k][data.Name.loc[k].find("_"+p_or_q+"[(")+7]))
    return np.asarray([id_line, id_bus])
