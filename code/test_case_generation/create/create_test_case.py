# -*- coding: utf-8 -*-
"""
Created on Tue Nov  16 11:30:34 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

This file is used to create the network used in the test case.
In this version :
    - import_cigre_lv_modif : CIGRE Low voltage network modified
    - Custom network for thesis and LMP test
    - Random network generation using networkx
    
Type of bus :
    - b : HTA
    - n : slack
    - m : Low voltage
"""
max_vm_pu_bt = 1.1
min_vm_pu_bt = 0.9

# Standard librairies
import pandas as pd
from os.path import join
import os
import numpy as np
import pandapower.networks as pn
import pandapower as pp
import networkx as nx
from pandapower import plotting
from pandapower.plotting.generic_geodata import create_generic_coordinates
# Custom import
import code_these.src._import as imp
import code_these.src._initialize as initialize
# Reproducibility
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence

def _copy_network_from_empty(net_origin, name="Eu"):
    net = pp.create_empty_network(name=name, f_hz=50.0)
    # Create bus
    for bus_index in net_origin.bus.index :
        try : 
            x, y = net_origin.bus_geodata.loc[bus_index, ['x','y']]
            pp.create_bus(net,
                          name="BUS"+str(bus_index),
                          index = bus_index,
                          vn_kv = net_origin.bus.loc[bus_index, "vn_kv"],
                          min_vm_pu = 0.9,
                          max_vm_pu = 1.1,
                          geodata = (x,y))
        except :
            pp.create_bus(net,
                          name="BUS"+str(bus_index),
                          index = bus_index,
                          vn_kv = net_origin.bus.loc[bus_index, "vn_kv"],
                          min_vm_pu = 0.9,
                          max_vm_pu = 1.1)
            
    # create lines
    if name == "Eu" or name == "random_BT" :
        lines = net_origin.line[["from_bus","to_bus", "length_km"]].to_numpy()
        for k, l in enumerate(lines) :
            pp.create_line_from_parameters(net,
                                           from_bus=int(l[0]),
                                           to_bus=int(l[1]),
                                           length_km=l[2],
                                           name="LINE"+str(k),
                                           index=k,
                                           df=1.0,
                                           parallel=1,
                                           in_service=True,
                                           max_i_ka = 0.421,
                                           max_loading_percent=100,
                                           r_ohm_per_km = 1,
                                           x_ohm_per_km = 0.09,
                                           c_nf_per_km = 0, 
                                           r0_ohm_per_km = 1.2, 
                                           x0_ohm_per_km = 0.09,
                                           c0_nf_per_km = 0)
    elif name == "Case33" or name == "Cigre_mv" :
        for k, line_index in enumerate(net_origin.line.index):
            pp.create_line_from_parameters(net,
                                           from_bus=int(net_origin.line.loc[line_index, "from_bus"]),
                                           to_bus=int(net_origin.line.loc[line_index, "to_bus"]),
                                           length_km=net_origin.line.loc[line_index, "length_km"],
                                           name="LINE" + str(line_index),
                                           index=line_index,
                                           df=1.0,
                                           parallel=net_origin.line.loc[line_index, "parallel"],
                                           in_service=True,
                                           max_i_ka=net_origin.line.loc[line_index, "max_i_ka"], # 9999 in the original file, not possible
                                           max_loading_percent=100,
                                           r_ohm_per_km=net_origin.line.loc[line_index, "r_ohm_per_km"],
                                           x_ohm_per_km=net_origin.line.loc[line_index, "x_ohm_per_km"],
                                           c_nf_per_km=0,
                                           r0_ohm_per_km=0,
                                           x0_ohm_per_km=0,
                                           c0_nf_per_km=0)
        for k, transfo_index in enumerate(net_origin.trafo.index):
            pp.create_transformer_from_parameters(net,
                                                  std_type=None,
                                                  hv_bus=int(net_origin.trafo.loc[transfo_index, "hv_bus"]),
                                                  lv_bus=int(net_origin.trafo.loc[transfo_index, "lv_bus"]),
                                                  sn_mva=net_origin.trafo.loc[transfo_index, "sn_mva"],
                                                  vn_hv_kv=net_origin.trafo.loc[transfo_index, "vn_hv_kv"],
                                                  vn_lv_kv=net_origin.trafo.loc[transfo_index, "vn_lv_kv"],
                                                  vkr_percent=net_origin.trafo.loc[transfo_index, "vkr_percent"],
                                                  vk_percent=net_origin.trafo.loc[transfo_index, "vk_percent"],
                                                  pfe_kw=net_origin.trafo.loc[transfo_index, "pfe_kw"],
                                                  i0_percent=net_origin.trafo.loc[transfo_index, "i0_percent"],
                                                  shift_degree=0,
                                                  tap_phase_shifter=False,
                                                  in_service=True,
                                                  name="TRANSFO"+str(transfo_index),
                                                  vector_group=None,
                                                  index=None,
                                                  max_loading_percent=100,
                                                  parallel=1,
                                                  df=1.0)
        pp.drop_switches_at_buses(net, buses=list(set(list(net.switch.loc[:,"bus"]))))
    # Create ext grid agent. Mandatory to simulate network but not realy used during simulation
    pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    return net

def create_pn(rs, line_lenght_variance=0, probability="normal", scale_load=1, scale_gen=1, case="Case33"):
    if case == "Case33":
        net = pn.case33bw()
        pp.drop_lines(net, lines=[36, 32, 33, 34, 35]) #--------- Coller au case 33 IEEE et non le 33bw
    elif case == "Cigre_mv" :
        net = pn.create_cigre_network_mv()
        pp.drop_lines(net, lines=[13, 14])
        
    for l in net.line.index :
        rd = _sample(probability)
        net.line.loc[l, "length_km"] *= 1+ np.abs(rd)*line_lenght_variance
    net = _copy_network_from_empty(net, name=case)
    net.bus.loc[:,"type"] = "m"
    net.bus.loc[net.bus.index[0], "type"] = "b"  # --------- interdire des agents sur le bus 0
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    pp.runpp(net)
    return net

def create_simplest_network(rs, line_lenght_variance=0, probability="normal", scale_load=1, scale_gen=1):
    net = pp.create_empty_network(name='Simplest', f_hz=50.0)
    pp.create_bus(net, name="BUS0", index = 0, vn_kv = 0.4, min_vm_pu = 0.9, max_vm_pu = 1.1, type="b",  geodata = (0,0))
    pp.create_bus(net, name="BUS1", index = 1, vn_kv = 0.4, min_vm_pu = 0.9, max_vm_pu = 1.1, type="m",  geodata = (1,0.5))
    pp.create_bus(net, name="BUS2", index = 2, vn_kv = 0.4, min_vm_pu = 0.9, max_vm_pu = 1.1, type="m",  geodata = (1,-0.5))
    pp.create_line(net, name="LINE1", from_bus=0, to_bus=1, length_km=1, std_type="48-AL1/8-ST1A 0.4", # Type of line in LV  : The list of available line can be found using the command : pp.available_std_types(net, element='line')
                    df=1.0, parallel=1, in_service=True, max_loading_percent=100)
    pp.create_line(net, name="LINE2", from_bus=0, to_bus=2, length_km=1, std_type="48-AL1/8-ST1A 0.4", # Type of line in LV  : The list of available line can be found using the command : pp.available_std_types(net, element='line')
                    df=1.0, parallel=1, in_service=True, max_loading_percent=100)
    rd = _sample(probability)
    net.line.loc[:, "length_km"] = net.line.loc[:,"length_km"]*(1+ np.abs(rd)*line_lenght_variance)
    pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    pp.runpp(net)
    return net

def create_SGE_RL_network(rs, line_lenght_variance=0, probability="normal", scale_load=1, scale_gen=1):
    net = pp.create_empty_network(name='SGE_RL', f_hz=50.0)
    pp.create_bus(net, name="BUS0", index = 0, vn_kv = 20, min_vm_pu = 0.95, max_vm_pu = 1.05, type="b",  geodata  = (0,0))
    pp.create_bus(net, name="BUS1", index = 1, vn_kv = 20, min_vm_pu = 0.95, max_vm_pu = 1.05, type="m",  geodata  = (1,0.5))
    pp.create_line(net, name="LINE1", from_bus=0, to_bus=1, length_km=1, std_type="48-AL1/8-ST1A 20.0", # Type of line in LV  : The list of available line can be found using the command : pp.available_std_types(net, element='line')
                    df=1.0, parallel=1, in_service=True, max_loading_percent=100)
    rd = _sample(probability)
    net.line.loc[:, "length_km"] = net.line.loc[:,"length_km"]*(1+ np.abs(rd)*line_lenght_variance)
    pp.create_ext_grid(net, name="sell_grid", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_grid", vm_pu=1.0, bus=0, max_p_mw=0)
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    pp.runpp(net)
    return net

def import_cigre_lv_modif():
    """Import the network from panda power.
    Modifications :
        - Delete the industrial subnetwork.
        - Change the line caracteristics.
        - Replace switches with HTA lines.
        - Delete Geodata.
    """
    net = pn.create_cigre_network_lv() # Import the network
    pp.drop_buses(net, [20,21,22]) # Delete the industrial substation
    # Change the type of lines
    connections = net.line[["from_bus","to_bus"]].to_numpy() # Get the connections (all lines are in the LV network)
    pp.drop_lines(net, lines=net.line.index) # Drop all lines to build new ones
    for k in connections : # build new lines
        pp.create_line(net,
                       from_bus=k[0],
                       to_bus=k[1],
                       length_km=0.03,
                       std_type="48-AL1/8-ST1A 0.4", # Type of line in LV  : The list of available line can be found using the command : pp.available_std_types(net, element='line')
                       name=None,
                       index=None,
                       geodata=None,
                       df=1.0,
                       parallel=1,
                       in_service=True,
                       max_loading_percent=100) 
    
    # Replace switches by HTA lines
    for k in net.switch.index.unique():
        net.bus.loc[net.switch.loc[k,"element"], "type"] = "b"
        pp.create_line(net,
                       from_bus=0,
                       to_bus=net.switch.loc[k,"element"],
                       length_km=0.1,
                       std_type="48-AL1/8-ST1A 20.0",
                       name=None,
                       index=None,
                       geodata=None,
                       df=1.0,
                       parallel=1,
                       in_service=True,
                       max_loading_percent=100)
    pp.drop_switches_at_buses(net, buses=[0]) # Delete switches (they are replaced by HTA)
    # Drop geodata
    net.bus_geodata.drop(net.bus_geodata.index, inplace=True)
    return net

def create_European_lv(rs, line_lenght_variance=0, probability="normal"):
    raise ValueError("Not meant to be used without testing.")
    net = pn.ieee_european_lv_asymmetric(scenario='on_peak_566')
    net.asymmetric_load.drop(net.asymmetric_load.index, inplace=True)
    net.load.drop(net.load.index, inplace=True)
    # Create ext grid agent. Mandatory to simulate network but not realy used during simulation
    net.ext_grid.drop(net.ext_grid.index, inplace=True)
    pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    net.trafo.shift_degree = 0
    rd = _sample(probability)
    net.line.loc[:, "length_km"] = net.line["length_km"]*(1+ np.random.weibull(1.2, len(net.line))*line_lenght_variance)
    pp.runpp(net)
    return net

def _sample(probability):
    # Sample line folling a given distribution
    if probability == "normal":
        rd = np.random.normal(0,1)
    elif probability == "weibull":
        rd = 0.826*np.random.weibull(1.1) # Same mean as N(0,1)
    elif probability == "exp":
        rd = 0.798*np.random.exponential(1) # Same mean as N(0,1)
    return rd

def _useless_bus(net):
    """Delete all uselesse bus (bus only connecgted to 2 other buses), and create new lines to connect left buses"""
    ind = net.bus.index.values
    for id_bus in ind:
        line = net.line.loc[((net.line["to_bus"]==id_bus) | (net.line["from_bus"]==id_bus))]
        count = len(line)
        if count == 2 :
            bus_to_connect = line[["to_bus", "from_bus"]].values.flatten()[line[["to_bus", "from_bus"]].values.flatten() != id_bus]
            net.bus.drop(index = id_bus, inplace=True)
            net.line.drop(index = line.index, inplace=True)
            pp.create_line_from_parameters(net,
                                           name = "LINE"+str(line.index[0]),
                                           index = line.index[0],
                                           from_bus = bus_to_connect[0],
                                           to_bus = bus_to_connect[1],
                                           length_km = line.length_km.values.sum(),
                                           r_ohm_per_km = line.length_km.values.mean(),
                                           x_ohm_per_km = line.length_km.values.mean(),
                                           c_nf_per_km = line.length_km.values.mean(),
                                           r0_ohm_per_km = line.length_km.values.mean(),
                                           x0_ohm_per_km = line.length_km.values.mean(),
                                           c0_nf_per_km = line.length_km.values.mean(),
                                           max_i_ka = line.length_km.values.min())
    return net

def _reindex(net, name="Eu"):
    old_bus_index = {"old_bus_index" : net.bus.index}
    old_line_index = {"old_line_index" : net.line.index}
    bus_index = pd.DataFrame(old_bus_index, index=np.arange(0, len(net.bus.index), 1))
    line_index = pd.DataFrame(old_line_index, index=np.arange(0, len(net.line.index), 1))
    net.bus["index"] = np.arange(0, len(net.bus.index), 1)
    net.bus.set_index("index", inplace=True)
    if name == "Eu":
        net.bus_geodata = net.bus_geodata.loc[bus_index["old_bus_index"]]
        net.bus_geodata["index"] = np.arange(0, len(net.bus.index), 1)
        net.bus_geodata.set_index("index", inplace=True)
    net.line["index"] = np.arange(0, len(net.line.index), 1)
    net.line.set_index("index", inplace=True)
    for bus in bus_index.index :
        net.line.loc[net.line["from_bus"]==bus_index.loc[bus, "old_bus_index"], "from_bus"] = bus
        net.line.loc[net.line["to_bus"]==bus_index.loc[bus, "old_bus_index"], "to_bus"] = bus
    return net

def create_European_lv_custom(rs, line_lenght_variance=0,
                              probability="normal", scale_load=1, scale_gen=1):
    """Create the European Low Voltage Test Feeder: https://cmte.ieee.org/pes-testfeeders/resources/
    Delete the 3 phases to keep only one.
    line variance modifies the length of each line."""
    
    def _delete_bus_same_geodata(net):
        """Some buses have exactly the same geodata. Changeing this for better visualisation."""
        duplicated_geodata = net.bus_geodata.index[net.bus_geodata.duplicated(keep=False)]
        bus_connection = np.asarray([[len(net.line.loc[((net.line["to_bus"]==id_bus) | (net.line["from_bus"]==id_bus))]), id_bus] for id_bus in net.bus.index])
        bus_alone = bus_connection[np.where(bus_connection[:,0]==1)[0], 1]
        for id_bus in duplicated_geodata :
            if id_bus in bus_alone :
                net.bus.drop(index=id_bus, inplace=True)
                net.bus_geodata.drop(index=id_bus, inplace=True)
                line = net.line.loc[((net.line["to_bus"]==id_bus) | (net.line["from_bus"]==id_bus))]
                net.line.drop(index = line.index, inplace=True)
        return net
    
    def _delete_useless_attributes(net):
        net.line_geodata = net.line_geodata[0:0]
        # net.bus_geodata = net.bus_geodata[0:0] # Comment to keep original coords
        net.asymmetric_load = net.asymmetric_load[0:0]
        net.res_trafo = net.res_trafo[0:0]
        net.res_bus_3ph = net.res_bus_3ph[0:0]
        net.res_line_3ph = net.res_line_3ph[0:0]
        net.res_trafo_3ph = net.res_trafo_3ph[0:0]
        net.res_ext_grid_3ph = net.res_ext_grid_3ph[0:0]
        net.res_asymmetric_load_3ph = net.res_asymmetric_load_3ph[0:0]
        return net
    
    def _delete_trafo(net):
        net.trafo = net.trafo[0:0]
        net.bus.drop(index=0, inplace=True)
        net.ext_grid.loc[0, "bus"] = 1
        return net

    net = pn.ieee_european_lv_asymmetric(scenario='on_peak_566')
    net = _delete_bus_same_geodata(net)
    net = _useless_bus(net)
    net = _delete_useless_attributes(net)
    net = _delete_trafo(net)
    net = _reindex(net)
    net = _copy_network_from_empty(net)
    for l in net.line.index :
        rd = _sample(probability)
        net.line.loc[l, "length_km"] *= 1+ np.abs(rd)*line_lenght_variance
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    pp.runpp(net)
    return net

def create_LMP_network():
    """Create a synthetic and not realistic low voltage network that will exibit multiple LMP.
    It a is simple tree network.
    Line capacity should be set low in order to show congestions."""
    # Init net
    net = pp.create_empty_network(name='LMP_network', f_hz=50.0, sn_mva=1, add_stdtypes=True)
    # Create HTA bus
    pp.create_bus(net, vn_kv = 20,
                 name="ext", index=0, 
                 type='n', zone="HTA", in_service=True)
    pp.create_bus(net, vn_kv = 20, name="Bus_1", index=1,
                  type='b', zone="HTA", in_service=True)
    # Create line between HTA buses
    pp.create_line(net, from_bus=0, to_bus=1,
                        length_km= 0.3, std_type="48-AL1/8-ST1A 20.0",
                        name="HTA_0_1", max_loading_percent=100)
    # Create ext grid agent. Mandatory to simulate network but not realy used during simulation
    pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    # Create low voltage buses
    for k in range(15):
        pp.create_bus(net, vn_kv = 0.4, name="Bus_"+str(k+2), index=k+2,
                      type='m', zone="BT", in_service=True)
    # Create transformer between HTA and BT
    pp.create_transformer(net, hv_bus=1, lv_bus=2, 
                      std_type="0.4 MVA 20/0.4 kV",
                      name="trafo_", in_service=True, index=None,
                      max_loading_percent=100, parallel=1, df=1.0)
    net.trafo["shift_degree"]=0 # OPF fails if it has to consider angle.
    # Create connection between LV buses. Tree structure.
    bus_connection = [[2,3],
                      [3,5],
                      [3,6],
                      [5,9],
                      [5,10],
                      [3,6],
                      [6,11],
                      [6,12],
                      [2,4],
                      [4,7],
                      [7,13],
                      [7,14],
                      [4,8],
                      [8,15],
                      [8,16]                      ]
    for connection in bus_connection :
        fr = connection[0]
        to = connection[1]
        pp.create_line(net, from_bus=fr, to_bus=to,
                        length_km= 0.3, std_type="94-AL1/15-ST1A 0.4",
                        name=str(fr)+"_"+str(to), max_loading_percent=100)
    return net

def create_random_tree_HTA_network(rs, n_bus=20, n_subnetwork=1,
                               line_lenght=0.03, line_lenght_variance=0,
                               probability="normal", scale_load=1, scale_gen=1):
    """Create a random tree low voltage network.
    
    Parameter
    ----------
    - rs : random sequence generator
        Shoud be specify to randomly sample tree networks.
    - n_bus : int
        Number of bus of each subnetwork.
    - n_subnetwork : int
        Number lv networks
    - line_lenght : float
        Lenght of a line (in km)
    - line_lenght_variance : float
        Dispersion of lines (in km). Use a Weibull distribution (default is 0, not random)
    """
    # Init net
    net = pp.create_empty_network(name='synthetic_network', f_hz=50.0, sn_mva=1, add_stdtypes=True)
    # Init HTA connection
    pp.create_bus(net, vn_kv = 20,
                 name="ext", index=0, 
                 type='n', zone="HTA", in_service=True)
    # Create two ext grid, one sells, one buy. Allow to have piece wise linear cost. buy_state_rate should be lower than sell_spot otherwise not convex.
    pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    # Create the rest of the network
    for k in range(n_subnetwork): # A sub network is define as a lv network.
        add_random_tree_network(rs, net, n_bus=n_bus, lv_id=k, line_lenght=line_lenght, line_lenght_variance=line_lenght_variance, probability=probability)
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    return net

def connect_HTA(rs, net, n_bus):
    """Connect the network to the HTA network.
    For this we select the HTA bus wich is not connected to any other bus exept the transfo and the ext_grid.
    
    Parameter
    ----------
    - rs : random sequence generator
        Shoud be specify to randomly sample tree networks.
    - net : pandapower network
    - n_bus : int
        Number of bus of each subnetwork."""
    # Get the HTA bus to connect to
    buses = net.bus.loc[(net.bus["vn_kv"]==20) &
                        (net.bus["type"]=="b")].index # Get all HTA buses
    from_bus = []
    if len(net.line.loc[net.line["from_bus"]==0])<=1:
        from_bus.append(0)
    for k in buses:
        if (len(net.line.loc[net.line["from_bus"]==k])==0) and (k!=n_bus):
            from_bus.append(k)

    from_bus = rs.choice(from_bus)
    
    # Create the line
    pp.create_line(net,
                   from_bus=from_bus,
                   to_bus=n_bus,
                   length_km=0.035,
                   std_type="48-AL1/8-ST1A 20.0",
                   name="HTA_{0}_{1}".format(from_bus,n_bus),
                   index=None,
                   geodata=None,
                   df=1.0,
                   parallel=1,
                   in_service=True,
                   max_loading_percent=100)

def _create_BT(net, rs, n_bus, lv_id, connected_to, line_lenght=0.03, probability="normal", line_lenght_variance=0):
    # Generate the random tree network using networkx
    seed = int(rs.random()*1e20)
    tree = nx.random_tree(n=n_bus, seed=seed)
    n_bus_tot = len(net.bus)
    # Init bus
    for bus in tree._node :
        pp.create_bus(net,
                      vn_kv = 0.4,
                      name="Bus_{0}".format(bus+n_bus_tot),
                      index=bus+n_bus_tot, 
                      type='m', zone="LV_{0}".format(lv_id), in_service=True,
                      max_vm_pu=max_vm_pu_bt, min_vm_pu=min_vm_pu_bt)
    # Get line connection
    connections = []
    for bus in tree._node :
        for k in tree._adj[bus].keys():
            connections.append([bus, k])
    # Delete duplicates
    c = [connections[0]]
    for link in connections[1:]:
        if np.isin(np.isin(c, link).sum(axis=1),2).any() or np.isin(np.isin(c, np.flip(link)).sum(axis=1),2).any() :
            pass
        else :
            c.append(link)
    c = np.asarray(c)
    c = c+n_bus_tot
    # Init lines
    for k in c:
        rd = _sample(probability)
        pp.create_line(net,
                       from_bus=k[0],
                       to_bus=k[1],
                       length_km= line_lenght*(1+ np.abs(rd)*line_lenght_variance),
                       std_type="94-AL1/15-ST1A 0.4",
                       name="{0}_{1}".format(k[0],k[1]),
                       max_loading_percent=100)
    pp.create_line(net,
                   from_bus=n_bus_tot,
                   to_bus=connected_to,
                   length_km= line_lenght*(1+ np.abs(rd)*line_lenght_variance),
                   std_type="94-AL1/15-ST1A 0.4",
                   name="{0}_{1}".format(k[0],k[1]),
                   max_loading_percent=100)
    return net

def add_random_tree_network(rs, net, lv_id, n_bus=20,
                            line_lenght=0.03, line_lenght_variance=0, probability="normal"):
    """Create a random distribution network (radial structure).
    
    Parameter
    ---------
    - rs : random sequence generator
        Shoud be specify to randomly sample tree networks.
    - net : pandapower network
        Network on witch to add the subnetwork.
    - n_bus : int
        Number of buses of the network.
    - lv_id : int
        Id of the lv subnetwork
    - line_lenght : float
        Lenght of a line (in km)
    - line_lenght_variance : float
        Dispersion of lines (in km). Use a Weibull distribution (default is 0, not random)"""
    ############# create net #############
    # Init HTA connection
    id_HTA = len(net.bus)
    pp.create_bus(net,
                  vn_kv = 20,
                  name="Bus_{0}".format(id_HTA),
                  index=id_HTA,
                  type='b',
                  zone="HTA",
                  in_service=True)
    connect_HTA(rs, net, id_HTA)
    # Init the bus after transformer
    id_BT = len(net.bus)
    pp.create_bus(net,
                  vn_kv = 0.4,
                  name="Bus_{0}".format(id_BT),
                  index=id_BT,
                  type='b',
                  zone="LV_{0}".format(lv_id),
                  in_service=True)
    
    net = _create_BT(net, rs, n_bus, lv_id, id_BT, line_lenght, probability, line_lenght_variance)
    
    # Init trafo
    pp.create_transformer(net, 
                          hv_bus=id_HTA, 
                          lv_bus=id_BT, 
                          std_type="0.4 MVA 20/0.4 kV",
                          name="trafo_{0}_{1}".format(n_bus-1,n_bus),
                          in_service=True,
                          index=None,
                          max_loading_percent=100,
                          parallel=1,
                          df=1.0)
    net.trafo["shift_degree"]=0 # OPF fails if it has to consider angle.
    
    # Add first line after transformer
    pp.create_line(net,
                   from_bus=n_bus,
                   to_bus=n_bus+1,
                   length_km=0.005,
                   std_type="94-AL1/15-ST1A 0.4",
                   name="{0}_{1}".format(n_bus,n_bus+1),
                   index=None,
                   geodata=None,
                   df=1.0,
                   parallel=1,
                   in_service=True,
                   max_loading_percent=100)
    return net

def create_random_tree_BT_network(rs, n_bus=20, line_lenght=0.03, line_lenght_variance=0,
                               probability="normal", scale_load=1, scale_gen=1):
    """Create a random tree low voltage network.
    
    Parameter
    ----------
    - rs : random sequence generator
        Shoud be specify to randomly sample tree networks.
    - n_bus : int
        Number of bus of each subnetwork.
    - n_subnetwork : int
        Number lv networks
    - line_lenght : float
        Lenght of a line (in km)
    - line_lenght_variance : float
        Dispersion of lines (in km). Use a Weibull distribution (default is 0, not random)
    """
    # Init net
    net = pp.create_empty_network(name='synthetic_network', f_hz=50.0, sn_mva=1, add_stdtypes=True)
    pp.create_bus(net, vn_kv = 0.4, name="Bus_0", index=0, type='m', zone="LV_0", in_service=True,
                  max_vm_pu=max_vm_pu_bt, min_vm_pu=min_vm_pu_bt)
    # Create the rest of the network
    net = _create_BT(net, rs, n_bus, 0, 0, line_lenght, probability, line_lenght_variance)
    net = _useless_bus(net)
    net = _reindex(net, "random_BT")
    net = _copy_network_from_empty(net, "random_BT")
    # # Create two ext grid, one sells, one buy. Allow to have piece wise linear cost. buy_state_rate should be lower than sell_spot otherwise not convex.
    # pp.create_ext_grid(net, name="sell_spot", vm_pu=1.0, bus=0, min_p_mw=0)
    # pp.create_ext_grid(net, name="buy_state_rate", vm_pu=1.0, bus=0, max_p_mw=0)
    #Add scale to load ad gen
    net.scale_load = scale_load
    net.scale_gen = scale_gen
    return net

def main(dic, path_origin, folder, save=True):
    """Create the network and saves it.
    
    Parameter
    ----------
    All the following parameter are included in dic :
        - rs : random sequence generator
            Shoud be specify to randomly sample tree networks.
        - path_origin : str
            Path where data is stored (both times series and vreated test case). By default in the NASS.
        - folder : int
            Id of the folder where testcase is saved.
        - network : str
            Type of network created. Can be ["CIGRE", "random", "LMP"].
        - n_bus : int
            Number of buses of the network. (default is 20)
        - n_subnetwork : int
            Number lv networks (default is 1)
        - line_lenght : float
            Lenght of a line in km. (default is 0.03)
        - line_lenght_variance : float
            Dispersion of lines (in km). Use a normal distribution (default is 0, not random)
    - save : bool
        If True, save the created network in the designated folder (default is True)
        
    Return
    ---------
    - net : PandaPower network saved in path_origin folder"""
    rs = dic["rs"]
    n_bus = dic["n_bus"]
    n_subnetwork = dic["n_subnetwork"]
    line_lenght = dic["line_lenght"]
    line_lenght_variance = dic["line_lenght_variance"]
    probability = dic["probability"]
    scale_load = dic["scale_load"]
    scale_gen = dic["scale_gen"]
    allow_q = dic["allow_q"]
    network = dic["network"]
    # Create the type of network passed as argument
    if network == "CIGRE":
        net = import_cigre_lv_modif()
    elif network == "random_HTA" :
        net = create_random_tree_HTA_network(rs, n_bus=n_bus, n_subnetwork=n_subnetwork,
                                         line_lenght=line_lenght, line_lenght_variance=line_lenght_variance,
                                         probability=probability, scale_load=scale_load, scale_gen=scale_gen)
        pp.runpp(net)
        create_generic_coordinates(net, respect_switches=False, library="igraph")
    elif network == "random_BT" :
        net = create_random_tree_BT_network(rs, n_bus=n_bus,
                                         line_lenght=line_lenght, line_lenght_variance=line_lenght_variance,
                                         probability=probability, scale_load=scale_load, scale_gen=scale_gen)
        pp.runpp(net)
        create_generic_coordinates(net, respect_switches=False, library="igraph")
    elif network == "LMP" :
        net = create_LMP_network()
    elif network == "Eu" :
        # net = create_European_lv(rs, line_lenght_variance)
        net = create_European_lv_custom(rs, line_lenght_variance,
                                        probability=probability, scale_load=scale_load, scale_gen=scale_gen)
    elif network == 'Case33' or network == 'Cigre_mv':
        net = create_pn(rs,line_lenght_variance, probability=probability,
                        scale_load=scale_load, scale_gen=scale_gen, case=network)
    elif network == 'SGE_RL' :
        net = create_SGE_RL_network(rs, line_lenght_variance=line_lenght_variance, probability=probability,
                                    scale_load=scale_load, scale_gen=scale_gen)
    elif network == 'Simplest':
        net = create_simplest_network(rs, line_lenght_variance,
                                        probability=probability, scale_load=scale_load, scale_gen=scale_gen)
    else :
        raise ValueError("network code '{0}' not implemented.".format(network))
    net.allow_q = allow_q # used in src/_modif_net/init_net to set q bounds
    if save :
        # Save the network
        save_dir_path = join(path_origin, "processed/use/", str(folder))
        # Create folder if not exist
        os.makedirs(save_dir_path, exist_ok=True)
        pd.to_pickle(net, join(save_dir_path,"net.p"))
    return net

if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    
    dic = initialize._init_dic()
    
    save = True
    plot = True
    
    # Possible path (depending on the computed being used)
    path_origin , folder = imp.path_import("PC_Gueno_win",1)
    fodler_list = [folder]
    # fixed random seed for debugging
    rs = RandomState(MT19937(SeedSequence(1)))
    for folder in fodler_list:
        net = main(dic, path_origin, folder, save=save)
        plotting.simple_plot(net) # Quick plot of the networks structure
        if plot : # More extensive plot using plotly.
            plotting.plotly.simple_plotly(net, auto_open=False)
            plotting.plotly.pf_res_plotly(net, aspectratio=(2, 1), auto_open=False)