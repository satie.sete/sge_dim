# -*- coding: utf-8 -*-
"""
Created on Tue Nov  16 10:00:00 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

This file creates test cases.
"""
# Custom import
import code_these.test_case_generation.create.process_data_UMASS_apt as pdu_apt
import code_these.test_case_generation.create.process_data_UMASS_pv as pdu_pv
import code_these.test_case_generation.create.process_data_EV_residential as pde
import code_these.test_case_generation.create.process_data_EPEX as pdep
import code_these.test_case_generation.create.create_test_case as ctc
import code_these.test_case_generation.create.create_agents as ca
import code_these.test_case_generation.create.assign_agent_to_bus as aatb
import code_these.test_case_generation.plot.plot_test_case as plot
import code_these.src._import as imp
import code_these.src._initialize as initialize

from os.path import join
import os
import pickle

def main(dic, path_origin = "H:/data/", folder=0, import_data=False, plot_test_case=True):
    
    """
    Create the test case : 
        - Import the data from the data set
        - Create a random network
        - Select the amount of pv, apartement and electric vehicule station
        - Agregate them in agent
        - Place agents on the network
        - save the time series by separating in days (less memory used when simulating)
    
    Parameters
    -----------
    All the following parameter are included in dic
        - rs : Random Seed generator
            Nedded for reproducibity
        - network : str
            If 'random' creates a random network, if 'CIGRE' import the low voltage CIGRE network.
        - n_bus : int
            Number of bus in each subnetwork of the random network (default is 20)
        - n_subnetwork : int
            number of subnetwork (low voltage) in the random network (default is 1)
        - line_lenght : float
            Lenght of a line in km. (default is 0.03
        - line_lenght_variance : float
            Dispersion of lines (in km). Use a normal distribution (default is 0, not random)
            
        - apt_p_kw : float
            Total peak power of all flats (defaul is 100 kW)
        - ev_percent : float
            Ratio of the peak power of ev station and the peak power consumed by load (default is 0.15)
        - pv_percent : float
            Ratio of the peak power of PV panels and the peak power consumed by load (default is 0.3) 
        
    - import_data : bool
        If True, import the data from the data set and eliminate bad data. (default is False)
    
    - folder : int
        Id of the folder where testcase is saved.
    - path_origin : str
        Path where data is stored (both times series and vreated test case). By default in the NASS.
        """
    # Save dict
    path_folder = join(path_origin, "processed/use", str(folder))
    os.makedirs(path_folder, exist_ok=True)
    with open(join(path_folder, 'dict_parameters.pkl'), 'wb') as fp:
        pickle.dump(dic, fp)
    
    if import_data == True:
        print("-----Processing UMASS apt -----")
        pdu_apt.main(path_origin=path_origin)
        print("DONE \n")
        
        print("-----Processing UMASS pv -----")
        pdu_pv.main(path_origin=path_origin)
        print("DONE \n")
        
        print("-----Processing Electric Vehicules-----")
        pde.main(path_origin=path_origin, n_agents=100)
        print("DONE \n")
        
        print("-----Processing EPEX spot prices -----")
        pdep.main(path_origin=path_origin)
        print("DONE \n")
        
    print("-----Creating Panda Network-----")
    ctc.main(dic, path_origin, folder)
    print("DONE \n")
    
    print("-----Aggregating PV, EV et home to create agents-----")
    ca.main(dic, path_origin, folder)
    print("DONE \n")
    
    print("-----Assign agent to bus-----")
    aatb.main(dic, path_origin, folder, plot=True)
    print("DONE \n")
    
    if plot_test_case == True :
        plot.main(folder, path_origin=path_origin, title="Network")
    
    print("-----Test case created successfully =D -----")

### MAIN ###
if __name__ == "__main__":
    """The part of the code will be executed only if THIS file is executed (not if it is call by another file).
    This serves for testing purposes only."""
    path_origin , folder = imp.path_import("PC_Gueno_win",1)
    
    dic = initialize._init_dic()
  
    main(dic, path_origin, folder, import_data=False)