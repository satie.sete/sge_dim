# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 10:00:00 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes
"""

import pandas as pd
import numpy as np
from os.path import join
import os
import datetime
# Custom import
import code_these.src._import as imp

def import_data(dir_path):
    EV = pd.read_csv(join(dir_path, "Dataset 1_EV charging reports.csv"), delimiter=";")
    return EV

def separate_agent(EV):
    g_ID = EV["Garage_ID"].unique() # Get name of charging station
    power = np.zeros((len(g_ID),60*24*365)) # Init the array containing the power drown from each station
    for k in range(len(EV)): # For each charge
        date_start = EV["Start_plugin"][k] # When charging strats
        date_end = EV["End_plugout"][k] # When charging stops
        g = EV["Garage_ID"][k] # ID of the charging station
        try : # Some charging have no end plug_out making then unusable
            p = float(EV["El_kWh"][k].replace(',','.'))/float(EV["Duration_hours"][k].replace(',','.')) # Get power during the charge (assums constant charging power)
            ixd_g = np.where(g_ID==g)
            idx_start, ixd_end = date_to_idx(date_start, date_end)
            power[ixd_g[0][0], idx_start:ixd_end] = p
        except :
            pass
    return power, g_ID

def create_synthetic_agents(EV, n_agents):
    day = 60*24
    n_days = 365
    batch = [] # collect each day that have an EV activity
    synthetic_agents = np.zeros((n_agents, day*n_days))
    for old_agent in range(EV.shape[0]):
        for k in range(n_days):
            if np.sum(np.abs(EV[old_agent, k*day:(k+1)*day])) != 0 :
                batch.append(EV[old_agent, k*day:(k+1)*day])
    for new_agent in range(n_agents) :
        random_sample = np.random.randint(0,len(batch), n_days)
        for k in range(n_days):
            synthetic_agents[new_agent, k*day:(k+1)*day] = batch[random_sample[k]]
    # Scale agent, some have 3.5kW max power, others 7kW
    scale = np.random.choice([0.5,1], (n_agents,1))
    synthetic_agents *= scale
    return synthetic_agents
    
def _convert(d):
    date = datetime.datetime.strptime(d, '%d.%m.%Y %H:%M')
    idx = (int(date.strftime("%j"))*24+int(date.strftime("%H")))*60 + int(date.strftime("%M"))
    return idx

def date_to_idx(date_start, date_end):
    idx_start = _convert(date_start)
    ixd_end = _convert(date_end)
    return idx_start, ixd_end

def stats(EV):
    min_power = EV.min(axis=1)
    max_power = EV.max(axis=1)
    mean_power = EV.mean(axis=1)
    EV_info = pd.DataFrame(np.array([range(EV.shape[0]), EV.shape[0]*[None], min_power, max_power, mean_power]).T,
                           columns=["id_data", "id_original", 'min_p_kw', 'max_p_kw', 'mean_p_kw'])
    return EV_info

def save_to_csv(save_dir_path, EV, EV_info):
    os.makedirs(save_dir_path, exist_ok=True)
    EV_info.to_csv(join(save_dir_path,"info.csv"), index=False)
    for k in range(EV.shape[0]):
        for a in range(365):
            min_in_day = 24*60
            path = join(save_dir_path, str(k))
            os.makedirs(path, exist_ok=True)
            np.savetxt(join(path,str(a)+".csv"),
                        EV[k][a*min_in_day:(a+1)*min_in_day],
                        delimiter=",")

#######################################################################################
######################################## MAIN #########################################
#######################################################################################

def main(path_origin, n_agents):
    """Import and process the EV chaging station data set.
    Save the result in a file.
    """
    # Import paths
    dir_path = join(path_origin, "raw/EV/Residential_charging/")
    save_dir_path = join(path_origin, "processed/data/ev")
    EV = import_data(dir_path=dir_path)
    EV, EV_name = separate_agent(EV)
    EV_synthetic = create_synthetic_agents(EV, n_agents)
    EV_info = stats(EV_synthetic)
    save_to_csv(save_dir_path, EV_synthetic, EV_info)
    return EV_synthetic, EV_info

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC_Roman", None)
    EV, EV_info = main(path_origin=path_origin, n_agents=100)
    
    plot = True
    if plot:
        import matplotlib.pyplot as plt
        for k in range(len(EV)):
            plt.plot(EV[k])
            plt.title("Recharge power of station {0}".format(k))
            plt.xlabel("time (min)")
            plt.ylabel("Power (kW)")
            plt.show()