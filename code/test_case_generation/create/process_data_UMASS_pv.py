# -*- coding: utf-8 -*-
"""
Created on Tue Sep  7 21:51:56 2021

@author: Guenole CHEROT, CNRS SATIE ENS Rennes

Purpose of this file :
    Imports the data from UMAS Smart* project.
    Upsamples the data that is not 1 min time step.
    Cleans the bugs (to big or to small values, NaN, ...).
    Saves the result in a CSV file.

Data :
    Located in a local repository at the ENS Rennes (H:/data/Conso/UMASS).
    Original data available at : http://traces.cs.umass.edu/index.php/Smart/Smart
"""

# Strandard libraries
import numpy as np
import pandas as pd
from os.path import join
import os
import datetime
# Custom import
import code_these.src._import as imp

#######################################################################################
#################################### Process data #####################################
#######################################################################################

def process_data_pv(path_origin):
    data_dir_path, save_dir_path_apt = _get_folders(path_origin, "pv")
    data, info = _process(data_dir_path, save_dir_path_apt)
    return data, info
    
def _get_folders(path_origin, category):
    data_dir_path = join(path_origin,"raw/Conso/UMASS/solar-panels/solar_panels")
    save_dir_path = join(path_origin, "processed/data", str(category))
    os.makedirs(save_dir_path, exist_ok=True)
    return data_dir_path, save_dir_path

def _process(path, save_path):
    info = imp.get_directories_path(path)
    df = pd.DataFrame(np.array([range(len(info)), info]).T, columns=["id_data", "id_original"])
    k = 0
    dat = []
    for ID in info :
        file_path = join(path, ID)
        file_save_path = join(save_path, str(k))
        data = import_pv_data(file_path)
        data = _upsample(data)
        dat.append(data)
        _save_data(data, file_save_path)
        df.loc[k, "min_p_kw"] = data.iloc[:,1].values.min()
        df.loc[k, "max_p_kw"] = data.iloc[:,1].values.max()
        df.loc[k, "mean_p_kw"] = data.iloc[:,1].values.mean()
        k+=1
    df.to_csv(join(save_path, "info.csv"), index=False)
    return dat, info

def import_pv_data(file_path):
    data = []
    for file in imp.get_files_path(file_path, 'csv'):
        data.append(pd.read_csv(join(file_path,file), names=["delete","date","p_kWh"]))
    data = pd.concat(data)
    return data

def _save_data(data, save_path):
    os.makedirs(save_path, exist_ok=True)
    for k in range(365):
        begin = k*24*60
        end = (k+1)*24*60
        np.savetxt(join(save_path, str(k)+".csv"),
                   data.iloc[begin:end,1].values,
                   delimiter=",")

def _upsample(data):
    """Fill the rest of the day with 0 values."""
    data.pop("delete")
    data = data.set_index(pd.DatetimeIndex(data.loc[:,"date"]))
    data.sort_index(inplace=True)
    y = int(data.index.strftime('%Y')[0])
    idx = pd.date_range(str(y)+"-01-01", str(y+1)+"-01-01", freq="min")
    ts = pd.DataFrame(index=idx)
    data = ts.join(data).fillna(method="ffill")
    data = ts.join(data).fillna(0)
    return data

def _convert(d):
    date = datetime.datetime.strptime(d, '%Y-%m-%d %H:%M:%S')
    idx = (int(date.strftime("%j"))*24+int(date.strftime("%H")))*60 + int(date.strftime("%M"))
    return idx

#######################################################################################
######################################## MAIN #########################################
#######################################################################################

def main(path_origin):
    data, info = process_data_pv(path_origin = path_origin)
    return data, info

if __name__ == "__main__":
    path_origin , folder = imp.path_import("PC",None)
    
    data, info = main(path_origin = path_origin)
    plot = True
    if plot :
        import matplotlib.pyplot as plt
        for k in range(len(data)):
            plt.plot(data[k].loc[:,"p_kWh"])
            plt.title("Power produced by PV panel n°{0}".format(k))
            plt.xlabel("Date")
            plt.ylabel("Power (kW)")
            plt.show()