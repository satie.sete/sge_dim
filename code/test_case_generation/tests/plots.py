# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 14:44:05 2022

@author: Guenole

This file contains all plot functions needed to check that the random generation of test case went well.
"""
import matplotlib.pyplot as plt
import numpy as np

############################### From process_data_UMASS ###############################

def plot_day_agent(data, day, Id_list=[]):
    """Plot the power consumed on one day for a specific agent."""
    if Id_list == []:
        Id_list = data["dataid"].unique()
        agent = "(all)"
    else :
        agent = Id_list[0]
    for Id in Id_list:
        plt.plot(np.arange(0,24*60+1,1),
                 data[(data["dataid"] == Id) &
                      (data["time"] >= day*24*60) &
                      (data["time"] <= (day+1)*24*60)]["p_kw"])
    plt.xlabel("Time (in min)")
    plt.ylabel("Power (in kW)")
    plt.title("Power consumed by agent {0} on day {1}".format(agent, day))
    plt.show()
    
def plot_day_sum(data, day):
    """Plot the total power consumed (sums all agent) on a specific day."""
    plt.plot(np.arange(0,24*60+1,1),
             data.groupby("time").sum()[(data.groupby("time").sum().index >= day*24*60) &
                                       (data.groupby("time").sum().index <= (day+1)*24*60)]["p_kw"]
             )
    plt.xlabel("Time (in min)")
    plt.ylabel("Power (in kW)")
    plt.title("Total power consumed (sums all agent) on day {0}".format(day))
    plt.show()
    
def plot_year_sum(data):
    """Plot the total power consumed (sums all agent) on a specific day."""
    plt.plot(data.groupby("time").sum()["p_kw"])
    plt.xlabel("Time (in min)")
    plt.ylabel("Power (in kW)")
    plt.title("Total power consumed (sums all agent) during the year")
    plt.show()
    
############################### From process_data_EV ###############################

def plot_charging_power(time_serie_df):
    # Plot the results
    plt.figure(dpi=200)
    plt.plot(time_serie_df["date"], time_serie_df["p_kw"])
    plt.title("Agregated power consumed by the charging station throughout the year")
    plt.xlabel("date")
    plt.ylabel("Charging power (kW)")
    plt.grid()
    plt.show()
    
##################### From create_agent and assign_agent_to_bus #####################

def plot_stat(data):
    plt.fill_between(data["dataid"], data["max_p_kw"], data["min_p_kw"],
             facecolor="orange", # The fill color
             color='blue',       # The outline color
             alpha=0.2,          # Transparency of the fill
             label="min and max value")   
    plt.plot(data["mean_p_kw"], label="mean_p_kw")
    plt.grid()
    plt.ylabel("Power (kW)")
    plt.xlabel("ID")
    plt.legend()
    plt.title("Statistics about the power exchange of each agent")
    plt.show()
    
############################### From separate_in_days ###############################

def plot_time_series(data):
    for k in range(data["dataid"].unique().size):
        plt.plot(data.loc[data["dataid"]==k, "grid"])
        plt.xlabel("Time (min)")
        plt.ylabel("Power (MW)")
        plt.title("Power consumed by the agregated agent {0}".format(k))
        plt.show()