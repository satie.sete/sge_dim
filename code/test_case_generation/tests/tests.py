# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 15:47:18 2022

@author: Guenole
"""

import pandas as pd
from os.path import join
import test_case_generation.tests.plots as plots
import pandapower as pp
# Custom import
import src._import as imp
from test_case_generation.tests.home_made_plotly import home_made_plotly

def test_process_data_UMASS(path_origin, plot=False):
    """Data processed by this file are stored in data/processed/data.
    We can have a look at the shape of the processed data."""
    # Import data
    path_save = join(path_origin, "processed/data")
    UMASS_apt = pd.read_csv(join(path_save, "UMASS_apt.csv"))
    UMASS_pv = pd.read_csv(join(path_save, "UMASS_pv.csv"))
    UMASS_time = pd.read_csv(join(path_save, "UMASS_time.csv"))
    apt_table = pd.read_csv(join(path_save, "apt_table.csv"))
    solar_table = pd.read_csv(join(path_save, "solar_table.csv"))
    ## Plot the data (at least some part of it)
    def plot_res(data):
        plots.plot_day_agent(data=data, day=100, Id_list=[])
        plots.plot_day_agent(data=data, day=120, Id_list=[22])
        plots.plot_day_sum(data=data, day=10)
        plots.plot_year_sum(data=data)
    
    if plot :
        plot_res(data=UMASS_apt)
        plot_res(data=UMASS_pv)
    
    ## Test that there is no Nan or crazy values
    assert ~UMASS_apt.isnull().values.any()
    assert ~UMASS_pv.isnull().values.any()
    assert ~UMASS_time.isnull().values.any()
    
    # Return data
    return UMASS_apt, UMASS_pv, UMASS_time, apt_table, solar_table
    

def test_process_data_EV(path_origin, plot=False):
    """Data processed by this file are stored in data/processed/data.
    We can have a look at the shape of the processed data."""
    # Import data
    path_save = join(path_origin, "processed/data")
    EV = pd.read_csv(join(path_save, "EV.csv"))
    EV_table = pd.read_csv(join(path_save, "EV_table.csv"))
    ## Plot the data (at least some part of it)
    def plot_res(data):
        plots.plot_day_agent(data=data, day=100, Id_list=[])
        plots.plot_day_sum(data=data, day=10)
        plots.plot_year_sum(data=data)
    
    if plot :
        plot_res(data=EV)
    
    ## Test that there is no Nan or crazy values
    assert ~EV.isnull().values.any()
    
    # Return data
    return EV, EV_table

def test_create_agents(path_origin, folder=0, plot=False):
    # Import data
    path_save = join(path_origin, "processed/use", str(folder))
    agent_table = pd.read_csv(join(path_save, "agent_table.csv"))
    import csv
    names = ["id_apt", "id_pv", "id_ev"]
    file = []
    for n in names :
        with open(join(path_save,'{0}.csv'.format(n)), newline='') as csvfile:
            f = csv.reader(csvfile, delimiter=',')
            tab = []
            for l in f :
                tab.append(l)
            file.append(tab)
    id_apt = file[0]
    id_pv = file[1]
    id_ev = file[2]
    
    if plot :
        plots.plot_stat(data=agent_table)
    
    # Return data
    return agent_table, id_apt, id_pv, id_ev

def test_separate_in_days(path_origin, folder=0, plot=False):
    # Import data
    path_save = join(path_origin, "processed/use", str(folder), "data_separated_days")
    day = pd.read_csv(join(path_save, "data_{0}.csv".format(10)))
    if plot :
        plots.plot_time_series(day)
    return day

def test_create_test_case(path_origin, folder=0, plot= False):
    pass

def test_assign_agent_to_bus(path_origin, folder=0, plot= False):
    """Show the network with the placing of each agent.
    The network should ahve at least one agent on each leaf.
    One agent on the ext_grid bus."""
    path_folder = join(path_origin, "processed/use", str(folder))
    net = pp.from_pickle(join(path_folder,"net.p"))
    if plot :
        home_made_plotly(net)

def main(path_origin, folder, test_process = True, plot=False):
    # Test each file
    if test_process :
        UMASS_apt, UMASS_pv, UMASS_time, apt_table, solar_table = test_process_data_UMASS(path_origin=path_origin, plot=plot)
        EV, EV_table = test_process_data_EV(path_origin=path_origin, plot=plot)
        print("Some of the series look weird (very big values). They are remooved in create agent.")
    agent_table, id_apt, id_pv, id_ev = test_create_agents(path_origin=path_origin, folder=folder, plot=plot)
    day = test_separate_in_days(path_origin=path_origin, folder=folder, plot=plot)
    test_create_test_case(path_origin=path_origin, folder=folder, plot=plot)
    test_assign_agent_to_bus(path_origin=path_origin, folder=folder, plot=plot)
    
    print("Tests passed successfully")
    return agent_table, id_apt, id_pv, id_ev, day

if __name__ == "__main__":
    # Possible path to where data is located
    path_origin , folder = imp.path_import("PC",1)
    
    agent_table, id_apt, id_pv, id_ev, day = main(path_origin=path_origin, folder=folder, test_process=False, plot=True)
    
    
    